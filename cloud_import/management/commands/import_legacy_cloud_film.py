import datetime
import os
import pathlib
import shutil
from bson import json_util

from django.core.management.base import BaseCommand
from django.core.files import File
from django.conf import settings

from static_assets import models as models_assets
from common import upload_paths


class Command(BaseCommand):
    help = 'Import legacy Blender Cloud Project'

    def add_arguments(self, parser):
        parser.add_argument('project_dir', type=str)
        parser.add_argument(
            '-s', '--slug', dest='project_names', action='append', help="provides project slugs"
        )
        parser.add_argument(
            '--all', action='store_true', help='Import all projects in the directory'
        )

    def load_doc(self, path: pathlib.Path):
        with open(str(path), "r") as read_file:
            return json_util.loads(read_file.read())

    def add_local_file(self, model, field_name, filepath: pathlib.Path):
        with open(str(filepath), 'rb') as f:
            django_file = File(f)
            setattr(model, field_name, django_file)
            model.save()

    def get_file_object(self, project_doc_path, file_id):
        self.stdout.write(self.style.NOTICE(f"\tGetting file {file_id}"))
        file_doc_path = project_doc_path.parent / 'files' / str(file_id) / 'file.json'
        return self.load_doc(file_doc_path)

    def add_static_asset_path(self, ob, property, file_path):
        # Build the full path for the file. In Blender Cloud there is a convention to place all
        # assets in a "_" directory at the root of the bucket.
        file_path_full = f"_/{file_path}"
        setattr(ob, property, file_path_full)
        ob.save()

    def import_project(self, project_abspath):
        from projects import models as models_project

        project_doc_path = project_abspath / 'index.json'
        project_doc = self.load_doc(project_doc_path)

        def add_static_asset_path(ob, property, file_path):
            # Build the full path for the file. In Blender Cloud there is a convention to place all
            # assets in a "_" directory at the root of the bucket.
            file_path_full = f"_/{file_path}"
            setattr(ob, property, file_path_full)
            ob.save()

        def add_images_to_project(film):
            # Add images for the project
            project_pictures = {'picture_header'}
            for picture in project_pictures:
                self.stdout.write(self.style.NOTICE('Adding pictures for project %s' % film))
                file_doc_path = (
                    project_doc_path.parent / 'files' / str(project_doc[picture]) / 'file.json'
                )
                file_doc = self.load_doc(file_doc_path)
                add_static_asset_path(project, picture, file_doc['file_path'])
            # Add Poster and logo
            project_pictures = {'poster', 'logo'}
            for picture in project_pictures:
                file_doc_path = (
                    project_doc_path.parent
                    / 'files'
                    / str(project_doc['extension_props']['cloud'][picture])
                    / 'file.json'
                )
                file_doc = self.load_doc(file_doc_path)
                add_static_asset_path(project, picture, file_doc['file_path'])

        def get_or_create_project():
            # Fetch or create Project object
            self.stdout.write(self.style.NOTICE('Creating project %s' % project_doc['url']))
            if models_project.films.Project.objects.filter(slug=project_doc['url']).exists():
                project = models_project.films.Project.objects.get(slug=project_doc['url'])
                self.stdout.write(
                    self.style.WARNING('Project %s already exists' % project_doc['url'])
                )
            else:
                project = models_project.films.Project.objects.create(
                    title=project_doc['name'],
                    slug=project_doc['url'],
                    description=project_doc['summary'],
                    summary=project_doc['description'],
                    status=models_project.ProjectStatus.released,
                    picture_header='place/holder.jpg',
                    logo='place/holder.jpg',
                    poster='place/holder.jpg',
                    is_published=True,
                )

            return film

        def get_or_create_collections_and_assets(film):
            # Create Chapters
            project_nodes_path = project_abspath / 'nodes'

            def get_or_create_collection(node_doc, as_parent=False):
                tab_char = "\t" if as_parent else ""
                self.stdout.write(
                    self.style.NOTICE(f"{tab_char}Creating collection {node_doc['_id']}")
                )
                if 'parent' in node_doc and node_doc['parent']:
                    if str(node_doc['_id']) == str(node_doc['parent']):
                        self.stdout.write(self.style.WARNING('Self parent detected'))
                        return
                    parent_node_doc = self.load_doc(
                        project_nodes_path / str(node_doc['parent']) / 'index.json'
                    )
                    parent = get_or_create_collection(parent_node_doc, as_parent=True)
                else:
                    parent = None

                node_id = str(node_doc['_id'])
                description = '' if 'description' not in node_doc else node_doc['description']
                collection = models_project.Collection.objects.get_or_create(
                    project=project,
                    parent=parent,
                    order=1,
                    name=node_doc['name'],
                    text=description,
                    slug=node_id,
                )[0]
                if 'picture' in node_doc and node_doc['picture']:
                    file_doc = self.get_file_object(project_doc_path, str(node_doc['picture']))
                    self.add_static_asset_path(collection, 'preview', file_doc['file_path'])

                models_project.Collection.objects.filter(pk=collection.pk).update(
                    date_created=node_doc['_created'], date_updated=node_doc['_updated']
                )

                return collection

            def get_or_create_asset(node_doc):
                self.stdout.write(self.style.NOTICE(f"Creating asset {node_doc['_id']}"))
                if 'file' not in node_doc['properties']:
                    self.stdout.write(
                        self.style.WARNING(
                            f"File property not found in asset " f"{node_doc['_id']}"
                        )
                    )
                    return
                file_doc = self.get_file_object(
                    project_doc_path, str(node_doc['properties']['file'])
                )
                # Get first variation for video
                # TODO(fsiddi) Handle storage of original file and variations
                if node_doc['properties']['content_type'] == 'video':
                    if 'variations' not in file_doc:
                        self.stdout.write(
                            self.style.WARNING(
                                f"Missing variation in video file " f"{file_doc['_id']}"
                            )
                        )
                        return
                    variation = file_doc['variations'][0]
                    source_path = f"_/{variation['file_path']}"
                else:
                    source_path = f"_/{file_doc['file_path']}"

                static_asset = models_assets.StaticAsset.objects.get_or_create(
                    source=source_path,
                    source_type=node_doc['properties']['content_type'],
                    original_filename=file_doc['name'],
                    size_bytes=file_doc['length'],
                    user_id=1,
                )[0]

                if 'picture' in node_doc and node_doc['picture']:
                    file_doc = self.get_file_object(project_doc_path, str(node_doc['picture']))

                    static_asset.thumbnail = f"_/{file_doc['file_path']}"
                    static_asset.save()

                if node_doc['properties']['content_type'] == 'video':
                    models_assets.Video.objects.get_or_create(
                        static_asset=static_asset, duration_seconds=datetime.timedelta(minutes=5)
                    )
                elif node_doc['properties']['content_type'] == 'image':
                    models_assets.Image.objects.get_or_create(static_asset=static_asset)

                if 'parent' in node_doc and node_doc['parent']:
                    parent_node_doc = self.load_doc(
                        project_nodes_path / str(node_doc['parent']) / 'index.json'
                    )
                    parent = get_or_create_collection(parent_node_doc)
                else:
                    parent = None

                node_id = str(node_doc['_id'])

                description = '' if 'description' not in node_doc else node_doc['description']
                asset = models_project.Asset.objects.get_or_create(
                    project=project,
                    collection=parent,
                    order=order,
                    name=node_doc['name'],
                    description=description,
                    slug=node_id,
                    is_published=True,
                    category='artwork',
                    static_asset=static_asset,
                )[0]

                models_project.Asset.objects.filter(pk=asset.pk).update(
                    date_created=node_doc['_created'], date_updated=node_doc['_updated']
                )

            order = 1
            for node in os.listdir(project_nodes_path):
                node_doc = self.load_doc(project_nodes_path / node / 'index.json')

                if node_doc['node_type'] == 'group':
                    get_or_create_collection(node_doc)
                elif node_doc['node_type'] == 'asset':
                    get_or_create_asset(node_doc)
                order += 1

        project = get_or_create_project()
        add_images_to_project(film)
        get_or_create_collections_and_assets(film)

        self.stdout.write(self.style.SUCCESS(f"All is great for {project.title}"))

    def handle(self, *args, **options):
        dirname_abspath = pathlib.Path(options['project_dir'])

        if options['all']:
            for entry in os.scandir(dirname_abspath):
                if entry.is_dir():
                    self.import_project(dirname_abspath / entry)
            return

        for project_name in options['project_names']:
            self.import_project(dirname_abspath / project_name)
