from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, override_settings
from django.urls import reverse_lazy

from common.tests.factories.blog import PostFactory
from common.tests.factories.users import UserFactory


@override_settings(
    DEFAULT_FILE_STORAGE='django.core.files.storage.InMemoryStorage',
)
class TestPostFeed(TestCase):
    feed_url = reverse_lazy('post-feed')

    @classmethod
    def setUpTestData(cls) -> None:
        cls.free_posts = [
            PostFactory(
                is_subscribers_only=False,
                author=UserFactory(),
                is_published=True,
                thumbnail=SimpleUploadedFile('test.png', b'foobar', content_type='image/png'),
            )
            for _ in range(2)
        ]
        cls.post_subscribers_only = PostFactory(
            is_subscribers_only=True, author=UserFactory(), is_published=True
        )

    def test_feed_contains_free_post_but_not_subscriber_only_posts(self):
        response = self.client.get(self.feed_url)

        self.assertEqual(response.status_code, 200)

        self.assertContains(response, self.free_posts[0].get_absolute_url())
        self.assertContains(response, self.free_posts[1].get_absolute_url())
        self.assertContains(response, self.free_posts[0].excerpt)
        self.assertContains(response, self.free_posts[1].excerpt)
        self.assertContains(response, self.free_posts[0].title)
        self.assertContains(response, self.free_posts[1].title)

        self.assertNotContains(response, self.post_subscribers_only.get_absolute_url())
        self.assertNotContains(response, self.post_subscribers_only.excerpt)
        self.assertNotContains(response, self.post_subscribers_only.title)
