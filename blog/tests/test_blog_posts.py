from datetime import timedelta

from actstream.models import Action
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import timezone

from looper.tests.factories import SubscriptionFactory

from blog.models import Post
from comments.models import Comment
from common.tests.factories.blog import PostFactory
from common.tests.factories.comments import CommentFactoryWithSignals, CommentUnderPostFactory
from common.tests.factories.projects import ProjectFactory
from common.tests.factories.helpers import create_test_image
from common.tests.factories.users import UserFactory

User = get_user_model()


@override_settings(
    DEFAULT_FILE_STORAGE='django.core.files.storage.InMemoryStorage',
)
class TestPostCreation(TestCase):
    fixtures = ['groups']

    @classmethod
    def setUpTestData(cls) -> None:
        cls.admin = User.objects.create_superuser(
            username='superuser', password='Blender123!', email='admin@example.com'
        )
        cls.project = ProjectFactory()
        cls.thumbnail = create_test_image()
        cls.post_add_url = reverse('admin:blog_post_add')

    def setUp(self) -> None:
        self.client.login(username='superuser', password='Blender123!')

    def test_post_creation(self):
        initial_post_count = Post.objects.count()
        print(initial_post_count)

        with self.thumbnail as img:
            post_form_data = {
                'project': self.project.id,
                'title': 'New blog post',
                'slug': 'new-blog-post',
                'topic': 'Announcement',
                'content': '# Test text',
                'thumbnail': img,
                'author': self.admin.id,
                'category': Post.PostCategory.UPDATE,
            }
            response = self.client.post(self.post_add_url, post_form_data, follow=True)
            self.assertEqual(response.status_code, 200)

            # Check for form errors (adminform is present if the form validation has failed)
            if 'adminform' in response.context_data:
                form = response.context_data['adminform'].form
                if form.errors:
                    print(f"Form errors: {form.errors}")
                self.assertEqual(form.is_valid(), True)

        self.assertEqual(Post.objects.count(), initial_post_count + 1)
        post = Post.objects.latest('date_created')
        self.assertHTMLEqual(
            post.content_html,
            '<h1 class="is-anchor" id="test-text"><a href="#test-text">Test text</a></h1>',
        )

    def test_updating_post(self):
        post = PostFactory()
        initial_post_count = Post.objects.count()

        post_change_url = reverse('admin:blog_post_change', kwargs={'object_id': post.pk})
        change_data = {
            'title': post.title,
            'category': post.category,
            'content': 'Updated content with *markdown*',
        }
        response = self.client.post(post_change_url, change_data, follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Post.objects.count(), initial_post_count)

    def test_updating_post_is_publish_also_sets_date_published_to_now(self):
        # not using freeze_time here because it breaks admin if `date_hierarchy` is used,
        # causing AttributeError:
        # "Cannot find 'tags' on FakeDatetime object, 'tags' is an invalid parameter to prefetch_related()"
        post = PostFactory(is_published=False, date_published=None)
        self.assertFalse(post.is_published)
        self.assertIsNone(post.date_published)
        initial_post_count = Post.objects.count()

        post_change_url = reverse('admin:blog_post_change', kwargs={'object_id': post.pk})
        change_data = {
            'title': post.title,
            'slug': post.slug,
            'category': post.category,
            'content': post.content,
            'author': post.author_id,
            # Publish the previously unpublished post
            'is_published': True,
        }
        response = self.client.post(post_change_url, change_data, follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Post.objects.count(), initial_post_count)
        post.refresh_from_db()
        self.assertTrue(post.is_published)
        self.assertIsNotNone(post.date_published)
        # date_published should have been set to now
        now = timezone.now()
        self.assertAlmostEqual(post.date_published, now, delta=timedelta(seconds=5))

    def test_blog_author_cannot_be_deleted(self):
        post = PostFactory()

        self.assertFalse(post.author.is_staff)
        self.assertFalse(post.author.is_superuser)
        self.assertFalse(post.author.can_be_deleted)


@override_settings(
    DEFAULT_FILE_STORAGE='django.core.files.storage.InMemoryStorage',
)
class TestPostComments(TestCase):
    fixtures = ['groups']

    def setUp(self):
        self.user_without_subscription = UserFactory()
        self.user = UserFactory()
        self.other_user = UserFactory()

        self.post = PostFactory()
        self.post_url = reverse('api-post-comment', kwargs={'post_pk': self.post.pk})
        self.post_comment = CommentUnderPostFactory(comment_post__post=self.post)

        # Commenting without subscription is not allowed, so add these to the right group
        subscribers, _ = Group.objects.get_or_create(name='subscriber')
        self.user.groups.add(subscribers)
        self.other_user.groups.add(subscribers)
        self.post_comment.user.groups.add(subscribers)

    def test_reply_to_comment_creates_notifications(self):
        # No activity yet
        self.assertEqual(Action.objects.count(), 0)

        self.client.force_login(self.user)
        data = {'message': 'Comment message', 'reply_to': self.post_comment.pk}
        response = self.client.post(self.post_url, data, content_type='application/json')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Action.objects.count(), 2)

        # No notifications for the user who replied to the comment
        self.assertEqual(list(Action.objects.notifications(self.user)), [], self.user)
        # A notification for the author of the comment they replied to
        comment = Comment.objects.get(pk=response.json()['id'])
        self.assertEqual(
            [str(_) for _ in Action.objects.notifications(self.post_comment.user)],
            [f'{self.user} replied to {comment} on {self.post_comment} 0 minutes ago'],
            self.post_comment.user,
        )
        # A notification for the author of the blog post
        self.assertEqual(
            [str(_) for _ in Action.objects.notifications(self.post.author)],
            [f'{self.user} commented {comment} on {self.post} 0 minutes ago'],
        )

    def test_reply_to_comment_deleted_deletes_notification(self):
        # No activity yet
        self.assertEqual(Action.objects.count(), 0)

        self.client.force_login(self.user)
        data = {'message': 'Comment message', 'reply_to': self.post_comment.pk}
        response = self.client.post(self.post_url, data, content_type='application/json')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Action.objects.count(), 2)

        # No notifications for the user who replied to the comment
        self.assertEqual(list(Action.objects.notifications(self.user)), [], self.user)
        # A notification for the author of the comment they replied to
        comment = Comment.objects.get(pk=response.json()['id'])
        self.assertEqual(
            [str(_) for _ in Action.objects.notifications(self.post_comment.user)],
            [f'{self.user} replied to {comment} on {self.post_comment} 0 minutes ago'],
            self.post_comment.user,
        )
        # A notification for the author of the blog post
        self.assertEqual(
            [str(_) for _ in Action.objects.notifications(self.post.author)],
            [f'{self.user} commented {comment} on {self.post} 0 minutes ago'],
        )

        # A notifications should be deleted along with the reply
        comment.delete()
        self.assertEqual(Action.objects.count(), 0)

    def test_comment_deleted_deletes_notifications(self):
        # No activity yet
        self.assertEqual(Action.objects.count(), 0)
        # Create an unrelated notification that must remain after comment is deleted
        [
            CommentFactoryWithSignals(
                reply_to=CommentUnderPostFactory(comment_post__post=PostFactory())
            )
            for _ in range(2)
        ]
        self.assertEqual(Action.objects.count(), 2)

        self.client.force_login(self.user)
        data = {'message': 'Comment message', 'reply_to': self.post_comment.pk}
        response = self.client.post(self.post_url, data, content_type='application/json')

        self.assertEqual(response.status_code, 200)
        comment = Comment.objects.get(pk=response.json()['id'])
        # Two more notifications were created
        self.assertEqual(Action.objects.count(), 4)
        # Post author was notified about a comment
        self.assertEqual(
            Action.objects.filter(
                verb='commented',
                action_object_object_id=comment.pk,
                action_object_content_type_id=ContentType.objects.get_for_model(Comment).pk,
            ).count(),
            1,
        )
        # Comment author was notified about a reply to comment
        self.assertEqual(
            Action.objects.filter(
                verb='replied to',
                target_object_id=self.post_comment.pk,
                target_content_type_id=ContentType.objects.get_for_model(Comment).pk,
            ).count(),
            1,
        )

        # No notifications for the user who replied to the comment
        self.assertEqual(list(Action.objects.notifications(self.user)), [], self.user)
        # A notification for the author of the comment they replied to
        self.assertEqual(
            [str(_) for _ in Action.objects.notifications(self.post_comment.user)],
            [f'{self.user} replied to {comment} on {self.post_comment} 0 minutes ago'],
            self.post_comment.user,
        )
        # A notification for the author of the blog post
        self.assertEqual(
            [str(_) for _ in Action.objects.notifications(self.post.author)],
            [f'{self.user} commented {comment} on {self.post} 0 minutes ago'],
        )

        # Notifications should be deleted along with the post comment
        self.post_comment.delete()
        self.assertEqual(
            Action.objects.filter(action_object_object_id=self.post_comment.pk).count(), 0
        )
        self.assertEqual(list(Action.objects.notifications(self.post_comment.user)), [])
        self.assertEqual(list(Action.objects.notifications(self.post.author)), [])
        # Other notifications remained as is
        self.assertEqual(Action.objects.count(), 2)

    def test_reply_to_comment_soft_deleted_deletes_notification(self):
        # No activity yet
        self.assertEqual(Action.objects.count(), 0)

        self.client.force_login(self.user)
        data = {'message': 'Comment message', 'reply_to': self.post_comment.pk}
        response = self.client.post(self.post_url, data, content_type='application/json')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Action.objects.count(), 2)

        # No notifications for the user who replied to the comment
        self.assertEqual(list(Action.objects.notifications(self.user)), [], self.user)
        # A notification for the author of the comment they replied to
        comment = Comment.objects.get(pk=response.json()['id'])
        self.assertEqual(
            [str(_) for _ in Action.objects.notifications(self.post_comment.user)],
            [f'{self.user} replied to {comment} on {self.post_comment} 0 minutes ago'],
            [str(_) for _ in Action.objects.notifications(self.post_comment.user)],
        )
        # A notification for the author of the blog post
        self.assertEqual(
            [str(_) for _ in Action.objects.notifications(self.post.author)],
            [f'{self.user} commented {comment} on {self.post} 0 minutes ago'],
        )

        # A notifications should be deleted along with the reply
        comment.soft_delete()
        self.assertEqual(list(Action.objects.notifications(self.post_comment.user)), [])
        self.assertEqual(list(Action.objects.notifications(self.post.author)), [])
        self.assertEqual(Action.objects.count(), 0)

    def test_comment_soft_deleted_deletes_notifications(self):
        # No activity yet
        self.assertEqual(Action.objects.count(), 0)
        # Create an unrelated notification that must remain after comment is deleted
        [
            CommentFactoryWithSignals(
                reply_to=CommentUnderPostFactory(comment_post__post=PostFactory())
            )
            for _ in range(2)
        ]
        self.assertEqual(Action.objects.count(), 2)

        self.client.force_login(self.user)
        data = {'message': 'Comment message', 'reply_to': self.post_comment.pk}
        response = self.client.post(self.post_url, data, content_type='application/json')

        self.assertEqual(response.status_code, 200)
        comment = Comment.objects.get(pk=response.json()['id'])
        # Two more notifications were created
        self.assertEqual(Action.objects.count(), 4)
        # Post's author was notified about a comment
        self.assertEqual(
            Action.objects.filter(
                verb='commented',
                action_object_object_id=comment.pk,
                action_object_content_type_id=ContentType.objects.get_for_model(Comment).pk,
            ).count(),
            1,
        )
        # Comment's author was notified about a reply to comment
        self.assertEqual(
            Action.objects.filter(
                verb='replied to',
                target_object_id=self.post_comment.pk,
                target_content_type_id=ContentType.objects.get_for_model(Comment).pk,
            ).count(),
            1,
        )

        # No notifications for the user who replied to the comment
        self.assertEqual(list(Action.objects.notifications(self.user)), [], self.user)
        # A notification for the author of the comment they replied to
        self.assertEqual(
            [str(_) for _ in Action.objects.notifications(self.post_comment.user)],
            [f'{self.user} replied to {comment} on {self.post_comment} 0 minutes ago'],
            self.post_comment.user,
        )
        # A notification for the author of the blog post
        self.assertEqual(
            [str(_) for _ in Action.objects.notifications(self.post.author)],
            [f'{self.user} commented {comment} on {self.post} 0 minutes ago'],
        )

        # Notifications should be deleted along with the post comment
        self.post_comment.soft_delete_tree()
        self.assertEqual(
            Action.objects.filter(action_object_object_id=self.post_comment.pk).count(), 0
        )
        self.assertEqual(list(Action.objects.notifications(self.post_comment.user)), [])
        self.assertEqual(
            list(Action.objects.notifications(self.post.author)),
            [],
        )
        # Other notifications remained as is
        self.assertEqual(Action.objects.count(), 2)

    def test_reply_to_your_own_comment_does_not_create_notification(self):
        # No activity yet
        self.assertEqual(Action.objects.count(), 0)

        self.client.force_login(self.post_comment.user)
        data = {'message': 'Comment message', 'reply_to': self.post_comment.pk}
        response = self.client.post(self.post_url, data, content_type='application/json')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Action.objects.count(), 1)

        comment = Comment.objects.get(pk=response.json()['id'])
        # No notifications for the user who replied to the comment
        self.assertEqual(list(Action.objects.notifications(self.post_comment.user)), [])
        # A notification for the author of the blog post
        self.assertEqual(
            [str(_) for _ in Action.objects.notifications(self.post.author)],
            [f'{self.post_comment.user} commented {comment} on {self.post} 0 minutes ago'],
        )

    def test_liking_post_comment_creates_notification_for_comments_author(self):
        # No activity yet
        self.assertEqual(Action.objects.count(), 0)

        self.client.force_login(self.user)
        response = self.client.post(
            self.post_comment.like_url, {'like': True}, content_type='application/json'
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Action.objects.count(), 1)
        action = Action.objects.first()
        self.assertEqual(action.action_object, self.post_comment)
        self.assertEqual(action.actor, self.user)
        self.assertEqual(action.target, self.post)
        self.assertFalse(action.public)

        self.assertNotEqual(self.post.author, self.post_comment.user)
        # Comment's author should be notified about the like on their comment
        self.assertEqual(
            [str(_) for _ in Action.objects.notifications(self.post_comment.user)],
            [f'{self.user} liked {self.post_comment} on {self.post} 0 minutes ago'],
        )
        # but blog post's author should not be notified
        self.assertEqual(list(Action.objects.notifications(self.post.author)), [], self.post.author)

    def test_delete_post_comment_deletes_notifications_for_likes(self):
        # No activity yet
        self.assertEqual(Action.objects.count(), 0)

        self.client.force_login(self.user)
        response = self.client.post(
            self.post_comment.like_url, {'like': True}, content_type='application/json'
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Action.objects.count(), 1)
        action = Action.objects.first()
        self.assertEqual(action.action_object, self.post_comment)
        self.assertEqual(action.actor, self.user)
        self.assertEqual(action.target, self.post)
        self.assertFalse(action.public)

        self.assertNotEqual(self.post.author, self.post_comment.user)
        # Comment's author should be notified about the like on their comment
        self.assertEqual(
            [str(_) for _ in Action.objects.notifications(self.post_comment.user)],
            [f'{self.user} liked {self.post_comment} on {self.post} 0 minutes ago'],
        )
        # but blog post's author should not be notified
        self.assertEqual(list(Action.objects.notifications(self.post.author)), [], self.post.author)

        # Notifications should be deleted along with the comment
        self.post_comment.delete()
        self.assertEqual(Action.objects.count(), 0)

    def test_soft_delete_post_comment_deletes_notifications_for_likes(self):
        # No activity yet
        self.assertEqual(Action.objects.count(), 0)

        self.client.force_login(self.user)
        response = self.client.post(
            self.post_comment.like_url, {'like': True}, content_type='application/json'
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Action.objects.count(), 1)
        action = Action.objects.first()
        self.assertEqual(action.action_object, self.post_comment)
        self.assertEqual(action.actor, self.user)
        self.assertEqual(action.target, self.post)
        self.assertFalse(action.public)

        self.assertNotEqual(self.post.author, self.post_comment.user)
        # Comment's author should be notified about the like on their comment
        self.assertEqual(
            [str(_) for _ in Action.objects.notifications(self.post_comment.user)],
            [f'{self.user} liked {self.post_comment} on {self.post} 0 minutes ago'],
        )
        # but blog post's author should not be notified
        self.assertEqual(list(Action.objects.notifications(self.post.author)), [], self.post.author)

        # Notifications should be deleted along with the comment
        self.post_comment.soft_delete()
        self.assertEqual(Action.objects.count(), 0)

    def test_commenting_on_post_creates_notification_for_posts_author(self):
        # No activity yet
        self.assertEqual(Action.objects.count(), 0)

        self.client.force_login(self.user)
        data = {'message': 'Comment message'}
        response = self.client.post(self.post_url, data, content_type='application/json')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Action.objects.count(), 1)
        action = Action.objects.first()
        comment = Comment.objects.get(pk=response.json()['id'])
        self.assertEqual(action.actor, self.user)
        self.assertEqual(action.target, self.post)
        self.assertEqual(action.action_object, comment)
        self.assertEqual(
            str(action),
            f'{self.user} commented {comment} on {self.post} 0 minutes ago',
            str(action),
        )

        self.assertNotEqual(self.post.author, self.post_comment.user)
        # Blog post's author should be notified about the comment on their post
        self.assertEqual(list(Action.objects.notifications(self.post.author)), [action])
        self.assertEqual(list(Action.objects.notifications(self.post_comment.user)), [])


@override_settings(
    DEFAULT_FILE_STORAGE='django.core.files.storage.InMemoryStorage',
)
class TestViewPost(TestCase):
    fixtures = ['groups', 'gateways', 'plans']

    @classmethod
    def setUpTestData(cls) -> None:
        # PostFactory mutes signals, hence a separate UserFactory call to make sure Customer is created
        cls.post = PostFactory(is_subscribers_only=False, author=UserFactory())
        cls.post_subscribers_only = PostFactory(is_subscribers_only=True, author=UserFactory())
        cls.post_unpublished = PostFactory(is_published=False, author=UserFactory())
        cls.post_scheduled = PostFactory(
            is_published=True,
            date_published=timezone.now() + timedelta(days=10),
            author=UserFactory(),
        )
        cls.staff = UserFactory(is_staff=True)

    def test_post_can_be_viewed_by_anyone(self):
        subscriber = UserFactory()
        SubscriptionFactory(customer=subscriber.customer, status='active')

        group, _ = Group.objects.get_or_create(name='demo')
        demo = UserFactory()
        demo.groups.add(group)

        url = self.post.get_absolute_url()
        for account, role in (
            (None, 'anonymous'),
            (subscriber, 'subscriber'),
            (demo, 'demo'),
            (self.post.author, 'author'),
        ):
            with self.subTest(account=account, role=role):
                assert not account or account.customer
                self.client.logout()
                if account:
                    self.client.force_login(account)
                response = self.client.get(url)
                self.assertEqual(response.status_code, 200)
                self.assertContains(response, self.post.content_html)

    def test_unpublished_post_cannot_be_viewed_by_anyone(self):
        subscriber = UserFactory()
        SubscriptionFactory(customer=subscriber.customer, status='active')

        group, _ = Group.objects.get_or_create(name='demo')
        demo = UserFactory()
        demo.groups.add(group)

        url = self.post_unpublished.get_absolute_url()
        # Unpublished post cannot be viewed by regular accounts
        for account, role in (
            (None, 'anonymous'),
            (subscriber, 'subscriber'),
            (demo, 'demo'),
        ):
            with self.subTest(account=account, role=role):
                assert not account or account.customer
                self.client.logout()
                if account:
                    self.client.force_login(account)
                response = self.client.get(url)
                self.assertEqual(response.status_code, 404)

        # Unpublished post can be viewed by staff
        for account, role in (
            (self.staff, 'staff'),
        ):
            with self.subTest(account=account, role=role):
                assert not account or account.customer
                self.client.logout()
                if account:
                    self.client.force_login(account)
                response = self.client.get(url)
                self.assertEqual(response.status_code, 200)
                self.assertContains(response, self.post_unpublished.content_html)

    def test_scheduled_post_cannot_be_viewed_by_anyone(self):
        subscriber = UserFactory()
        SubscriptionFactory(customer=subscriber.customer, status='active')

        group, _ = Group.objects.get_or_create(name='demo')
        demo = UserFactory()
        demo.groups.add(group)

        url = self.post_scheduled.get_absolute_url()
        # Scheduled post cannot be viewed by regular accounts
        for account, role in (
            (None, 'anonymous'),
            (subscriber, 'subscriber'),
            (demo, 'demo'),
        ):
            with self.subTest(account=account, role=role):
                assert not account or account.customer
                self.client.logout()
                if account:
                    self.client.force_login(account)
                response = self.client.get(url)
                self.assertEqual(response.status_code, 404)

        # Scheduled post can be viewed by staff
        for account, role in (
            (self.staff, 'staff'),
        ):
            with self.subTest(account=account, role=role):
                assert not account or account.customer
                self.client.logout()
                if account:
                    self.client.force_login(account)
                response = self.client.get(url)
                self.assertEqual(response.status_code, 200)
                self.assertContains(response, self.post_scheduled.content_html)

    def test_subscribers_only_post_cannot_be_viewed_by_non_subscriber(self):
        non_subscriber = UserFactory()

        url = self.post_subscribers_only.get_absolute_url()
        for account, role in (
            (None, 'anonymous'),
            (non_subscriber, 'non-subscriber'),
        ):
            with self.subTest(account=account, role=role):
                assert not account or account.customer
                self.client.logout()
                if account:
                    self.client.force_login(account)
                response = self.client.get(url)
                self.assertEqual(response.status_code, 200)
                self.assertNotContains(response, self.post_subscribers_only.content_html)
                self.assertContains(response, 'Join Blender Studio')
                self.assertContains(response, '/join/')

    def test_subscribers_only_post_can_be_viewed_by_subscriber_or_demo(self):
        subscriber = UserFactory()
        SubscriptionFactory(customer=subscriber.customer, status='active')

        group, _ = Group.objects.get_or_create(name='demo')
        demo = UserFactory()
        demo.groups.add(group)

        url = self.post_subscribers_only.get_absolute_url()
        for account, role in (
            (subscriber, 'subscriber'),
            (demo, 'demo'),
        ):
            with self.subTest(account=account, role=role):
                assert account.customer
                self.client.logout()
                if account:
                    self.client.force_login(account)
                response = self.client.get(url)
                self.assertEqual(response.status_code, 200)
                self.assertContains(response, self.post_subscribers_only.content_html)
