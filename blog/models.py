import datetime
from typing import Any

from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.text import slugify
from taggit.managers import TaggableManager

from comments.models import Comment
from common import mixins, markdown
from common.upload_paths import get_upload_to_hashed_path
from training.models import Training
import common.help_texts
import static_assets.models as models_static_assets

User = get_user_model()


class Post(mixins.CreatedUpdatedMixin, mixins.StaticThumbnailURLMixin, models.Model):
    class PostCategory(models.TextChoices):
        UPDATE = 'update', 'Update'
        ARTICLE = 'article', 'Article'
        STANDALONE = 'standalone', 'Standalone Project'

    class Meta:
        ordering = ('-date_published',)

    project = models.ForeignKey(
        'projects.Project', blank=True, null=True, on_delete=models.CASCADE, related_name='posts'
    )
    training = models.ForeignKey(
        Training, blank=True, null=True, on_delete=models.CASCADE, related_name='posts'
    )

    author = models.ForeignKey(User, on_delete=models.PROTECT, related_name='authored_posts')
    contributors = models.ManyToManyField(
        User,
        blank=True,
        help_text='People who contributed to creation of this blog post.',
        verbose_name='contributors (optional)',
    )
    slug = models.SlugField(max_length=128)
    date_published = models.DateTimeField(blank=True, null=True)
    legacy_id = models.CharField(max_length=256, blank=True)
    is_published = models.BooleanField(default=False)
    is_subscribers_only = models.BooleanField(default=False)

    title = models.CharField(max_length=512)
    category = models.CharField(
        max_length=64,
        choices=PostCategory.choices,
        db_index=True,
        default=PostCategory.UPDATE,
    )
    excerpt = models.TextField(
        blank=True, help_text='An optional short description displayed on the blog card.'
    )
    content = models.TextField(help_text=common.help_texts.markdown_with_html)
    content_html = models.TextField(blank=True, editable=False)
    thumbnail = models.FileField(
        upload_to=get_upload_to_hashed_path,
        blank=True,
        help_text='A 1920x1080 picture for the blog list and on social media.',
    )
    header = models.FileField(
        upload_to=get_upload_to_hashed_path,
        blank=True,
        help_text='A 2048x640 picture for the blog header. Can be abstract.',
    )

    comments = models.ManyToManyField(Comment, through='PostComment', related_name='post')
    attachments = models.ManyToManyField(models_static_assets.StaticAsset, blank=True)
    tags = TaggableManager(blank=True)

    def __str__(self):
        return self.slug

    def save(self, *args: Any, **kwargs: Any) -> None:
        """Generates the html version of the content and saves the object."""
        if not self.slug:
            self.slug = slugify(self.title)
        # Clean but preserve some of the HTML tags
        self.content = markdown.clean(self.content)

        # Convert Markdown to HTML, and replace headings with anchor links.
        markdown_to_html = markdown.render_unsafe(self.content)
        self.content_html = markdown.replace_headings_with_links(markdown_to_html)

        # Set publish date if it's not set and the post is published
        if not self.date_published and self.is_published:
            self.date_published = timezone.now()
        super().save(*args, **kwargs)

    @property
    def is_new(self) -> bool:
        if self.is_scheduled:
            return False
        return self.date_published > timezone.now() - datetime.timedelta(days=7)

    @property
    def is_scheduled(self) -> bool:
        if self.date_published and self.is_published:
            return self.date_published > timezone.now()
        return False

    def get_absolute_url(self) -> str:
        return self.url

    @property
    def url(self) -> str:
        return reverse('post-detail', kwargs={'slug': self.slug})

    @property
    def comment_url(self) -> str:
        return reverse('api-post-comment', kwargs={'post_pk': self.pk})

    @property
    def like_url(self) -> str:
        return reverse('api-post-like', kwargs={'post_pk': self.pk})

    @property
    def admin_url(self) -> str:
        return reverse('admin:blog_post_change', args=[self.pk])

    def reading_time(self) -> int:
        """
        Calculate the total reading time for the post by dividing the words
        count by an average of words per minute.
        Returns the total time in minutes.
        """
        # Average reading speed in words per minute (200 WPM)
        words_per_minute = 200
        word_count = len(self.content.split())
        # Calculate reading time in minutes (rounded up to nearest minute)
        return max(1, round(word_count / words_per_minute))

    def watch_time(self) -> float:
        """
        Calculate the total watch time for the post by summing the durations
        of all video attachments.
        Returns the total duration in minutes.
        """
        total_duration_seconds = 0

        # Filter video attachments by source_type.
        video_attachments = self.attachments.filter(
            source_type=models_static_assets.StaticAssetFileTypeChoices.video
        )

        for asset in video_attachments:
            if asset.video and asset.video.duration:
                total_duration_seconds += asset.video.duration.total_seconds()

        return total_duration_seconds / 60  # Return total duration in minutes


class PostComment(models.Model):
    """An intermediary model between Post and Comment.

    A PostComment should in fact only relate to one Comment, hence the
    OneToOne comment field.
    """

    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    comment = models.OneToOneField(Comment, on_delete=models.CASCADE)

    def get_absolute_url(self) -> str:
        return self.post.url


class Like(mixins.CreatedUpdatedMixin, models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['user', 'post'], name='only_one_like_per_post_and_user')
        ]

    # Whenever a User is deleted their Like lives on to ensure integrity of the conversation.
    # Instead, we remove the reference to the User to honor the deletion request as much as
    # possible.
    user = models.ForeignKey(
        User, null=True, blank=False, on_delete=models.SET_NULL, related_name='liked_posts'
    )
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='likes')

    def __str__(self) -> str:
        return f'Like by {self.username} on {self.post}'

    @property
    def username(self) -> str:
        return '<deleted>' if self.user is None else self.user.username
