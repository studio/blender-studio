from django.contrib import admin
from django.forms import Textarea

from blog.models import Post
from common.mixins import ViewOnSiteMixin
import search.signals


@admin.register(Post)
class PostAdmin(ViewOnSiteMixin, admin.ModelAdmin):
    date_hierarchy = 'date_published'
    save_on_top = True
    list_display = [
        'title',
        'category',
        'project_label',
        'tags_list',
        'author_name',
        'subscribers_only',
        'is_published',
        'date_published',
        'view_link',
    ]
    list_filter = [
        'category',
        'is_subscribers_only',
        'is_published',
        'category',
        'project',
    ]

    def project_label(self, obj: Post) -> str:
        project = obj.project if obj.project else obj.training if obj.training else ""
        return project

    project_label.short_description = 'Project'

    def subscribers_only(self, obj: Post) -> bool:
        return obj.is_subscribers_only

    subscribers_only.short_description = 'Subs Only'
    subscribers_only.boolean = True

    def author_name(self, obj: Post) -> str:
        return obj.author.full_name

    author_name.short_description = 'Author'

    def tags_list(self, obj: Post) -> str:
        max_tags = 5
        tags = obj.tags.all()[:max_tags] # Get max_tags 5 tags
        tag_names = [tag.name for tag in tags]

        remaining_count = obj.tags.count() - max_tags # Append remaining tags if any
        if remaining_count > 0:
            tag_names.append(f"+ {remaining_count}")

        return ", ".join(tag_names)

    tags_list.short_description = 'Tags'

    def formfield_for_dbfield(self, db_field, **kwargs):
        """Override display of "excerpt" field.

        Make it appear smaller than the content text area.
        """
        if db_field.name == 'excerpt':
            kwargs['widget'] = Textarea(attrs={'rows': 2, 'cols': 40})
        return super().formfield_for_dbfield(db_field, **kwargs)

    fieldsets = (
        (
            None,
            {
                'fields': [
                    'date_published',
                    'title',
                    'slug',
                    'category',
                    'tags',
                    'project',
                    'training',
                    ('author', 'contributors'),
                    'excerpt',
                    'content',
                    'attachments',
                    'header',
                    'thumbnail',
                    'is_subscribers_only',
                    'is_published',
                ],
            },
        ),
    )
    autocomplete_fields = ['author', 'attachments', 'contributors', 'project', 'training']
    search_fields = ['slug', 'title', 'excerpt']
    prepopulated_fields = {
        'slug': ('title',),
    }

    actions = [search.signals.reindex, search.signals.remove_from_index]

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        return queryset.prefetch_related('tags')
