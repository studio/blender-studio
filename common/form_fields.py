"""Defines custom form fields and widgets."""
from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.safestring import mark_safe
import django.db.models.fields.files

from s3direct.widgets import S3DirectWidget

import common.storage


class S3DirectFileFieldWidget(S3DirectWidget):
    """Make S3DirectWidget work with FileField instead.

    The widget was written for URLField, we need it working with FileField instead.
    """

    def render(self, name, value, **kwargs):
        """Handle rendering of FileField."""
        if isinstance(value, django.db.models.fields.files.FieldFile):
            file_url = value.url if value else ''
            file_name = value.name if value else ''
        else:
            file_url = file_name = value or ''
        csrf_cookie_name = getattr(settings, 'CSRF_COOKIE_NAME', 'csrftoken')

        ctx = {
            'policy_url': reverse('s3direct'),
            'signing_url': reverse('s3direct-signing'),
            'dest': self.dest,
            'name': name,
            'csrf_cookie_name': csrf_cookie_name,
            'file_url': file_url,
            'file_name': file_name,
        }

        return mark_safe(render_to_string('s3direct_widget.html', ctx))


class S3DirectFileField(forms.FileField):
    """Upload a file using direct upload to S3."""

    default_error_messages = {
        **forms.FileField.default_error_messages,
        'does_not_exist': "Couldn't find this file in the storage, was the upload successful?",
    }

    def clean(self, value, initial=None):
        """Parse the S3 key out of the given value and check its existence."""
        # S3DirectWidget returns an absolute URL, we just need the path part of it:
        key = value
        if settings.AWS_STORAGE_BUCKET_NAME in value:
            key = value.split(settings.AWS_STORAGE_BUCKET_NAME)[-1][1:]
        if not key:
            raise ValidationError(self.error_messages['required'], code='required')
        if not common.storage.file_exists(key):
            raise ValidationError(self.error_messages['does_not_exist'], code='required')
        return key
