from factory.django import DjangoModelFactory
import factory

from looper.tests.factories import SubscriptionFactory

from subscriptions.models import Team


class TeamFactory(DjangoModelFactory):
    class Meta:
        model = Team

    name = factory.Faker('text', max_nb_chars=15)
    subscription = factory.SubFactory(SubscriptionFactory)
