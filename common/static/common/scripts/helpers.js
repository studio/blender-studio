document.addEventListener('DOMContentLoaded', function() {
  function init() {
    const commentInputTextarea = document.querySelectorAll('.js-comment-input-textarea');

    function adjustCommentInputTextarea(item) {
      item.style.height = 'auto';
      item.style.height = item.scrollHeight + 'px';
    }

    commentInputTextarea.forEach(function(item) {
      item.addEventListener('input', function() {
        adjustCommentInputTextarea(item);
      });
    });
  }

  // TODO: call function 'init' from modal callback instead
  setTimeout(function() {
    // init();
  }, 1000);
});
