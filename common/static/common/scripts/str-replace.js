document.addEventListener('DOMContentLoaded', function() {
  // Create function replaceSubstrings
  function replaceSubstrings(className, replacements) {
    const items = document.querySelectorAll('.' + className);

    items.forEach(function (item) {
      let content = item.textContent;

      // Iterate through each replacement pair
      replacements.forEach(function (pair) {
        const stringOld = pair[0];
        const stringNew = pair[1];

        content = content.replace(new RegExp(stringOld, 'g'), stringNew);
      });

      // Update the item's content
      item.textContent = content;
    });
  }


  // Replace currency texts with symbols
  const replacements = [
    ['EUR', '€'],
    ['USD', '$'],
  ];

  replaceSubstrings('js-price-per-month', replacements);
});
