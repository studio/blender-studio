(function navigation() {
  function navDrawerToggle() {
    document.querySelector('body').classList.toggle('nav-drawer-open');
  }

  document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.navdrawertoggle').forEach((i) => {
      i.addEventListener('click', () => {
        navDrawerToggle();
      });
    });
  });
})();

document.addEventListener('DOMContentLoaded', () => {
  // adds active class to any nav item with a href that matches the current url
  // eslint-disable-next-line no-restricted-globals
  const url = location.pathname;
  document
    .querySelectorAll(
      `a.list-group-item[href="${url}"], a.drawer-nav-section-link[href="${url}"], a.drawer-nav-dropdown[href="${url}"], .nav-item[data-link="${url}"]`
    )
    .forEach((link) => {
      link.classList.add('active');
    });
});

// Allow side-scroll drag on desktop
function sideScrollNavigation() {
  const slider = document.querySelector('.side-scroll');
  let isDown = false;
  let startX;
  let scrollLeft;

  if (slider) {
    slider.addEventListener('mousedown', (e) => {
      isDown = true;
      slider.classList.add('active');
      startX = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
    });
    slider.addEventListener('mouseleave', () => {
      isDown = false;
      slider.classList.remove('active');
    });
    slider.addEventListener('mouseup', () => {
      isDown = false;
      slider.classList.remove('active');
    });
    slider.addEventListener('mousemove', (e) => {
      if (!isDown) return;
      e.preventDefault();
      const x = e.pageX - slider.offsetLeft;
      const walk = x - startX;
      slider.scrollLeft = scrollLeft - walk;
    });
  }
}

window.addEventListener('DOMContentLoaded', () => {
  sideScrollNavigation();
});
