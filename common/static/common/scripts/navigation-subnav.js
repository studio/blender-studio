document.addEventListener('DOMContentLoaded', function() {
  const navGlobalSubnavLinks = document.querySelectorAll('.js-nav-global-subnav-link');

  if (!navGlobalSubnavLinks || navGlobalSubnavLinks.length === 0) {
    return;
  }

  function showNavSubnavPopover(navSubnavPopover, link) {
    positionNavSubnavPopover(navSubnavPopover, link);
    navSubnavPopover.classList.add('show');
  }

  function hideNavSubnavPopover(navSubnavPopover) {
    navSubnavPopover.classList.remove('show');
  }

  function positionNavSubnavPopover(navSubnavPopover, link) {
    // Get the link's left position
    const navGlobalSubnavLinkRect = link.getBoundingClientRect();
    const navGlobalSubnavLinkPositionLeft = navGlobalSubnavLinkRect.left;

    // Position 'navSubnavPopover'
    navSubnavPopover.style.left = navGlobalSubnavLinkPositionLeft + 'px';
  }

  function init() {
    navGlobalSubnavLinks.forEach(link => {
      const navGlobalSubnavLinkAttr = link.getAttribute('data-subnav');
      const navSubnavPopover = document.querySelector(navGlobalSubnavLinkAttr);

      if (!navSubnavPopover) {
        return;
      }

      // Create event 'navGlobalSubnavLink' on mouseover
      link.addEventListener('mouseover', function() {
        showNavSubnavPopover(navSubnavPopover, link);
      });

      // Create event 'navGlobalSubnavLink' on mouseleave
      link.addEventListener('mouseleave', function(e) {
        // Check if mouse has left 'navSubnavPopover' area
        if (!navSubnavPopover.contains(e.relatedTarget)) {
          hideNavSubnavPopover(navSubnavPopover);
        }
      });

      // Create event 'navSubnavPopover' on mouseleave
      navSubnavPopover.addEventListener('mouseleave', function() {
        hideNavSubnavPopover(navSubnavPopover);
      });

      // Reposition 'navSubnavPopover' on window resize
      window.addEventListener('resize', function() {
        hideNavSubnavPopover(navSubnavPopover);
      });
    });
  }

  init();
});
