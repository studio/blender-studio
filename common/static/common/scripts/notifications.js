/* global ajax:false bootstrap:false */
function markAsRead(event) {
  event.preventDefault();
  const element = event.currentTarget;
  const url = element.dataset.markReadUrl;

  if (element.dataset.isRead === 'true') return;

  ajax.jsonRequest('POST', url).then(() => {
    if (element.href) {
      window.location.href = element.href;
    } else {
      // TODO: @web-assets optionally make js notifications mark as read named function part of web-assets, that can be called on project level
      element.closest('.js-notifications-item')
        // Set notifications item parent is read
        .classList.add('is-read');

      const tooltip = bootstrap.Tooltip.getInstance(event.target);
      tooltip.dispose();
      element.remove();
    }
  });
}

function markAllAsRead(event) {
  event.preventDefault();
  const element = event.currentTarget;
  const url = element.dataset.markAllReadUrl;

  ajax.jsonRequest('POST', url).then(() => {
    document.querySelectorAll('.js-notifications-item').forEach((i) => {
      // Set notifications items is read
      i.classList.add('is-read');

      if (document.querySelector('.notifications-counter')) {
        document.querySelector('.notifications-counter').remove();
      }
    });
  });
}

document.querySelectorAll('[data-mark-read-url][data-is-read="false"]').forEach((item) => {
  item.addEventListener('click', (e) => markAsRead(e));
});

document.querySelectorAll('[data-mark-all-read-url]').forEach((item) => {
  item.addEventListener('click', (e) => markAllAsRead(e));
});
