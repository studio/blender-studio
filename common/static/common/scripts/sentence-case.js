document.addEventListener('DOMContentLoaded', function() {
  // Get all elements with the class 'js-sentence-case'
  const sentenceCaseItems = document.querySelectorAll('.js-sentence-case');

  // Create function to convert a string to sentence case
  function sentenceCase(str) {
    return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
  }

  // Apply sentence case to each item
  sentenceCaseItems.forEach((item) => {
    const textOriginal = item.textContent;
    const textSentenceCase = sentenceCase(textOriginal);

    item.textContent = textSentenceCase;
  });
});
