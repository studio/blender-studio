from django.http.request import HttpRequest
from django.http.response import HttpResponse
from django.shortcuts import redirect, render
from django.views.decorators.http import require_safe

import blog.models as models_blog
from common.models import TemplateVariable
from common.queries import get_latest_trainings_and_production_lessons
from projects.models import Project
from projects.queries import (get_production_logs, get_random_featured_assets)
from training.models import Training, Section
from training.queries.sections import recently_watched
from training.views.common import recently_watched_sections_to_template


@require_safe
def home(request: HttpRequest) -> HttpResponse:
    """
    Render the home page of Blender Studio.

    **Context**:
        ``records``
            A paginator Page object with dictionaries representing recently created objects:
            instances of :model:`blog.Post`, :model:`ProductionLog`, or
            :model:`training.Training`. The dictionaries have the following keys:

            - 'pk': int - the primary key of the related object,
            - 'date_created': datetime - the date when the object was created,
            - 'obj_type': str - specifies the type (model) of the object,
            - 'object': a particular Model instance.

    **Template**
        :template:`common/home.html`
    """
    if not request.user.is_authenticated:
        return redirect('welcome')

    context = {
        'featured_projects': Project.objects.filter(is_featured=True),
        'featured_trainings': get_latest_trainings_and_production_lessons(),
        'featured_project_assets': get_random_featured_assets(limit=8),
        'latest_posts': models_blog.Post.objects.filter(is_published=True)[:6],
        'recently_watched_sections': [],
        **{entry.key: entry for entry in TemplateVariable.objects.all()},
    }

    try:
        frontpage_project_slug = TemplateVariable.objects.get(
            key='frontpage_project_slug'
        )
        frontpage_project = Project.objects.get(slug=frontpage_project_slug.text)
        frontpage_project_production_logs = get_production_logs(frontpage_project)
        context['frontpage_project_production_logs'] = frontpage_project_production_logs
    except (Project.DoesNotExist, TemplateVariable.DoesNotExist):
        context['frontpage_project_production_logs'] = None

    if request.user.is_authenticated:
        recently_watched_sections = recently_watched(user_pk=request.user.pk)
        context['recently_watched_sections'] = recently_watched_sections_to_template(
            recently_watched_sections
        )

    return render(request, 'common/home.html', context)


@require_safe
def welcome(request: HttpRequest) -> HttpResponse:
    """
    Render the welcome page of Blender Studio.

    **Context**:
        ``featured_projects``
            A queryset of projects marked as featured.
        ``featured_trainings``
            A queryset of trainings marked as featured.

    **Template**
        :template:`common/welcome.html`
    """
    context = {
        'featured_projects': Project.objects.filter(is_featured=True),
        'featured_trainings': Training.objects.filter(is_featured=True),
        'featured_sections': Section.objects.filter(is_featured=True, is_published=True),
        'featured_project_assets': get_random_featured_assets(limit=8),
        'next_home': True,
        **{entry.key: entry for entry in TemplateVariable.objects.all()},
    }

    return render(request, 'common/welcome.html', context)
