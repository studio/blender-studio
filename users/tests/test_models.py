from datetime import datetime, timezone

from django.test import TestCase

from common.tests.factories.users import UserFactory


class TestModel(TestCase):
    def test_cannot_be_deleted(self):
        staff = UserFactory(is_staff=True)
        superuser = UserFactory(is_superuser=True, is_staff=True)

        for account, role in (
            (staff, 'staff'),
            (superuser, 'superuser'),
        ):
            with self.subTest(role=role):
                self.assertFalse(account.can_be_deleted)

    def test_request_deletion_does_nothing_when_cannot_be_deleted(self):
        staff = UserFactory(is_staff=True)
        superuser = UserFactory(is_superuser=True, is_staff=True)
        for account, role in (
            (staff, 'staff'),
            (superuser, 'superuser'),
        ):
            with self.subTest(role=role), self.assertLogs('users.models', level='WARNING') as log:
                account.request_deletion('')
                self.assertIn(
                    f'Deletion requested for a protected account pk={account.pk}, ignoring',
                    log.output[0],
                )
                account.refresh_from_db()
                self.assertIsNone(account.date_deletion_requested)
                self.assertTrue(account.is_active)

    def test_request_deletion_deactivates_account(self):
        account = UserFactory()

        with self.assertLogs('users.models', level='WARNING') as log:
            account.request_deletion('2020-12-31T23:02:03+00:00')
            self.assertIn(
                f'Deletion of pk={account.pk} requested on 2020-12-31T23:02:03+00:00, deactivating this account',
                log.output[0],
            )

        account.refresh_from_db()
        self.assertEqual(
            account.date_deletion_requested, datetime(2020, 12, 31, 23, 2, 3, tzinfo=timezone.utc)
        )
        self.assertFalse(account.is_active)
