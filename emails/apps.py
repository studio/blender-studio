from django.apps import AppConfig


class EmailsConfig(AppConfig):
    name = 'emails'

    def ready(self) -> None:
        import emails.signals  # noqa: F401
