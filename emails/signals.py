from django.db.models.signals import pre_save
from django.dispatch import receiver

from common.utils import html_to_text
import emails.models


@receiver(pre_save, sender=emails.models.Email)
def _html_to_text_message(sender: object, instance: emails.models.Email, **kwargs):
    instance.message = html_to_text(instance.html_message)
