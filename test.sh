#!/usr/bin/env sh
set -ux

env PYTHONPATH=. mypy .
black --check .
flake8 .
./manage.py test
yarn run eslint .
yarn run stylelint ./**/*.scss
