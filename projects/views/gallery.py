"""Render project asset gallery and collections."""
from django.contrib.redirects.models import Redirect
from django.http import Http404
from django.http import HttpResponse, HttpRequest
from django.shortcuts import get_object_or_404, render, redirect
from django.urls.base import reverse
from django.views.decorators.http import require_safe
from django.views.generic import TemplateView

from common.queries import has_active_subscription
from projects.models import Project, Collection, Asset
from projects.queries import (
    get_gallery_drawer_context,
    get_current_asset,
    get_asset_by_slug,
    should_show_landing_page,
)


@require_safe
def collection_list(request: HttpRequest, project_slug: str) -> HttpResponse:
    """
    Display all the project collections as well as the featured artwork in the gallery.

    **Context:**

    ``project``
        An instance of :model:`projects.Project`.
    ``asset``
        An :model:`projects.Asset` that's currently selected and will be shown via the JS modal.
        It's necessary to retrieve it in advance so that correct OG meta could be set.
    ``collections``
        A dict of all the project's collections; needed for the drawer menu.

        Structured as follows::

            {
                collection_0: [nested_collection_0, nested_collection_1, ...],
                collection_1: [...],
                ...
            }
    ``featured_artwork``
        A queryset of :model:`projects.Asset`-s belonging to the project and marked as featured.
        The featured assets are displayed on entering the gallery; also needed for the
        drawer menu (where the 'Featured Artwork' fake collection is added).
    ``user_can_edit_collection``
        A bool specifying whether the current user should be able to edit
        :model:`projects.Collection` items displayed in the drawer menu.
    ``user_can_edit_asset``
        A bool specifying whether the current user should be able to edit
        :model:`projects.Asset` items (featured assets displayed on the main gallery page).

    **Template:**

    :template:`projects/gallery.html`
    """
    project = get_object_or_404(Project, slug=project_slug)
    if should_show_landing_page(request, project):
        return redirect(
            reverse('project-detail', kwargs={'project_slug': project_slug}), permanent=False
        )

    drawer_menu_context = get_gallery_drawer_context(project, request.user)

    context = {
        'project': project,
        'user_can_view_asset': (
            request.user.is_authenticated and has_active_subscription(request.user)
        ),
        'user_can_edit_asset': (
            request.user.is_staff and request.user.has_perm('projects.change_asset')
        ),
        'user_has_production_credit': (
            request.user.is_authenticated
            and request.user.production_credits.filter(project=project)
        ),
        **drawer_menu_context,
        **get_current_asset(request),
    }

    return render(request, 'projects/gallery.html', context)


@require_safe
def collection_detail(
    request: HttpRequest, project_slug: str, collection_slug: str
) -> HttpResponse:
    """
    Display all the published assets in a :model:`projects.Collection`.

    **Context:**

    ``project``
        An instance of :model:`projects.Project`. The project that the current collection belongs to.
    ``asset``
        An :model:`projects.Asset` that's currently selected and will be shown via the JS modal.
        It's necessary to retrieve it in advance so that correct OG meta could be set.
    ``current_collection``
        An instance of :model:`projects.Collection`.
    ``current_assets``
        A queryset of published assets in the current_collection,
        ordered by ``order``, ``date_created``.
    ``collections``
        A dict of all the project's collections; needed for the drawer menu.

        Structured as follows::

            {
                collection_0: [nested_collection_0, nested_collection_1, ...],
                collection_1: [...],
                ...
            }
    ``featured_artwork``
        A queryset of :model:`projects.Asset`-s belonging to the project and marked as featured;
        needed for the drawer menu (where the 'Featured Artwork' fake collection is added).
    ``user_can_edit_collection``
        A bool specifying whether the current user should be able to edit
        :model:`projects.Collection` items displayed in the drawer menu.
    ``user_can_edit_asset``
        A bool specifying whether the current user should be able to edit
        :model:`projects.Asset` items displayed in the collection page.

    **Template:**

    :template:`projects/collection_detail.html`
    """
    project = get_object_or_404(Project, slug=project_slug)
    if not request.user.is_superuser and not project.is_published:
        raise Http404("Project does not exist")
    if should_show_landing_page(request, project):
        return redirect(
            reverse('project-detail', kwargs={'project_slug': project_slug}), permanent=False
        )

    try:
        collection = get_object_or_404(Collection, slug=collection_slug, project_id=project.id)
    except Exception:
        try:
            asset = get_asset_by_slug(slug=collection_slug, project_id=project.id, request=request)
            return redirect(asset.url, permanent=True)
        except Asset.DoesNotExist:
            # Any other old Cloud endpoints are maintained via Redirects
            existing_redirect = Redirect.objects.filter(old_path=request.path).first()
            if existing_redirect:
                return redirect(existing_redirect.new_path, permanent=True)
        raise
    child_collections = collection.child_collections.order_by(*Collection._meta.ordering)
    drawer_menu_context = get_gallery_drawer_context(project, request.user)

    context = {
        'project': project,
        'current_collection': collection,
        'current_assets': (
            collection.assets.filter(is_published=True).order_by(*Asset._meta.ordering)
        ),
        'child_collections': child_collections,
        'user_can_view_asset': (
            request.user.is_authenticated and has_active_subscription(request.user)
        ),
        'user_can_edit_asset': (
            request.user.is_staff and request.user.has_perm('projects.change_asset')
        ),
        **drawer_menu_context,
        **get_current_asset(request),
    }

    return render(request, 'projects/collection_detail.html', context)


class AllAssets(TemplateView):
    """View to handle visibility of a user credit for the project."""

    template_name = 'projects/latest_assets.html'

    def get_project(self, project_slug):  # noqa: D102
        return get_object_or_404(Project, slug=project_slug, is_published=True)

    def get_context_data(self, **kwargs):  # noqa: D102
        context = super().get_context_data(**kwargs)
        project = self.get_project(kwargs['project_slug'])
        context['project'] = project
        context.update(get_gallery_drawer_context(project, self.request.user))
        return context
