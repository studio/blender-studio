# noqa: D100
from dataclasses import dataclass
from itertools import chain
from datetime import datetime
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.forms import ModelForm, widgets
from django.http import HttpResponse, HttpResponseNotAllowed
from django.http.request import HttpRequest
from django.shortcuts import render, get_object_or_404
from django.views.decorators.http import require_safe
from django.views.generic import TemplateView

from common.queries import has_active_subscription
from projects.models import Project, ProjectFlatPage, FilmProductionCredit, ProjectStatus
from projects.queries import (
    get_production_logs_page,
    get_current_asset,
    get_featured_assets,
    should_show_landing_page,
)
from blog.models import Post


class FilmProductionCreditForm(ModelForm):
    """Manage user production credit."""

    class Meta:
        model = FilmProductionCredit
        fields = ['is_public']
        widgets = {
            'is_public': widgets.CheckboxInput,
        }
        help_texts = {'is_public': 'Include my name in the end credits.'}


@dataclass
class ProjectDisplayObject:
    title: str
    published_date: datetime
    description: str
    thumbnail: object
    url: str
    status: str
    category: str
    is_published: bool
    is_featured: bool

    def get_status_display(self):
        return self.status

    def get_category_display(self):
        return self.category

@require_safe
def project_list(request: HttpRequest) -> HttpResponse:
    """Display all the published projects.

    Can be Projects that are not films, or Posts of category standalone.
    """
    projects = Project.objects.exclude(category=Project.ProjectCategory.FILM).order_by(
        'status', '-release_date'
    )
    posts = Post.objects.filter(category=Post.PostCategory.STANDALONE, date_published__isnull=False)

    # Convert each queryset to a list of `SimpleObject`
    projects_list = [
        ProjectDisplayObject(
            title=obj.title,
            published_date=obj.date_created,
            description=obj.description,
            thumbnail=obj.thumbnail,
            url=obj.url,
            status=obj.get_status_display() if obj.status != ProjectStatus.released else '',
            category=obj.get_category_display() if obj.category != obj.ProjectCategory.OTHER else 'Project',
            is_published=obj.is_published,
            is_featured=obj.is_featured,
        )
        for obj in projects
    ]
    posts_list = [
        ProjectDisplayObject(
            title=obj.title,
            published_date=obj.date_published,
            description=obj.excerpt,
            thumbnail=obj.thumbnail,
            url=obj.url,
            status='',
            category='Article',
            is_published=obj.is_published,
            is_featured=False,
        )
        for obj in posts
    ]

    # Merge and sort the lists
    merged_list_sorted = sorted(
        chain(projects_list, posts_list), key=lambda x: x.published_date, reverse=True
    )

    # Put featured first
    merged_list_sorted = sorted(merged_list_sorted, key=lambda x: x.is_featured, reverse=True)

    context = {
        'projects': merged_list_sorted,
        'user_can_edit_project': (
            request.user.is_staff and request.user.has_perm('projects.change_project')
        ),
    }

    return render(request, 'projects/project_list.html', context)


@require_safe
def film_list(request: HttpRequest) -> HttpResponse:
    context = {
        'projects': (
            Project.objects.filter(category=Project.ProjectCategory.FILM).order_by(
                'status', '-release_date'
            )
        ),
        'user_can_edit_project': (
            request.user.is_staff and request.user.has_perm('projects.change_project')
        ),
    }

    return render(request, 'projects/film_list.html', context)


@require_safe
def project_detail(request: HttpRequest, project_slug: str) -> HttpResponse:
    """
    Display the detail page of the :model:`projects.Project` specified by the given slug.
    """

    project = get_object_or_404(
        Project,
        slug=project_slug,
    )
    featured_artwork = get_featured_assets(project)

    context = {
        'project': project,
        'featured_artwork': featured_artwork,
        'user_can_view_asset': (
            request.user.is_authenticated and has_active_subscription(request.user)
        ),
        'user_can_edit_project': (
            request.user.is_staff and request.user.has_perm('projects.change_project')
        ),
        'user_can_edit_asset': (
            request.user.is_staff and request.user.has_perm('projects.change_asset')
        ),
        'user_has_production_credit': (
            request.user.is_authenticated
            and request.user.production_credits.filter(project=project)
        ),
        'project_blog_posts': project.posts.all()
        if request.user.is_staff
        else project.posts.filter(is_published=True),
        **get_current_asset(request),
    }
    if project.show_production_logs_as_featured:
        context['production_logs_page'] = get_production_logs_page(project)

    template_file = 'projects/project_detail.html'
    if should_show_landing_page(request, project):
        template_file = 'projects/project_landing_page.html'
    return render(request, template_file, context)


@require_safe
def flatpage(request: HttpRequest, project_slug: str, page_slug: str) -> HttpResponse:
    """
    Display the "About" page of the :model:`projects.Project` specified by the given slug.

    **Context:**

    ``project``
        A :model:`projects.Project` instance; the project that the flatpage belongs to.
    ``flatpage``
        A :model:`projects.ProjectFlatPage` instance.
    ``user_can_edit_project``
        A bool specifying whether the current user is able to edit the
        :model:`projects.Project` to which the page belongs.
    ``user_can_edit_flatpage``
        A bool specifying whether the current user is able to edit the
        :model:`projects.ProjectFlatPage` displayed in the page.

    **Template:**

    :template:`projects/flatpage.html`
    """
    project = get_object_or_404(Project, slug=project_slug, is_published=True)
    flatpage = get_object_or_404(ProjectFlatPage, project=project, slug=page_slug)
    context = {
        'project': project,
        'flatpage': flatpage,
        'user_can_edit_project': (
            request.user.is_staff and request.user.has_perm('projects.change_project')
        ),
        'user_can_edit_flatpage': (
            request.user.is_staff and request.user.has_perm('projects.change_projectflatpage')
        ),
        'user_has_production_credit': (
            request.user.is_authenticated
            and request.user.production_credits.filter(project=project)
        ),
    }
    return render(request, 'projects/flatpage.html', context)


class FilmProductionCreditView(LoginRequiredMixin, TemplateView):
    """View to handle visibility of a user credit for the project."""

    template_name = 'projects/production_credit.html'

    def get_project(self, project_slug):  # noqa: D102
        return get_object_or_404(Project, slug=project_slug, is_published=True)

    def get_credit(self, project):  # noqa: D102
        return get_object_or_404(FilmProductionCredit, user=self.request.user, project=project)

    def get_context_data(self, **kwargs):  # noqa: D102
        context = super().get_context_data(**kwargs)
        context['project'] = self.get_project(kwargs['project_slug'])
        context['credit'] = self.get_credit(context['project'])
        context['form'] = FilmProductionCreditForm(instance=context['credit'])
        return context

    def post(self, request, **kwargs):
        """Change is_public flag for this credit."""
        project = self.get_project(kwargs['project_slug'])
        credit = self.get_credit(project)
        if not credit.is_editable:
            return HttpResponseNotAllowed(['GET'], reason='Credit is not editable.')
        form = FilmProductionCreditForm(request.POST, instance=self.get_credit(project))
        form.save()
        messages.add_message(request, messages.INFO, 'Your preference has been saved.')
        return self.render_to_response(context=self.get_context_data(**kwargs))
