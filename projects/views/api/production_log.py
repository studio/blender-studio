from django.http import HttpResponse
from django.http.request import HttpRequest
from django.shortcuts import get_object_or_404, render
from django.views.decorators.http import require_safe

from common.queries import has_active_subscription
from projects.models import Project
from projects.queries import get_production_logs_page


@require_safe
def production_logs_page(request: HttpRequest, project_pk: int) -> HttpResponse:
    project = get_object_or_404(Project, id=project_pk, is_published=True)
    page_number = request.GET.get('page')
    per_page = request.GET.get('per_page')
    context = {
        'user_can_view_asset': (
            request.user.is_authenticated and has_active_subscription(request.user)
        ),
        'project': project,
        'production_logs_page': get_production_logs_page(project, page_number, per_page),
        'show_more_button': True,
    }

    return render(request, 'projects/components/activity_feed.html', context)
