from django.apps import AppConfig


class FilmConfig(AppConfig):
    name = 'projects'

    def ready(self) -> None:
        import projects.signals  # noqa: F401
        from actstream import registry

        registry.register(self.get_model('Asset'))
