from django.test import TestCase
from django.urls import reverse

from common.tests.factories.projects import ProjectFactory
from common.tests.factories.users import UserFactory
from projects.models import Project, FilmProductionCredit


class TestProductionCredit(TestCase):
    def setUp(self) -> None:
        self.project: Project = ProjectFactory()
        self.user = UserFactory()

    def test_project_production_credit_model(self):
        """Assign credit to user and check if stored correctly."""

        FilmProductionCredit.objects.create(user=self.user, project=self.project)

        self.assertEqual(1, self.user.production_credits.count())
        # Ensure that is_public is None by default
        credit = self.user.production_credits.get(project=self.project)
        self.assertEqual(None, credit.is_public)

    def test_user_production_credit_endpoint(self):
        page_url = reverse('production-credit', kwargs={'project_slug': self.project.slug})
        response = self.client.get(page_url)
        self.assertEqual(response.status_code, 302)

        self.client.force_login(self.user)
        # with self.assertTemplateUsed('projects/production_credit.html'):

        # Ensure 404 if user has no credit on the project
        response = self.client.get(page_url)
        self.assertEqual(response.status_code, 404)

        FilmProductionCredit.objects.create(user=self.user, project=self.project)
        with self.assertTemplateUsed('projects/production_credit.html'):
            response = self.client.get(page_url)
            self.assertEqual(response.status_code, 200)
