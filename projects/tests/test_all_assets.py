from django.test import TestCase
from django.urls import reverse

from common.tests.factories.projects import ProjectFactory
from common.tests.factories.users import UserFactory


class TestAllAssets(TestCase):
    maxDiff = None

    def setUp(self):
        self.project = ProjectFactory()
        self.url = reverse('project-all-assets', kwargs={'project_slug': self.project.slug})

    def test_view_not_logged_in(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

    def test_view_logged_in(self):
        user = UserFactory()
        self.client.force_login(user)

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
