import datetime as dt
from unittest.mock import patch

from django.test import TestCase
from django.test.client import RequestFactory
from django.urls.base import reverse

from common.tests.factories.projects import (
    ProjectFactory,
    CollectionFactory,
    AssetFactory,
    ProductionLogFactory,
    ProductionLogEntryFactory,
    ProductionLogEntryAssetFactory,
)
from common.tests.factories.static_assets import StaticAssetFactory
from common.tests.factories.users import UserFactory
from projects.queries import SiteContexts, get_asset_context


class TestSiteContextResolution(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.factory = RequestFactory()
        cls.user = UserFactory()

        cls.asset = AssetFactory()
        cls.other_asset = AssetFactory(project=cls.asset.project, collection=cls.asset.collection)

    @patch('projects.queries.get_next_asset_in_gallery', return_value=None)
    @patch('projects.queries.get_previous_asset_in_gallery', return_value=None)
    def test_gallery_site_context(self, get_previous_asset_mock, get_next_asset_mock):
        query_string = f'site_context={SiteContexts.GALLERY.value}'
        request = self.factory.get(f'{reverse("api-asset", args=(self.asset.pk,))}?{query_string}')
        request.user = self.user
        _ = get_asset_context(self.asset, request)

        get_previous_asset_mock.assert_called_once_with(self.asset)
        get_next_asset_mock.assert_called_once_with(self.asset)

    @patch('projects.queries.get_next_asset_in_featured_artwork', return_value=None)
    @patch('projects.queries.get_previous_asset_in_featured_artwork', return_value=None)
    def test_featured_artwork_site_context(self, get_previous_asset_mock, get_next_asset_mock):
        query_string = f'site_context={SiteContexts.FEATURED_ARTWORK.value}'
        request = self.factory.get(f'{reverse("api-asset", args=(self.asset.pk,))}?{query_string}')
        request.user = self.user
        _ = get_asset_context(self.asset, request)

        get_previous_asset_mock.assert_called_once_with(self.asset)
        get_next_asset_mock.assert_called_once_with(self.asset)

    @patch('projects.queries.get_next_asset_in_production_logs', return_value=None)
    @patch('projects.queries.get_previous_asset_in_production_logs', return_value=None)
    def test_production_logs_site_context(self, get_previous_asset_mock, get_next_asset_mock):
        query_string = f'site_context={SiteContexts.PRODUCTION_LOGS.value}'
        request = self.factory.get(f'{reverse("api-asset", args=(self.asset.pk,))}?{query_string}')
        request.user = self.user
        _ = get_asset_context(self.asset, request)

        get_previous_asset_mock.assert_called_once_with(self.asset)
        get_next_asset_mock.assert_called_once_with(self.asset)

    def test_wrong_site_context(self):
        query_string = 'site_context=definitely-incorrect'
        request = self.factory.get(f'{reverse("api-asset", args=(self.asset.pk,))}?{query_string}')
        request.user = self.user
        context = get_asset_context(self.asset, request)

        self.assertEqual(context['asset'], self.asset)
        self.assertEqual(context['previous_asset'], None)
        self.assertEqual(context['next_asset'], None)

    def test_no_site_context_in_query_string(self):
        request = self.factory.get(reverse("api-asset", args=(self.asset.pk,)))
        request.user = self.user
        context = get_asset_context(self.asset, request)

        self.assertEqual(context['asset'], self.asset)
        self.assertEqual(context['previous_asset'], None)
        self.assertEqual(context['next_asset'], None)


class TestAssetOrderingInGallery(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.site_context = SiteContexts.GALLERY.value
        cls.factory = RequestFactory()
        cls.user = UserFactory()

        project = ProjectFactory()
        cls.collection_a = CollectionFactory(project=project)
        collection_b = CollectionFactory(project=project, order=1)
        collection_c = CollectionFactory(project=project, order=2)

        other_project = ProjectFactory()
        other_collection = CollectionFactory(project=other_project)

        # Assets from collection A: sorted by descending date_published
        cls.asset_a_3 = AssetFactory(
            project=project, collection=cls.collection_a, order=2, name='Aa', is_featured=True
        )
        cls.asset_a_2 = AssetFactory(project=project, collection=cls.collection_a, name='Aa')
        cls.asset_a_1 = AssetFactory(
            project=project, collection=cls.collection_a, order=1, name='Bb'
        )
        cls.asset_a_0 = AssetFactory(
            project=project, collection=cls.collection_a, order=1, name='Cc'
        )

        # Assets from other projects and collections, should not be included in context:
        AssetFactory(project=project, collection=collection_b, is_featured=True)
        AssetFactory(project=project, collection=collection_c, is_featured=True)
        AssetFactory(project=other_project, collection=other_collection)
        AssetFactory(project=other_project, collection=other_collection, is_featured=True)

    def test_previous_asset_for_first_asset_is_none(self):
        first_asset = self.asset_a_0
        request = self.factory.get(
            f'{reverse("api-asset", args=(first_asset.pk,))}?site_context={self.site_context}'
        )
        request.user = self.user
        context = get_asset_context(first_asset, request)

        self.assertEqual(context['asset'], first_asset)
        self.assertEqual(context['previous_asset'], None)
        self.assertEqual(context['site_context'], self.site_context)

    def test_next_asset_for_last_asset_is_none(self):
        last_asset = self.asset_a_3
        request = self.factory.get(
            f'{reverse("api-asset", args=(last_asset.pk,))}?site_context={self.site_context}'
        )
        request.user = self.user
        context = get_asset_context(last_asset, request)

        self.assertEqual(context['asset'], last_asset)
        self.assertEqual(context['next_asset'], None)
        self.assertEqual(context['site_context'], self.site_context)

    def test_assets_from_collection_sorted_by_descending_date_published(self):
        assets = [self.asset_a_0, self.asset_a_1, self.asset_a_2, self.asset_a_3]

        for previous_asset, asset, next_asset in zip(assets, assets[1:], assets[2:]):
            request = self.factory.get(
                f'{reverse("api-asset", args=(asset.pk,))}?site_context={self.site_context}'
            )
            request.user = self.user
            context = get_asset_context(asset, request)

            self.assertEqual(context['previous_asset'], previous_asset)
            self.assertEqual(context['asset'], asset)
            self.assertEqual(context['next_asset'], next_asset)


class TestAssetOrderingInFeaturedArtwork(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.site_context = SiteContexts.FEATURED_ARTWORK.value
        cls.factory = RequestFactory()
        cls.user = UserFactory()

        project = ProjectFactory()
        collection_a = CollectionFactory(project=project)
        collection_b = CollectionFactory(project=project, order=1)
        collection_c = CollectionFactory(project=project, order=2)

        other_project = ProjectFactory()
        other_collection = CollectionFactory(project=other_project)

        # All the featured assets from the project should be sorted by desc date_published:
        cls.featured_asset_0 = AssetFactory(
            project=project, collection=collection_a, order=2, is_featured=True
        )
        AssetFactory(project=project, collection=collection_a, order=1)
        AssetFactory(project=project, collection=collection_a, order=2)
        cls.featured_asset_1 = AssetFactory(
            project=project, collection=collection_b, is_featured=True
        )
        cls.featured_asset_2 = AssetFactory(
            project=project, collection=collection_c, is_featured=True
        )
        AssetFactory(project=project, collection=collection_a, order=3)
        cls.featured_asset_3 = AssetFactory(
            project=project, collection=collection_a, is_featured=True
        )

        # Assets from the other project should not be included in context:
        AssetFactory(project=other_project, collection=other_collection)
        AssetFactory(project=other_project, collection=other_collection, is_featured=True)

    def test_previous_asset_for_first_asset_is_none(self):
        first_asset = self.featured_asset_3  # featured are ordered latest published first
        request = self.factory.get(
            f'{reverse("api-asset", args=(first_asset.pk,))}?site_context={self.site_context}'
        )
        request.user = self.user
        context = get_asset_context(first_asset, request)

        self.assertEqual(context['asset'], first_asset)
        self.assertEqual(context['previous_asset'], None)
        self.assertEqual(context['site_context'], self.site_context)

    def test_next_asset_for_last_asset_is_none(self):
        last_asset = self.featured_asset_0
        request = self.factory.get(
            f'{reverse("api-asset", args=(last_asset.pk,))}?site_context={self.site_context}'
        )
        request.user = self.user
        context = get_asset_context(last_asset, request)

        self.assertEqual(context['asset'], last_asset)
        self.assertEqual(context['next_asset'], None)
        self.assertEqual(context['site_context'], self.site_context)

    def test_featured_assets_sorted_by_date_published(self):
        assets = [
            self.featured_asset_3,
            self.featured_asset_2,
            self.featured_asset_1,
            self.featured_asset_0,
        ]
        for previous_asset, asset, next_asset in zip(assets, assets[1:], assets[2:]):
            request = self.factory.get(
                f'{reverse("api-asset", args=(asset.pk,))}?site_context={self.site_context}'
            )
            request.user = self.user
            context = get_asset_context(asset, request)

            self.assertEqual(context['previous_asset'], previous_asset)
            self.assertEqual(context['asset'], asset)
            self.assertEqual(context['next_asset'], next_asset)


class TestAssetOrderingInProductionLogs(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.site_context = SiteContexts.PRODUCTION_LOGS.value
        cls.factory = RequestFactory()
        cls.user = UserFactory()

        author_a = UserFactory()
        author_b = UserFactory()

        project = ProjectFactory()
        log_start_date = dt.date(2020, 6, 1)
        prod_log = ProductionLogFactory(project=project, start_date=log_start_date)
        entry_a = ProductionLogEntryFactory(production_log=prod_log, author=author_a)

        cls.asset_a_3 = AssetFactory(
            project=project, static_asset=StaticAssetFactory(user=author_a)
        )
        cls.asset_a_2 = AssetFactory(
            project=project, static_asset=StaticAssetFactory(user=author_a)
        )
        cls.asset_a_1 = AssetFactory(
            project=project, static_asset=StaticAssetFactory(user=author_a)
        )
        cls.asset_a_0 = AssetFactory(
            project=project, static_asset=StaticAssetFactory(user=author_a)
        )

        # Author's A assets should be sorted by descending date_published
        ProductionLogEntryAssetFactory(production_log_entry=entry_a, asset=cls.asset_a_0)
        ProductionLogEntryAssetFactory(production_log_entry=entry_a, asset=cls.asset_a_1)
        ProductionLogEntryAssetFactory(production_log_entry=entry_a, asset=cls.asset_a_2)
        ProductionLogEntryAssetFactory(production_log_entry=entry_a, asset=cls.asset_a_3)

        # Assets from other production log entries should not be included in the context
        other_asset_a = AssetFactory(
            project=project, static_asset=StaticAssetFactory(user=author_a)
        )
        other_entry = ProductionLogEntryFactory(
            user=author_a, production_log=ProductionLogFactory(start_date=dt.date(1997, 12, 1))
        )
        ProductionLogEntryAssetFactory(production_log_entry=other_entry, asset=other_asset_a)
        asset_b = AssetFactory(project=project, static_asset=StaticAssetFactory(user=author_b))
        entry_b = ProductionLogEntryFactory(production_log=prod_log, author=author_b)
        ProductionLogEntryAssetFactory(production_log_entry=entry_b, asset=asset_b)

    def test_previous_asset_for_first_asset_is_none(self):
        first_asset = self.asset_a_0
        request = self.factory.get(
            f'{reverse("api-asset", args=(first_asset.pk,))}?site_context={self.site_context}'
        )
        request.user = self.user
        context = get_asset_context(first_asset, request)

        self.assertEqual(context['asset'], first_asset)
        self.assertEqual(context['previous_asset'], None)
        self.assertEqual(context['site_context'], self.site_context)

    def test_next_asset_for_last_asset_is_none(self):
        last_asset = self.asset_a_3
        request = self.factory.get(
            f'{reverse("api-asset", args=(last_asset.pk,))}?site_context={self.site_context}'
        )
        request.user = self.user
        context = get_asset_context(last_asset, request)

        self.assertEqual(context['asset'], last_asset)
        self.assertEqual(context['next_asset'], None)
        self.assertEqual(context['site_context'], self.site_context)

    def test_assets_in_production_log_entry_sorted_by_desc_date_published(self):
        assets = [
            self.asset_a_0,
            self.asset_a_1,
            self.asset_a_2,
            self.asset_a_3,
        ]
        for previous_asset, asset, next_asset in zip(assets, assets[1:], assets[2:]):
            request = self.factory.get(
                f'{reverse("api-asset", args=(asset.pk,))}?site_context={self.site_context}'
            )
            request.user = self.user
            context = get_asset_context(asset, request)

            self.assertEqual(context['previous_asset'], previous_asset)
            self.assertEqual(context['asset'], asset)
            self.assertEqual(context['next_asset'], next_asset)
