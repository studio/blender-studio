from django.db.utils import IntegrityError
from django.test.testcases import TestCase
from django.urls import reverse

from common.tests.factories.projects import ProjectFactory, ProjectFlatPageFactory
from projects.models import ProjectFlatPage


class TestFilmFlatPageModel(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.project_1 = ProjectFactory()
        cls.project_2 = ProjectFactory()
        cls.flatpage_data = {'project': cls.project_1, 'title': 'About', 'content': '# hello world'}

    def test_slug_and_content_html_created_on_save(self):
        flatpage = ProjectFlatPage.objects.create(
            project=self.project_1, title='New Page', content='# hello world'
        )
        self.assertEqual(flatpage.slug, 'new-page')
        self.assertHTMLEqual(flatpage.content_html, '<h1>hello world</h1>')

    def test_slug_created_only_if_not_provided(self):
        custom_slug = 'custom'
        flatpage = ProjectFlatPage.objects.create(
            project=self.project_1, title='New Page', slug=custom_slug, content='# hello world'
        )
        self.assertEqual(flatpage.slug, custom_slug)

    def test_create_pages_with_same_slugs_for_different_projects(self):
        assert self.project_1.slug != self.project_2.slug
        try:
            page_1 = ProjectFlatPage.objects.create(
                project=self.project_1, title='New Page', content='# hello world'
            )
            page_2 = ProjectFlatPage.objects.create(
                project=self.project_2, title='New Page', content='# hello world'
            )
        except IntegrityError as err:
            self.fail(
                f'It should be possible to create flat pages with the same slug '
                f'and different project slug.\n Original error: {err}'
            )
        self.assertEqual(page_1.slug, page_2.slug)

    def test_cannot_create_pages_with_same_slug_and_project_slug(self):
        with self.assertRaises(IntegrityError):
            ProjectFlatPage.objects.create(**self.flatpage_data)
            ProjectFlatPage.objects.create(**self.flatpage_data)

    def test_content_html_updated_on_save(self):
        page = ProjectFlatPage.objects.create(**self.flatpage_data)
        self.assertHTMLEqual(page.content_html, '<h1>hello world</h1>')
        page.content = '## Updated content'
        page.save()
        self.assertHTMLEqual(page.content_html, '<h2>Updated content</h2>')


class TestFilmFlatPage(TestCase):
    def setUp(self) -> None:
        self.project_slug = 'coffee-run'
        self.page_slug = 'about'
        self.content = '## A very specific flat page'
        self.project = ProjectFactory(slug=self.project_slug)
        self.page = ProjectFlatPageFactory(
            project=self.project, slug=self.page_slug, content=self.content
        )

    def test_flatpage_endpoint(self):
        page_url = reverse(
            'project-flatpage',
            kwargs={'project_slug': self.project_slug, 'page_slug': self.page_slug},
        )
        with self.assertTemplateUsed('projects/flatpage.html'):
            response = self.client.get(page_url)

            self.assertEqual(response.status_code, 200)
            self.assertInHTML(
                '<h2>A very specific flat page</h2>', response.content.decode('utf-8')
            )
