from django.test import TestCase
from unittest.mock import patch, call

from common.tests.factories.projects import ProjectFactory
from common.tests.factories.users import UserFactory
from projects.models import Project


class TestFilmModel(TestCase):
    def test_project_create_with_crew_member(self):
        """Tests for the ProjectCrew model.

        Create a new project, assign a new user with an 'Artist' role and
        ensure that it's available as part of project.projectcrew_set
        """

        expected_role = 'Artist'
        project: Project = ProjectFactory()
        author_a = UserFactory()
        project.crew.add(author_a, through_defaults={'role': expected_role})
        self.assertIn(author_a, project.crew.all())
        self.assertEqual(expected_role, project.projectcrew_set.get(user=author_a).role)

    @patch('storages.backends.s3boto3.S3Boto3Storage.url', return_value='s3://file')
    def test_project_storage_urls(self, mock_storage_url):
        """Tests for the Project image assets.

        Create a new project and check that the right storage is called when its image URLs are accessed.
        """
        project: Project = ProjectFactory()

        self.assertEqual(project.logo.url, 's3://file')
        self.assertEqual(project.poster.url, 's3://file')
        self.assertEqual(project.picture_header.url, 's3://file')
        self.assertEqual(project.thumbnail.url, 's3://file')

        mock_storage_url.assert_has_calls(
            (
                call(
                    project.logo.name,
                ),
                call(
                    project.poster.name,
                ),
                call(
                    project.picture_header.name,
                ),
                call(
                    project.thumbnail.name,
                ),
            )
        )
