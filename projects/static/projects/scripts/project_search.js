/* eslint-disable no-undef, no-unused-vars, no-nested-ternary */
const searchClientConfig = JSON.parse(document.getElementById('search-client-config').textContent);
const projectTitle = JSON.parse(document.getElementById('project-title').textContent);

const indexName = 'studio_date_desc';
const search = instantsearch({
  indexName,
  searchClient: instantMeiliSearch(searchClientConfig.hostUrl, searchClientConfig.apiKey),
  routing: {
    stateMapping: {
      stateToRoute(uiState) {
        const indexUiState = uiState[indexName];
        return {
          query: indexUiState.query,
          sortBy: indexUiState && indexUiState.sortBy,
          media_type: indexUiState.menu && indexUiState.menu.media_type,
        };
      },
      routeToState(routeState) {
        return {
          [indexName]: {
            query: routeState.query,
            sortBy: routeState.sortBy,
            menu: {
              media_type: routeState.media_type,
            },
          },
        };
      },
    },
  },
});

// -------- INPUT -------- //

// Create a render function
const renderSearchBox = (renderOptions, isFirstRender) => {
  const { query, refine, clear } = renderOptions;

  if (isFirstRender) {
    const input = document.getElementById('searchInput');
    input.value = query;
    const clearButton = document.getElementById('clearSearchBtn');
    let typingTimer;
    input.addEventListener('input', (event) => {
      clearTimeout(typingTimer); // Clear the previous timer
      typingTimer = setTimeout(() => {
        refine(input.value); // Call refine after delay
      }, 200);
    });
    clearButton.addEventListener('click', () => {
      clear();
      input.value = '';
    });
  }
};

// create custom widget
const customSearchBox = instantsearch.connectors.connectSearchBox(renderSearchBox);

// -------- SORTING -------- //

// Create the render function
const renderSortBy = (renderOptions, isFirstRender) => {
  const { options, currentRefinement, hasNoResults, refine, widgetParams } = renderOptions;

  if (isFirstRender) {
    const select = document.createElement('select');
    select.setAttribute('class', 'form-select');

    select.addEventListener('change', (event) => {
      refine(event.target.value);
    });

    widgetParams.container.insertAdjacentElement('beforeend', select);
  }

  const select = widgetParams.container.querySelector('select');

  select.disabled = hasNoResults;

  select.innerHTML = `
    ${options
      .map(
        (option) => `
          <option
            value="${option.value}"
            ${option.value === currentRefinement ? 'selected' : ''}
          >
            ${option.label}
          </option>
        `
      )
      .join('')}
  `;
};

// Create the custom widget
const customSortBy = instantsearch.connectors.connectSortBy(renderSortBy);

// -------- HITS -------- //

let lastRenderArgs;

// Create the render function
const renderHits = (renderOptions, isFirstRender) => {
  const { hits, showMore, widgetParams } = renderOptions;

  /* Debug. */
  // hits.map((item) => console.log(item));

  widgetParams.container.innerHTML = `
      ${hits
        .map(
          (item) =>
            `
            <div class="card-grid-item cards-item">
              <a aria-label="${item.name}" href="${item.url}" class="cards-item-content">
                <div class="cards-item-thumbnail">
                  ${
                  item.thumbnail_url
                    ? `<img alt="" aria-label="${item.name}" loading=lazy src="${item.thumbnail_url}">`
                    : `<div class="cards-item-thumbnail-icon"><i class="i-mediatype-${item.media_type}"></i></div>`
                  }
                  ${
                  item.contains_blend_file
                    ? ` <div class="cards-item-thumbnail-icon-overlay"><i class="i-blender"></i></div>`
                    : ''
                  }
                </div>
                <h3 class="cards-item-title fs-6 lh-base" data-tooltip="tooltip-overflow" data-placement="top" title="${item.name}">
                  <span class="overflow-text">
                    ${instantsearch.highlight({
                      attribute: 'name',
                      hit: item,
                    })}
                  </span>
                </h3>
                <div class="cards-item-extra">
                  <ul class="list-inline w-100">
                    <li>${item.media_type}</li>
                    <li>
                      ${
                        item.is_free
                          ? `<span class="badge badge-pill">Free</span>`
                          : ''
                      }
                    </li>
                    <li class="ms-auto">${epochToFormattedDate(item.timestamp)}</li>
                  </ul>
                </div>
              </a>
            </div>
            `
        )
        .join('')}
  `;

  lastRenderArgs = renderOptions;

  if (isFirstRender) {
    const sentinel = document.createElement('div');
    widgetParams.container.insertAdjacentElement('afterend', sentinel);

    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting && !lastRenderArgs.isLastPage) {
          showMore();
        }
      });
    });

    observer.observe(sentinel);

    // When the entirety of the first page and the sentinel fit into the viewport, observer doesn't fire.
    // This checks if the sentinel is **still** within the viewport and calls showMore,
    // otherwise the next page is never shown unless the browser window is resized.
    // This way showMore will be called at the end of one or more renders, for example
    // if someone is viewing the search page on a large vertical monitor.
    search.on('render', function() {
      const shouldShowMoreAlready = isElementInViewport(sentinel);
      const infiniteHits = search.renderState[search.indexName].infiniteHits;
      if (shouldShowMoreAlready && !infiniteHits.isLastPage) {
        infiniteHits.showMore();
      };
    });
  }
};

const customHits = instantsearch.connectors.connectInfiniteHits(renderHits);

// -------- FILTERS -------- //

// 1. Create a render function
const renderMenuSelect = (renderOptions, isFirstRender) => {
  const { items, canRefine, refine, widgetParams } = renderOptions;

  if (isFirstRender) {
    const select = document.createElement('select');

    select.setAttribute('class', 'form-control');
    select.addEventListener('change', (event) => {
      refine(event.target.value);
    });

    widgetParams.container.insertAdjacentElement('afterbegin', select);
    // widgetParams.container.appendChild(select);
  }

  const select = widgetParams.container.querySelector('select');

  select.disabled = !canRefine;

  select.innerHTML = `
    <option value="">${widgetParams.placeholder}</option>
    ${items
      .map(
        (item) =>
          `<option
            value="${item.value}"
            ${item.isRefined ? 'selected' : ''}
          >
            ${titleCase(item.label)}
          </option>`
      )
      .join('')}
  `;
};

// 2. Create the custom widget
const customMenuSelect = instantsearch.connectors.connectMenu(renderMenuSelect);

// -------- CONFIGURE -------- //

const renderConfigure = (renderOptions, isFirstRender) => {};

const customConfigure = instantsearch.connectors.connectConfigure(renderConfigure, () => {});

// -------- RENDER -------- //

search.addWidgets([
  customSearchBox({
    container: document.querySelector('#search-container'),
  }),
  customHits({
    container: document.querySelector('#hits'),
  }),
  customSortBy({
    container: document.querySelector('#sorting'),
    items: [
      { label: 'Relevance', value: 'studio' },
      { label: 'Date (new first)', value: 'studio_date_desc' },
      { label: 'Date (old first)', value: 'studio_date_asc' },
    ],
  }),
  customMenuSelect({
    container: document.querySelector('#searchMedia'),
    attribute: 'media_type',
    placeholder: 'All Types',
  }),
  customConfigure({
    container: document.querySelector('#hits'),
    searchParameters: {
      project_title: projectTitle,
      hitsPerPage: 12,
      disjunctiveFacetsRefinements: {
        project_title: [projectTitle.project],
        model: ['asset'],
      },
    },
  }),
]);

search.start();

document.addEventListener('DOMContentLoaded', () => {
  document.querySelector('#searchInput').focus();
});
