import logging

from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from common import markdown
from projects.models.assets import Asset, AssetComment, Like
from projects.models.collections import Collection
from users.queries import create_action_from_like

logger = logging.getLogger(__name__)


@receiver(pre_save, sender=Asset)
def clean_asset_description(sender: object, instance: Asset, raw: bool, **kwargs: object) -> None:
    """Clean asset description."""
    if raw:
        return
    instance.description = markdown.sanitize(instance.description)


@receiver(pre_save, sender=Collection)
def clean_collection_text(
    sender: object, instance: Collection, raw: bool, **kwargs: object
) -> None:
    """Clean collection text."""
    if raw:
        return
    instance.text = markdown.sanitize(instance.text)


@receiver(post_save, sender=AssetComment)
def notify_about_comment(
    sender: object, instance: AssetComment, created: bool, raw: bool, **kwargs: object
) -> None:
    """
    Generate notifications about comments under project assets.

    Because asset <-> comment relation uses a custom through model (AssetComment),
    project asset is not accessible in post_save of a Comment, only in post_save of the through model.
    """
    if raw:
        return
    if not created:
        return

    instance.comment.create_action()


@receiver(post_save, sender=Like)
def notify_about_like(
    sender: object, instance: Like, created: bool, raw: bool, **kwargs: object
) -> None:
    """Generate notifications about asset likes."""
    if raw:
        return
    if not created:
        return

    target = instance.asset
    # Don't notify when one likes their own blog post
    asset_author = target.static_asset.author or target.static_asset.user
    if instance.user == asset_author:
        return

    create_action_from_like(actor=instance.user, target=target)
