from django.urls import path, include

from projects.views import project, gallery, production_log
from projects.views.api.asset import asset as api_asset, asset_zoom, asset_like
from projects.views.api.comment import comment

urlpatterns = [
    path(
        'api/assets/<int:asset_pk>/',
        include(
            [
                path('', api_asset, name='api-asset'),
                path('zoom/', asset_zoom, name='api-asset-zoom'),
                path('comment/', comment, name='api-asset-comment'),
                path('like/', asset_like, name='api-asset-like'),
            ]
        ),
    ),
    path('', project.project_list, name='project-list'),
    path('<slug:project_slug>/', project.project_detail, name='project-detail'),
    path('<slug:project_slug>/gallery/', gallery.collection_list, name='project-gallery'),
    path(
        '<slug:project_slug>/production-credit/',
        project.FilmProductionCreditView.as_view(),
        name='production-credit',
    ),
    path(
        '<slug:project_slug>/production-logs/',
        production_log.ProductionLogPaginatedView.as_view(),
        name='production-logs',
    ),
    path(
        '<slug:project_slug>/production-logs/<int:year>/<str:month>/',
        production_log.ProductionLogMonthView.as_view(),
        name='production-logs-month',
    ),
    path(
        '<slug:project_slug>/production-log/<int:pk>/',
        production_log.ProductionLogDetailView.as_view(),
        name='production-log',
    ),
    path(
        '<slug:project_slug>/all-artwork/', gallery.AllAssets.as_view(), name='project-all-assets'
    ),
    path('<slug:project_slug>/pages/<slug:page_slug>/', project.flatpage, name='project-flatpage'),
    path(
        '<slug:project_slug>/<slug:collection_slug>/',
        gallery.collection_detail,
        name='collection-detail',
    ),
]
