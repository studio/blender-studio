"""Handy inclusion tags for project templates."""
from django import template

register = template.Library()


@register.inclusion_tag('projects/components/breadcrumbs.html', takes_context=True)
def show_breadcrumbs(context):
    """Creates a breadcrumb navigation for :template:`projects/gallery.html` and

    :template:`projects/collection_detail.html`.

    Breadcrumbs have links to the project and the collection hierarchy above the current
    collection. The current location name is also included, but without a link.

    **Tag template:**

    :template:`projects/components/breadcrumbs.html`
    """
    breadcrumbs = []
    project = context['project']
    collection = context.get('current_collection')
    if collection:
        current_location = collection.name

        while collection.parent:
            breadcrumbs.append((collection.parent.name, collection.parent.url))
            collection = collection.parent

        breadcrumbs.append((project.title, project.url))
    else:
        current_location = project.title

    extra_context = {
        key: context.get(key)
        for key in (
            'current_collection',
            'featured_artwork',
            'project',
            'perms',
            'request',
            'user_can_edit_asset',
            'user_can_edit_collection',
            'user_can_edit_project',
            'user_can_view_asset',
        )
    }
    return {
        'breadcrumbs': breadcrumbs[::-1],
        'current_location': current_location,
        **extra_context,
    }
