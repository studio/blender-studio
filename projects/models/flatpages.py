from typing import Any

from django.db import models
from django.urls import reverse
from django.utils.text import slugify

from common import mixins, markdown
import common.help_texts
import static_assets.models as models_static_assets
from projects.models import Project


class ProjectFlatPage(mixins.CreatedUpdatedMixin, models.Model):
    """Stores a single project-related flat page."""

    class Meta:
        constraints = [
            # The slug and project slug are used in the page url, which has to be unique.
            models.UniqueConstraint(
                fields=['slug', 'project'], name='unique_project_flat_page_url'
            ),
        ]

    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='flatpages')
    title = models.CharField(
        'Page title',
        max_length=50,
        help_text='It will be displayed as the section name in the navigation bar.',
    )
    slug = models.SlugField(
        'Page slug',
        blank=True,
        help_text=(
            'The page slug has to be unique per project. '
            'If it is not filled, it will be the slugified page title.'
        ),
    )
    content = models.TextField(blank=True, help_text=common.help_texts.markdown_with_html)
    content_html = models.TextField(blank=True, editable=False)
    attachments = models.ManyToManyField(models_static_assets.StaticAsset, blank=True)

    def save(self, *args: Any, **kwargs: Any) -> None:
        """Generates the html version of the content and saves the object."""
        if not self.slug:
            self.slug = slugify(self.title)
        # Clean but preserve some of the HTML tags
        self.content = markdown.clean(self.content)
        self.content_html = markdown.render_unsafe(self.content)
        super().save(*args, **kwargs)

    def __str__(self):
        return f'Flat page "{self.title}" of the project {self.project.title}'

    def get_absolute_url(self) -> str:
        return self.url

    @property
    def url(self) -> str:
        return reverse(
            'project-flatpage', kwargs={'project_slug': self.project.slug, 'page_slug': self.slug}
        )

    @property
    def admin_url(self) -> str:
        return reverse('admin:projects_projectflatpage_change', args=[self.pk])
