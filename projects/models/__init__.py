# We need to import all our Models here to make sure Django detects them.
from projects.models.collections import *
from projects.models.assets import *
from projects.models.projects import *
from projects.models.flatpages import *
from projects.models.production_logs import *
