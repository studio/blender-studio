from django.contrib import admin

from projects.models import flatpages
from common import mixins


@admin.register(flatpages.ProjectFlatPage)
class ProjectFlatPageAdmin(mixins.ViewOnSiteMixin, admin.ModelAdmin):
    autocomplete_fields = ['project', 'attachments']
    list_display = ('title', 'project', 'view_link')
    list_filter = [
        'project',
    ]
    prepopulated_fields = {'slug': ('slug',)}
