# noqa: D100

from datetime import datetime
from dateutil.relativedelta import relativedelta

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q

from projects.models import Project, FilmProductionCredit
import looper.models


class Command(BaseCommand):
    help = 'Generate production credits for a project.'

    def add_arguments(self, parser):
        parser.add_argument('project_slug', type=str, help='Project slug, e.g. sprite-fright')
        parser.add_argument(
            '--start_date', type=str, help='Date formatted YYYY-MM-DD', required=False
        )

    def generate_credits(self, options):
        """Populate Production Credits list for a project.

        Fetch all active subscribers and ensure that they kept their subscription active
        from a specific moment in time until now. If so, add grant them a Production Credit.
        """
        try:
            project = Project.objects.get(slug=options['project_slug'])
        except Project.DoesNotExist:
            raise CommandError('Project "%s" does not exist' % options['project_slug'])
        start_date_str = options['start_date']
        start_date = (
            start_date_str
            and datetime.strptime(start_date_str, '%Y-%m-%d').date()
            or project.date_created.date()
        )  # maybe use a date of first production log instead?
        end_date = project.release_date or datetime.today()

        # rough prefilter
        subscriptions = (
            looper.models.Subscription.objects.filter(
                Q(cancelled_at__gt=end_date) | Q(cancelled_at__isnull=True),
                started_at__lte=start_date,
                price__gt=0,
            )
            .prefetch_related('order_set')
            .order_by('id')
            .all()
        )
        credits = []
        MAX_GAP_DAYS = 10
        for subscription in subscriptions:
            if self.fully_paid_between_dates(subscription, start_date, end_date, MAX_GAP_DAYS):
                credits.append(FilmProductionCredit(project=project, user=subscription.user))
        self.stdout.write(self.style.SUCCESS(f"Found valid credits: {len(credits)}, updating db"))
        FilmProductionCredit.objects.bulk_create(credits, batch_size=500, ignore_conflicts=True)

    def handle(self, *args, **options):
        self.generate_credits(options)

    def fully_paid_between_dates(self, subscription, start_date, end_date, max_gap_days):
        """Decide if a subscription was active during a given time period.

        Activity is calculated from paid orders, considering every day in an interval
        [order.paid_at, order.paid_at + interval_length) as paid.
        interval_length here accounts for yearly and monthly subscriptions, and for orders that
        extend subscriptions by an irregular interval.

        Small gaps in activity are tolerated, as defined by max_gap_days parameter.
        """
        if subscription.started_at.date() > start_date:
            return False
        if subscription.cancelled_at is not None and subscription.cancelled_at.date() < end_date:
            return False

        interval_length = subscription.interval_length
        if subscription.interval_unit == 'year':
            interval_length *= 12
        orders = subscription.order_set.filter(paid_at__isnull=False).order_by('paid_at').all()
        paid_until = None
        for order in orders:
            paid_at = order.paid_at.date()
            # due to tax calculation order price may be lower than subscription price
            # but for extend_subscription orders the order price is higher than subscription price
            paid_for_months = interval_length * max(1, order.price.cents / subscription.price.cents)
            # see the reasoning in looper.models.Subscription.extend_subscription
            paid_for_days = int(30.4 * (paid_for_months % 1))
            paid_for_months = int(paid_for_months)

            # iterated through enough orders: success exit condition
            if paid_until is not None and paid_until >= end_date:
                return True

            # found a significant gap within given dates: failure exit condition
            if paid_at > start_date and (
                paid_until is None
                or (start_date - paid_until).days > max_gap_days
                or (paid_at - paid_until).days > max_gap_days
            ):
                return False

            # bump paid_until
            paid_until = paid_at + relativedelta(months=paid_for_months, days=paid_for_days)
        return False
