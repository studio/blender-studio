#!/bin/sh -ex

FORCE=$2

git fetch origin main:production
git push origin production

# Check if the '--force' argument is provided
EXTRA_VARS=""
if [ "$FORCE" = "--force" ]; then
  EXTRA_VARS="-e override_lock=true"
fi

pushd playbooks
source .venv/bin/activate
# Append $EXTRA_VARS to the ansible command if --force is provided
./ansible.sh -i environments/production shared/deploy.yaml $EXTRA_VARS
deactivate
popd
