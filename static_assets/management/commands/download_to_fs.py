# noqa: D100
import logging
import os.path

from django.core.files.storage import FileSystemStorage
from django.core.management.base import BaseCommand

from static_assets.models.static_assets import StaticAsset

file_system_storage = FileSystemStorage()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Command(BaseCommand):
    """Download static asset files of given IDs to file system storage."""

    help = "Download static asset files of given IDs to file system storage."

    def add_arguments(self, parser):
        """Add range of IDs to command options."""
        parser.add_argument('--min-id', type=int)
        parser.add_argument('--mex-id', type=int)

    def handle(self, *args, **options):  # noqa: D102
        id_min = options['min_id']
        id_mex = options['mex_id']
        for sa in StaticAsset.objects.filter(id__gte=id_min, id__lt=id_mex).order_by('id'):
            # TODO: optionally also update `source_storage` field with `fs`
            self._download_to_file_system_storage(sa)

    def _download_to_file_system_storage(self, sa: StaticAsset):
        if sa.thumbnail:
            self._save(sa.thumbnail, prefix='public')
        try:
            video = sa.video
            for variation in video.variations.all():
                self._save(variation.source)
            for track in video.tracks.all():
                self._save(track.source)
        except StaticAsset.video.RelatedObjectDoesNotExist:
            pass
        # sa.image has no extra files
        self._save(sa.source)

    def _save(self, field, prefix=''):
        output_path = os.path.join(prefix, field.name)
        logger.info('Downloading %s to path %s', field, output_path)
        if file_system_storage.exists(output_path):
            logger.warning('%s exists', output_path)
            return
        f = field.open()
        file_system_storage.save(output_path, f)
        f.close()
        logger.info('Downloaded %s to path %s', field, output_path)
