/* eslint-disable no-undef, no-unused-vars, no-nested-ternary */
const searchClientConfig = JSON.parse(
  document.getElementById('training-search-client-config').textContent
);
const favoritedTrainingIDs = JSON.parse(
  document.getElementById('training-favorited-ids').textContent
);
const search = instantsearch({
  indexName: 'training_date_desc',
  searchClient: instantMeiliSearch(searchClientConfig.hostUrl, searchClientConfig.apiKey),
  routing: true,
});

// -------- INPUT -------- //

// Create a render function
const renderSearchBox = (renderOptions, isFirstRender) => {
  const { query, refine, clear } = renderOptions;

  if (isFirstRender) {
    const input = document.getElementById('searchInput');
    input.value = query;
    const clearButton = document.getElementById('clearSearchBtn');
    let typingTimer;
    input.addEventListener('input', (event) => {
      clearTimeout(typingTimer); // Clear the previous timer
      typingTimer = setTimeout(() => {
        refine(input.value); // Call refine after delay
      }, 200);
    });
    clearButton.addEventListener('click', () => {
      clear();
      search.helper.clearRefinements('tags').search();
      input.value = '';
    });
  }
};

// create custom widget
const customSearchBox = instantsearch.connectors.connectSearchBox(renderSearchBox);

// -------- SORTING -------- //

// Create the render function
const renderSortBy = (renderOptions, isFirstRender) => {
  const { options, currentRefinement, hasNoResults, refine, widgetParams } = renderOptions;

  if (isFirstRender) {
    const select = document.createElement('select');
    select.setAttribute('class', 'form-control');

    select.addEventListener('change', (event) => {
      refine(event.target.value);
    });

    widgetParams.container.insertAdjacentElement('afterbegin', select);
  }

  const select = widgetParams.container.querySelector('select');

  select.disabled = hasNoResults;

  select.innerHTML = `
    ${options
      .map(
        (option) => `
          <option
            value="${option.value}"
            ${option.value === currentRefinement ? 'selected' : ''}
          >
            ${option.label}
          </option>
        `
      )
      .join('')}
  `;
};

// Create the custom widget
const customSortBy = instantsearch.connectors.connectSortBy(renderSortBy);

// -------- HITS -------- //

let lastRenderArgs;

// Create the render function
const renderHits = (renderOptions, isFirstRender) => {
  const { hits, showMore, widgetParams } = renderOptions;

  /* Debug. */
  // hits.map((item) => console.log(item));

  widgetParams.container.innerHTML = `
      ${hits
        .map(
          (item) =>
            `
<div class="card-training cards-item">
  <a href="${item.url}" class="cards-item-content" data-training-id="${item.id}" data-favorite-url="${item.favorite_url}" ${favoritedTrainingIDs.filter((i) => i === item.id).length > 0 ? 'data-checked="checked"' : ''}>
      <div class="cards-item-thumbnail">
        <img alt="${item.name}" src="${item.thumbnail_url}" loading="lazy">
      </div>

      ${item.project_title ? '<div class="cards-item-headline">' + item.project_title + '</div>' : ''}
      <h3 class="cards-item-title fs-base">${instantsearch.highlight({ attribute: 'name', hit: item })}</h3>
      <div class="cards-item-extra">
        <ul class="list-inline w-100">
          ${item.type ? `<li>${titleCase(item.type)}</li>`: ''}
          <li>${item.is_free ? `<span class="badge">Free</span>` : ''}</li>
          <li class="ms-auto">${item.difficulty ? `<i class="i-sliders"></i>&nbsp;${titleCase(item.difficulty)}` : ''}</li>
        </ul>
      </div>
  </a>
</div>
        `
        )
        .join('')}`;

  lastRenderArgs = renderOptions;

  if (isFirstRender) {
    const sentinel = document.createElement('div');
    widgetParams.container.insertAdjacentElement('afterend', sentinel);

    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting && !lastRenderArgs.isLastPage) {
          showMore();
        }
      });
    });

    observer.observe(sentinel);

    // When the entirety of the first page and the sentinel fit into the viewport, observer doesn't fire.
    // This checks if the sentinel is **still** within the viewport and calls showMore,
    // otherwise the next page is never shown unless the browser window is resized.
    // This way showMore will be called at the end of one or more renders, for example
    // if someone is viewing the search page on a large vertical monitor.
    search.on('render', function() {
      const shouldShowMoreAlready = isElementInViewport(sentinel);
      const infiniteHits = search.renderState[search.indexName].infiniteHits;
      if (shouldShowMoreAlready && !infiniteHits.isLastPage) {
        infiniteHits.showMore();
      };
    });

    return;
  }

  // On search is dispatches this event to setup the training cards (event listeners etc)
  document.dispatchEvent(new Event('trainingResults'));
};

const customHits = instantsearch.connectors.connectInfiniteHits(renderHits);

// -------- FILTERS -------- //

// 1. Create a render function
const renderMenuSelect = (renderOptions, isFirstRender) => {
  const { items, canRefine, refine, widgetParams } = renderOptions;

  if (isFirstRender) {
    const select = document.createElement('select');

    select.setAttribute('class', 'form-control');
    select.addEventListener('change', (event) => {
      refine(event.target.value);
    });

    widgetParams.container.insertAdjacentElement('afterbegin', select);
    // widgetParams.container.appendChild(select);
  }

  const select = widgetParams.container.querySelector('select');

  select.disabled = !canRefine;

  select.innerHTML = `
    <option value="">${widgetParams.placeholder}</option>
    ${items
      .map(
        (item) =>
          `<option
            value="${item.value}"
            ${item.isRefined ? 'selected' : ''}
          >
            ${titleCase(item.label)}
          </option>`
      )
      .join('')}
  `;
};

// 2. Create the custom widget
const customMenuSelect = instantsearch.connectors.connectMenu(renderMenuSelect);

// -------- CONFIGURE -------- //

const renderConfigure = (renderOptions, isFirstRender) => {};

const customConfigure = instantsearch.connectors.connectConfigure(renderConfigure, () => {});

// -------- RENDER -------- //
search.addWidgets([
  customSearchBox({
    container: document.querySelector('#search-container'),
  }),
  customHits({
    container: document.querySelector('#hits'),
  }),
  customMenuSelect({
    container: document.querySelector('#search_type'),
    attribute: 'type',
    placeholder: 'All Types',
  }),
  customMenuSelect({
    container: document.querySelector('#search_difficulty'),
    attribute: 'difficulty',
    placeholder: 'All Difficulties',
  }),
  customSortBy({
    container: document.querySelector('#sorting'),
    items: [
      { label: 'Date (new first)', value: 'training_date_desc' },
      { label: 'Relevance', value: 'training' },
      { label: 'Date (old first)', value: 'training_date_asc' },
    ],
  }),
  instantsearch.widgets.refinementList({
    container: '#tags-refinement',
    attribute: 'tags',
    operator: 'and', // Use 'or' for broader filtering
    sortBy: ['count:desc'],
    limit: 20, // Number of tags to display initially
    showMore: true, // Enable "Show more" option
    showMoreLimit: 40, // Total number of tags to show after clicking "Show more"
  }),
]);

search.start();

document.addEventListener('DOMContentLoaded', () => {
  document.querySelector('#searchInput').focus();
});
