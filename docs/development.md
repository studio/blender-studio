---
gitea: none
include_toc: true
---

# Development

## Requirements

* [git](https://git-scm.com/)
* Python 3.10
* [venv](https://docs.python.org/3.10/library/venv.html)
* [PostgreSQL](https://www.postgresql.org/download/) (tested with PostgreSQL 14 and 16)

## Set up instructions

Clone the repo:

    git clone git@projects.blender.org:studio/blender-studio.git

Create and activate a virtual environment using your favourite method, e.g.

    python3.10 -m venv .venv
    source .venv/bin/activate

Install required packages:

    pip install -r requirements_dev.txt

Create a PostgreSQL user named `studio`:

    sudo -u postgres createuser -d -l -P studio

Create a database named `studio`:

    sudo -u postgres createdb -O studio studio

Make a copy of example `.env`:

    cp .env.example .env

Initialise submodules included into this repo:

    git submodule update --init

Fill in AWS S3 and CloudFront credentials in your `.env`:
* Set values of `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` values to your access keys;
* Download the CloudFront key file and save it to the project directory (it should be named `pk-APK***.pem`);
* Set `AWS_CLOUDFRONT_KEY_ID='APK***'` where `APK***` is from the name of the key file above.

In the project folder, run migrations and load additional plans data:

    ./manage.py migrate
    ./manage.py loaddata default_site licenses template_variables groups gateways plans team_plans

Add `studio.local` to `/etc/hosts` as an alias of 127.0.0.1:

    127.0.0.1    localhost studio.local  # studio.local can be added on the same line as localhost

Create a superuser:

    echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@example.com', 'password')" | python manage.py shell

Run the server:

    ./manage.py runserver 8001

The project will be available at http://studio.local:8001.

(Optional) Install pre-commit hooks (see [pre-commit details](docs/development.md#before-committing)):

    pre-commit install

Set up the [Blender ID server](#blender-id-authentication) for authentication
and [MeiliSearch server](#search) for the search functionality.

Setup for video processing jobs. Download ngrok (https://ngrok.com/).
    - Run `./ngrok http 8010`
    - Update `.env`:
        - Set `COCONUT_API_KEY` to a valid value
        - Set `COCONUT_DECLARED_HOSTNAME` to `https://<random-value>.ngrok.io`

## Data import
You can add objects to the database manually via the Django's Admin panel.
There are also commands that import data from production, but running them requires some additional
arrangements - ask Francesco about it.


## Blender ID authentication and webhook

Login to Blender Studio is possible using Blender ID. For development, you can set up a local
instance of [Blender ID](https://docs.blender.org/id/development_setup/).

In your local Blender ID virtualenv load the following fixture:

    ./manage.py loaddata blender_studio_devserver

This creates an OAuth2 application with `BID_OAUTH_CLIENT`, `BID_OAUTH_SECRET`
and `BID_WEBHOOK_USER_MODIFIED_SECRET` values already hardcoded in `.env.example`.

**N.B.**: the webhook view delegates the actual updating of the user profile
to a background task, so in order to see the updates locally, start the processing of
tasks using the following:

    ./manage.py process_tasks

## Search setup
For a complete description of the search feature, see the [search documentation](search.md).
It also includes [production setup](search.md#deployment-to-production) and
[troubleshooting](search.md#troubleshooting) instructions.

1. Download `meilisearch` binary of version 0.25.2 from its [release page](https://github.com/meilisearch/meilisearch/releases/tag/v0.25.2),
save it in the project's directory, make it executable and launch it:
    ```
    chmod +x meilisearch
    ./meilisearch
    ```
2. With the project's venv activated, create indexes and fill them with data:
    ```
    ./manage.py create_search_indexes
    ./manage.py index_documents
    ```

The server will be available on port `7700` by default.
If you change it, adjust the `MEILISEARCH_API_ADDRESS` in `.env` as necessary.


## Workflow

#### Before committing
The following assumes that the virtual environment is activated.

[Pre-commit hooks](https://pre-commit.com) are responsible for automatically running black, mypy,
etc. on the staged files, before each commit. If there are issues, committing is aborted.

The pre-commit configuration is in the [.pre-commit-config.yaml](../.pre-commit-config.yaml) file.

In case of emergency, it is possible to
[disable one or more hooks](https://pre-commit.com/#temporarily-disabling-hooks). To completely
disable all the hooks, run `pre-commit uninstall`. To enable them again, run `pre-commit install`.

You can also execute the `test.sh` script: it runs mypy, black, tests, eslint, and stylelint on the
entire project (so it's slower and more likely to error out).


#### Git workflow
1. Rebase, don't merge.
2. `main` is the working branch. In order to work on a task, create a new branch off `main`.
It is technically possible to `git push --force` to `main`, however please consider at least warning
other developers if you plan to do it.

## Deployments

Studio doesn't use Docker, only a clone of its own repository and a few systemd units.
To deploy latest `production`, use the following script:

```
./deploy.sh
```
This will

* pull latest `main` and `production`;
* fast-forward `production` to `main`;
* SSH into `studio.blender.org` and there:
    * pull latest `production`;
    * do `pip install -r requirements_prod.txt`;
    * run database migrations;
    * do `collectstatic`;
    * restart `studio-background.service` and `studio-background.service`.

### Setting up timers for periodic tasks

Production Studio uses systemd timers intead of `crontab`. Timers and services are installed by `playbooks/shared/install.yaml` [playbook](../playbooks).

To view existing timers and details about when they were called last and other usefull info:
```
systemctl list-timers --all
```

All the units and timers can be found in [playbooks/templates/other-services/](../playbooks/templates/other-services/) of this repository.
