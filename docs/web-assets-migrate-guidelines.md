# Studio Web Assets migration guidelines

## Introduction
At Blender, we have [Web Assets](https://projects.blender.org/infrastructure/web-assets), a front-end framework to share design and interactivity components across Blender websites. The Studio website doesn't currently comply with Web Assets, and we're working on refactoring it to use this.

### Goals
This page has two goals:
- Help the Web Assets migration process by clarifying the project objectives and scope in the shorter term.
- Help improve the website front-end code to make it more consistent and maintainable in the longer term.

### Challenges
The migration process presents some unique challenges, that mainly come from conflicting Bootstrap versions within the project:
- Web Assets is based on Bootstrap 4, while the Studio website uses Bootstrap 5.
- Bootstrap 4 JavaScript (to comply with Web Assets class names) can't be added to the project without breaking exisiting JS functionality that is based on Bootstrap 5 (e.g. carousels, dropdowns, modals etc.).
- Bootstrap 5 classes can't be removed from the project without breaking JS, as classes are coupled (shared) between Boostrap styles, scripts and custom project-specific JS in the project.

## Roadmap and Solutions
After the migration and technical changes have been completed, the Studio website can be improved further. First, we may implement Web Assets base styling, focusing mainly on markup and styles. Then we can proceed further with cleanup, as outlined below.

### Do not
- Do not add the Bootstrap 4 JS bundle. _It would require rewriting all markup with dynamic functionality and could conflict with custom project-specific JS._
- Do not remove Bootstrap 5 JS bundle.
- Do not remove Bootstrap 5 classes from the markup used for JS.

### Do
#### v2.0.0
- [x] Recompile missing Bootstrap 5 Sass partials for scripts' presentation as needed (e.g., `carousel`, `dropdown`, `modal`, etc.). Recompile selectively only critical Bootstrap 5 style partials necessary for JS to work.
- [x] Recompile and keep using Bootstrap 5 style utilities.
- [x] Migrate missing Bootstrap 5 variables to Web Assets CSS 3 custom properties as needed.
- [x] Fix conflicting Bootstrap 5 JS as needed, by changing to lightweight, single-responsibility libraries.
- [x] **Cleanup and simplify inherited project-specific style components radically.**
- [x] Cleanup and simplify project-specific style variables.
- [x] Cleanup markup unused classes.
- [x] Cleanup improve markup code formatting to make it consistent.
- [x] Add semantic versioining, and release `v2.0.0`.

#### Post v2.0.0
See issues for details.

### Continuous
- Port new components to Web Assets.
- Include ported new components from Web Assets submodule, and remove project-specific components.
- Improve the design.

### Templates and statuses
This section lists all template markup files, and their statuses in the migration process.

#### Legend
Symbol | Description
--- | ---
🟢 | Changed, done
🟡 | Changed, in-progress
❌ | Is not used, safe to remove
🔴 | Unchanged, needs check
⚪ | Unchanged, needs no change

#### Apps

##### Blog app
`blog/templates/blog`
```
.
├── 🟢 blog_toolbar.html
├── 🟢 post_detail.html
├── 🟢 post_list.html
├── sections
│   └── 🟢 quote.html
└── 🟢 subscribe_jumbotron.html
```

##### Characters app
`characters/templates/characters`
```
.
├── 🟢 character_list.html
├── 🟢 charactershowcase_detail.html
├── 🟢 characterversion_detail.html
└── components
    ├── 🟢 card_character.html
    └── 🟢 card_character_showcase.html
```

##### Comments app
`comments/templates/comments`
```
.
└── components
    ├── 🟢 comment.html
    ├── 🟢 comment_input.html
    ├── 🟢 comment_section.html
    └── 🟢 comment_tree.html
```

##### Common
`common/templates/common`
```
.
├── 🟢 base.html
├── components
│   ├── ⚪ alert_coupon_announcement.html
│   ├── attachments
│   │   ├── ⚪ file_generic.html
│   │   ├── 🟢 file_image.html
│   │   └── 🟢 file_video.html
│   ├── cards
│   │   ├── 🟢 card_blog.html
│   │   ├── 🟢 card_carousel.html
│   │   ├── 🟢 card_profile.html
│   │   ├── 🟢 card_training.html
│   │   └── 🟢 pill.html
│   ├── ❌ checkbox_toggle.html
│   ├── 🟢 content_locked.html
│   ├── 🟢 faq.html
│   ├── 🟢 field_label_wrapped.html
│   ├── 🟢 file_carousel.html
│   ├── 🟢 file.html
│   ├── 🟢 file_production_log.html
│   ├── 🟢 file_section.html
│   ├── 🟢 folder.html
│   ├── helpers
│   │   ├── ⚪ image_resize.html
│   │   └── ⚪ image_set.html
│   ├── home_headers
│   │   ├── 🟢 project_general_left.html
│   │   ├── ❌ project_release_centered.html
│   │   └── ❌ livestream.html
│   ├── ⚪ meta.html
│   ├── 🟢 modal_asset.html
│   ├── 🟢 modal_asset_zoom.html
│   ├── 🟢 navbar_dropdown_notifications.html
│   ├── 🟢 navbar_dropdown_user.html
│   ├── 🟢 navbar.html
│   ├── navigation
│   │   ├── ❌ breadcrumb.html
│   │   ├── 🟢 buttons_toolbar.html
│   │   ├── 🟢 download_button.html
│   │   ├── ⚪ download_subtitles.html
│   │   ├── 🟢 footer.html
│   │   ├── 🟢 footer_simple.html
│   │   ├── 🟢 nav_drawer_section_file.html
│   │   ├── ❌ nav_drawer_section.html
│   │   ├── 🟢 nav_drawer_section_progress.html
│   │   ├── ⚪ nav_secondary_breadcrumb.html
│   │   └── 🟢 pagination.html
│   ├── ⚪ published_by.html
│   ├── 🟢 simple_header.html
│   ├── 🟢 spoiler_alert.html
│   ├── 🟢 video_player_embed.html
│   └── 🟢 video_player.html
├── errors
│   ├── ⚪ 400.html
│   ├── ⚪ 403_csrf.html
│   ├── ⚪ 403.html
│   ├── ⚪ 404.html
│   ├── ⚪ 500.html
│   └── ⚪ base.html
├── 🟢 home.html
├── refrence
│   └── ❌ buttons.html
└── 🟢 welcome.html
```

##### Users app
`users/templates/users`
```
.
└── actstream
    └── ⚪ actor.html
```

`users/templates/users`
```
.
├── 🟢 activity.html
├── components
│   ├── 🟢 action.html
│   ├── 🟢 actions.html
│   ├── 🟢 action_summary.html
│   ├── 🟢 avatar.html
│   ├── 🟢 badges.html
│   ├── ⚪ links.html
│   └── ⚪ nav_action.html
├── 🟢 notifications.html
└── settings
    ├── 🟢 base.html
    ├── 🟢 billing.html
    ├── 🟢 cloud_archive.html
    ├── 🟢 delete.html
    ├── 🟢 emails.html
    ├── 🟢 production_credits.html
    ├── 🟢 profile.html
    └── 🟢 tabs.html

```

##### Films app
`films/templates/films`
```
.
├── 🟢 base_projects.html
├── 🟢 collection_detail.html
├── components
│   ├── 🟢 breadcrumbs.html
│   ├── 🟢 card_project.html
│   ├── 🟢 jumbotron_project.html
│   ├── 🟢 pagination_dates.html
│   ├── 🟢 production_log_entry.html
│   └── 🟢 production_log_pagination.html
├── 🟢 project_detail.html
├── 🟢 project_landing_page.html
├── 🟢 films.html
├── 🟢 flatpage.html
├── 🟢 gallery.html
├── 🟢 latest_assets.html
├── 🟢 production_credit.html
├── 🟢 productionlog_detail.html
├── 🟢 productionlog.html
└── 🟢 productionlog_month.html
```

##### Search app
`search/templates/search`
```
.
├── components
│   └── 🟢 input.html
└── 🟢 search.html
```

##### Stats app
`stats/templates/stats`
```
.
└── stats
    └── 🟢 index.html
```

##### Subscriptions app
`subscriptions/templates/subscriptions`
```
.
├── 🟢 cancel.html
├── checkout
│   ├── 🟢 checkout_base.html
│   ├── ⚪ checkout_base_empty.html
│   ├── 🟢 checkout_done.html
│   └── 🟢 total.html
├── components
│   ├── 🔴 bank_transfer_details.txt
│   ├── 🔴 bank_transfer_reference.txt
│   ├── 🟢 billing_address_form.html
│   ├── 🟢 footer.html
│   ├── ❌ header_jumbotron.html
│   ├── 🟢 info.html
│   ├── 🔴 info.txt
│   ├── 🟢 info_with_status.html
│   ├── 🔴 info_with_status.txt
│   ├── 🟢 list.html
│   ├── 🟢 manage_team.html
│   ├── ⚪ payment_form.html
│   ├── 🟢 plan_selector.html
│   ├── 🟢 pretty_status.html
│   └── 🟢 total.html
├── join
│   ├── 🟢 billing_address.html
│   └── ⚪ select_plan_variation.html
├── 🟢 manage.html
└── widgets
    └── ⚪ region_select.html
```

##### Training app
`training/templates/training`
```
.
├── 🟢 base_training.html
├── 🟢 base_with_navigation.html
├── 🟢 chapter.html
├── components
│   ├── 🟢 card_button_favorite.html
│   ├── ❌ card_carousel_empty.html
│   ├── ❌ card_production_log.html
│   ├── 🟢 card_training.html
│   ├── 🟢 card_training_section.html
│   ├── 🟢 jumbotron.html
│   └── 🟢 progress_bar.html
├── 🟢 flatpage.html
├── 🟢 home_authenticated.html
├── 🟢 home_not_authenticated.html
├── 🟢 section.html
└── 🟢 training.html
```

Table last updated: _2024. 03. 29._

## Conventions
### Architecture
#### Styles
Follow [Studio CSS and Sass guidelines's](https://projects.blender.org/studio/blender-studio/src/branch/main/docs/css-and-sass-guidelines.md#architecture) architecture rules.

### Icons
#### Mappings
Web Assets uses the Fontutti icon set, while the Studio website uses Material Icons. Fontutti and Material Icons namings often match, but not always. For an easier and more consistent conversion from Material Icons to Fontutti, the following table lists icon name mappings for non-trivial icon alternatives.

Material Icons | Fontutti
--- | ---
`category` | `grid`
`create_new_folder` | `folder-plus`
`feed` | `doc`
`insert-drive-file` | `file`
`equalizer` | `bar-chart`
`equalizer` | `sliders`
`error` | `alert-circle`
`exit_to_app` | `log-in`
`extension` | `puzzle-outine`
`filter_list` | `filter`
`keyboard_down` | `chevron-down`
`movie` | `reel`
`note_add` | `file-plus`
`notifications` | `bell`
`person` | `user`
`recent_actors` | `users`
`school` | `graduation-cap`
`security` | `shield`
`timeline` | `trending-up`
`unarchive` | `plus-square`
`warning` | `alert-triangle`
`warning_amber` | `alert-triangle`

The same alternatives shall be used when migrating from Material Icons to Fontutti in other projects.

The following new icons were added to Fontutti explicitly for the Studio project:
- `alert-circle`
- `copyright` @fontawesome
- `credit-card`
- `eye-off`
- `file`
- `file-plus`
- `folder`
- `folder-plus`
- `graduation-cap` @linecons
- `hourglass-o` @fontawesome
- `hourglass-1` @fontawesome
- `sliders` @fontawesome

### Workflow
- Work on one Django app's templates at a time.
- Work on common Django templates on a per-occurrence basis.
- Work only on the markup and style layers in the short term, if possible. Most short term goals should be achievable by modifying the markup and styles only.
- Use prefixes for comment todos with the _@web-assets_ prefix. E.g. `TODO: @web-assets <todo>`
- Use Sass indented syntax for styles. For detailed guidelines, follow [Studio CSS and Sass guidelines](Studio-CSS-and-Sass-guidelines.md).
- Change default Web Assets styling only if needed. Keep design improvements for later.
