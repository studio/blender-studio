from datetime import timedelta
from unittest import mock
from unittest.mock import patch

from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.test import override_settings
from django.utils import timezone
import responses

from looper import admin_log
from looper.clock import Clock
from looper.models import Gateway, Subscription, Order
from looper.money import Money
from looper.tests.factories import SubscriptionFactory, create_customer_with_billing_address

from subscriptions.tests.base import BaseSubscriptionTestCase
import subscriptions.tasks
import users.tasks
import users.tests.util as util
from common.tests.factories.users import OAuthUserInfoFactory


class TestClockBraintree(BaseSubscriptionTestCase):
    def _create_subscription_due_now(self) -> Subscription:
        customer = create_customer_with_billing_address(country='NL', full_name='Jane Doe')
        OAuthUserInfoFactory(user=customer.user, oauth_user_id=554433)
        now = timezone.now()
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = now + relativedelta(months=-1)
            # print('fake now:', mock_now.return_value)
            subscription = SubscriptionFactory(
                customer=customer,
                payment_method__customer_id=customer.pk,
                payment_method__recognisable_name='Test payment method',
                payment_method__gateway=Gateway.objects.get(name='braintree'),
                currency='USD',
                price=Money('USD', 1110),
                status='active',
            )
        self._check_due_now(subscription)
        # Sanity check: no orders generated yet:
        self.assertIsNone(subscription.latest_order())
        return subscription

    def _check_due_now(self, subscription):
        now = timezone.now()
        # Sanity check: next payment should be due already:
        self.assertLess(subscription.next_payment, now)
        # Because some months have 31 days, next payment date can be over a day late in this test
        self.assertAlmostEqual(now, subscription.next_payment, delta=timedelta(hours=25))

    def setUp(self):
        super().setUp()
        # Allow requests to Braintree Sandbox
        responses.add_passthru('https://api.sandbox.braintreegateway.com:443/')

        self.subscription = self._create_subscription_due_now()

    @patch(
        # Make sure background task is executed as a normal function
        'subscriptions.signals.tasks.send_mail_automatic_payment_performed',
        new=subscriptions.tasks.send_mail_automatic_payment_performed.task_function,
    )
    def test_automated_payment_soft_failed_email_is_sent(self):
        now = timezone.now()

        # Tick the clock and check that order and transaction were created
        Clock().tick()

        # The subscription should not be renewed
        self.subscription.refresh_from_db()
        self.assertEqual('active', self.subscription.status)
        self.assertEqual(1, self.subscription.intervals_elapsed)
        self._check_due_now(self.subscription)

        # Test the order
        new_order = self.subscription.latest_order()
        self.assertEqual('soft-failed', new_order.status)
        self.assertEqual(1, new_order.collection_attempts)
        self.assertIsNotNone(new_order.number)
        self.assertAlmostEqual(now, new_order.retry_after, delta=timedelta(days=3))

        # Test the transaction
        new_transaction = new_order.latest_transaction()
        self.assertEqual('failed', new_transaction.status)
        self.assertEqual(
            'Cannot determine payment method: code 91508', new_transaction.failure_message
        )
        self.assertEqual(self.subscription.price, new_transaction.amount)
        entries_q = admin_log.entries_for(new_transaction)
        self.assertEqual(1, len(entries_q))
        self.assertIn('fail', entries_q.first().change_message)

        # Check that an email notification is sent
        self._assert_payment_soft_failed_email_is_sent(self.subscription)

    @patch(
        # Make sure background task is executed as a normal function
        'subscriptions.signals.tasks.send_mail_automatic_payment_performed',
        new=subscriptions.tasks.send_mail_automatic_payment_performed.task_function,
    )
    @patch(
        'users.signals.tasks.revoke_blender_id_role',
        new=users.tasks.revoke_blender_id_role.task_function,
    )
    @responses.activate
    def test_automated_payment_failed_email_is_sent(self):
        now = timezone.now()
        order = self.subscription.generate_order()
        # Make the clock attempt to charge the same order one last time
        order.retry_after = now - timedelta(seconds=2)
        order.collection_attempts = 2
        order.status = 'soft-failed'
        order.save(update_fields={'collection_attempts', 'retry_after', 'status'})
        self.assertIsNotNone(self.subscription.latest_order())

        # Tick the clock and check that order and transaction were created
        util.mock_blender_id_badger_badger_response(
            'revoke', 'cloud_subscriber', self.subscription.customer.user.oauth_info.oauth_user_id
        )
        Clock().tick()

        # The subscription should be on hold
        self.subscription.refresh_from_db()
        self.assertEqual('on-hold', self.subscription.status)
        self.assertEqual(0, self.subscription.intervals_elapsed)
        self._check_due_now(self.subscription)

        # Test the order
        new_order = self.subscription.latest_order()
        # It must be the same order
        self.assertEqual(order.pk, new_order.pk)
        self.assertEqual('failed', new_order.status)
        self.assertEqual(3, new_order.collection_attempts)
        self.assertAlmostEqual(now, new_order.retry_after, delta=timedelta(days=3))

        # Test the transaction
        new_transaction = new_order.latest_transaction()
        self.assertEqual('failed', new_transaction.status)
        self.assertEqual(
            'Cannot determine payment method: code 91508', new_transaction.failure_message
        )
        self.assertEqual(self.subscription.price, new_transaction.amount)
        entries_q = admin_log.entries_for(new_transaction)
        self.assertEqual(1, len(entries_q))
        self.assertIn('fail', entries_q.first().change_message)

        # Check that an email notification is sent
        self._assert_payment_failed_email_is_sent(self.subscription)

    @patch(
        # Make sure background task is executed as a normal function
        'subscriptions.signals.tasks.send_mail_automatic_payment_performed',
        new=subscriptions.tasks.send_mail_automatic_payment_performed.task_function,
    )
    @patch('users.signals.tasks.revoke_blender_id_role')
    @responses.activate
    def test_automated_payment_failed_email_but_another_active_subscription_exists(
        self, mock_revoke_blender_id_role
    ):
        now = timezone.now()
        order = self.subscription.generate_order()
        # Make the clock attempt to charge the same order one last time
        order.retry_after = now - timedelta(seconds=2)
        order.collection_attempts = 2
        order.status = 'soft-failed'
        order.save(update_fields={'collection_attempts', 'retry_after', 'status'})
        self.assertIsNotNone(self.subscription.latest_order())

        # Create another active subscription for the same user
        SubscriptionFactory(
            customer=self.subscription.customer,
            payment_method=self.subscription.payment_method,
            currency='USD',
            price=Money('USD', 1110),
            status='active',
        )

        # Tick the clock and check that order and transaction were created
        Clock().tick()

        # Check that an email notification is sent
        self._assert_payment_failed_email_is_sent(self.subscription)

        # But badge has not be revoked
        mock_revoke_blender_id_role.assert_not_called()

    @patch(
        # Make sure background task is executed as a normal function
        'subscriptions.signals.tasks.send_mail_automatic_payment_performed',
        new=subscriptions.tasks.send_mail_automatic_payment_performed.task_function,
    )
    def test_automated_payment_paid_email_is_sent(self):
        now = timezone.now()
        self.assertEqual(self.subscription.collection_method, 'automatic')

        # Tick the clock and check that subscription renews, order and transaction were created
        with patch(
            'looper.gateways.BraintreeGateway.transact_sale',
            return_value={'transaction_id': 'mock-transaction-id'},
        ):
            Clock().tick()

        # Test the order
        new_order = self.subscription.latest_order()
        self.assertEqual('paid', new_order.status)
        self.assertEqual(1, new_order.collection_attempts)
        self.assertIsNone(new_order.retry_after)
        self.assertIsNotNone(new_order.number)
        # self.assertNotEqual(last_order_pk, new_order.pk)

        # The subscription should be renewed now.
        self.subscription.refresh_from_db()
        self.assertEqual('active', self.subscription.status)
        self.assertEqual(1, self.subscription.intervals_elapsed)
        self.assertAlmostEqual(
            now + relativedelta(months=1),
            self.subscription.next_payment,
            delta=timedelta(seconds=1),
        )

        # Test the transaction
        new_transaction = new_order.latest_transaction()
        self.assertEqual('succeeded', new_transaction.status)
        self.assertEqual(self.subscription.price, new_transaction.amount)
        entries_q = admin_log.entries_for(new_transaction)
        self.assertEqual(1, len(entries_q))
        self.assertIn('success', entries_q.first().change_message)

        # Check that an email notification is sent
        self._assert_payment_paid_email_is_sent(self.subscription)

    @patch(
        # Make sure background task is executed as a normal function
        'subscriptions.signals.tasks.send_mail_managed_subscription_notification',
        new=subscriptions.tasks.send_mail_managed_subscription_notification.task_function,
    )
    @override_settings(LOOPER_MANAGER_MAIL='admin@example.com')
    def test_managed_subscription_notification_email_is_sent(self):
        now = timezone.now()
        self.subscription.collection_method = 'managed'
        self.subscription.save(update_fields={'collection_method'})

        # Tick the clock and check that subscription renews, order and transaction were created
        Clock().tick()

        # The subscription should be renewed now.
        self.subscription.refresh_from_db()
        self.assertEqual('active', self.subscription.status)
        self.assertEqual(1, self.subscription.intervals_elapsed)
        self.assertAlmostEqual(now, self.subscription.next_payment, delta=timedelta(hours=25))
        self.assertAlmostEqual(now, self.subscription.last_notification, delta=timedelta(seconds=1))

        # Check that an email notification is sent
        self._assert_managed_subscription_notification_email_is_sent(self.subscription)


@patch(
    # Make sure background task is executed as a normal function
    'subscriptions.signals.tasks.send_mail_subscription_expired',
    new=subscriptions.tasks.send_mail_subscription_expired.task_function,
)
@patch(
    'users.signals.tasks.revoke_blender_id_role',
    new=users.tasks.revoke_blender_id_role.task_function,
)
class TestClockExpiredSubscription(BaseSubscriptionTestCase):
    def test_subscription_on_hold_not_long_enough(self):
        now = timezone.now()
        customer = create_customer_with_billing_address(country='NL', full_name='Jane Doe')
        self.subscription = SubscriptionFactory(
            customer=customer,
            status='on-hold',
            # payment date has passed, but not long enough ago
            next_payment=now - settings.LOOPER_SUBSCRIPTION_EXPIRE_AFTER + timedelta(days=2),
        )

        Clock().tick()

        # Check that nothing has happened
        self.subscription.refresh_from_db()
        self.assertEqual('on-hold', self.subscription.status)
        self.assertFalse(self.subscription.is_cancelled)
        self.assertIsNone(self.subscription.cancelled_at)
        self._assert_no_emails_sent()

    @responses.activate
    def test_on_hold_too_long_status_changed_to_expired_email_sent(self):
        now = timezone.now()
        customer = create_customer_with_billing_address(country='NL', full_name='Jane Doe')
        OAuthUserInfoFactory(user=customer.user, oauth_user_id=223344)
        self.subscription = SubscriptionFactory(
            customer=customer,
            status='on-hold',
            # make sure to make it look like the subscription was started as some point
            started_at=now - settings.LOOPER_SUBSCRIPTION_EXPIRE_AFTER - timedelta(days=31),
            # payment date has passed a long long time ago
            next_payment=now - settings.LOOPER_SUBSCRIPTION_EXPIRE_AFTER - timedelta(days=1),
        )
        util.mock_blender_id_badger_badger_response(
            'revoke', 'cloud_subscriber', customer.user.oauth_info.oauth_user_id
        )

        Clock().tick()

        # Check that subscription is expired now
        self.subscription.refresh_from_db()
        self.assertEqual('expired', self.subscription.status)
        self.assertTrue(self.subscription.is_cancelled)
        self.assertAlmostEqual(now, self.subscription.cancelled_at, delta=timedelta(seconds=1))

        # Check that an email notification is sent
        self._assert_subscription_expired_email_is_sent(self.subscription)

    @responses.activate
    def test_on_hold_too_long_no_next_payment_has_recent_order_skipped(self):
        now = timezone.now()
        long_ago = now - settings.LOOPER_SUBSCRIPTION_EXPIRE_AFTER - timedelta(days=1)
        subscription = SubscriptionFactory(
            customer=create_customer_with_billing_address(),
            status='on-hold',
            # no next payment date set
            next_payment=None,
        )
        # Both created and updated are a long time ago
        Subscription.objects.filter(pk=subscription.pk).update(
            updated_at=long_ago,
        )
        # One order is old
        subscription.generate_order(save=True)
        Order.objects.filter(pk=subscription.latest_order().pk).update(
            created_at=long_ago
        )
        # However, there's another order that's too recent
        subscription.generate_order(save=True)

        Clock().tick()

        # Check that subscription did not expire
        subscription.refresh_from_db()
        self.assertEqual('on-hold', subscription.status)
        self.assertFalse(subscription.is_cancelled)
        self.assertIsNone(subscription.cancelled_at)

    @responses.activate
    def test_on_hold_too_long_no_next_payment_updated_recently_skipped(self):
        now = timezone.now()
        long_ago = now - settings.LOOPER_SUBSCRIPTION_EXPIRE_AFTER - timedelta(days=1)
        subscription = SubscriptionFactory(
            customer=create_customer_with_billing_address(),
            status='on-hold',
            # no next payment date set
            next_payment=None,
        )
        # Created long time ago, but updated recently
        Subscription.objects.filter(pk=subscription.pk).update(
            created_at=long_ago,
        )

        Clock().tick()

        # Check that subscription did not expire
        subscription.refresh_from_db()
        self.assertEqual('on-hold', subscription.status)
        self.assertFalse(subscription.is_cancelled)
        self.assertIsNone(subscription.cancelled_at)

    @responses.activate
    def test_on_hold_too_long_no_next_payment_or_orders_status_changed_to_expired(self):
        now = timezone.now()
        long_ago = now - settings.LOOPER_SUBSCRIPTION_EXPIRE_AFTER - timedelta(days=1)
        subscription = SubscriptionFactory(
            customer=create_customer_with_billing_address(),
            status='on-hold',
            # no next payment date set
            next_payment=None,
        )
        Subscription.objects.filter(pk=subscription.pk).update(
            updated_at=long_ago,
        )
        # The only order is too long ago
        subscription.generate_order(save=True)
        Order.objects.filter(pk=subscription.latest_order().pk).update(created_at=long_ago)

        Clock().tick()

        # Check that subscription is expired now
        subscription.refresh_from_db()
        self.assertEqual('expired', subscription.status)
        self.assertTrue(subscription.is_cancelled)
        self.assertAlmostEqual(now, subscription.cancelled_at, delta=timedelta(seconds=1))

        # No emails was sent because subscription was never active (has no `started_at`)
        self._assert_no_emails_sent()

    @responses.activate
    def test_on_hold_too_long_status_changed_to_expired_email_not_sent_to_deleted_account(self):
        now = timezone.now()
        customer = create_customer_with_billing_address(country='NL', full_name='Jane Doe')
        user = customer.user
        OAuthUserInfoFactory(user=user, oauth_user_id=223344)
        self.subscription = SubscriptionFactory(
            customer=customer,
            status='on-hold',
            # make sure to make it look like the subscription was started as some point
            started_at=now - settings.LOOPER_SUBSCRIPTION_EXPIRE_AFTER - timedelta(days=31),
            # payment date has passed a long long time ago
            next_payment=now - settings.LOOPER_SUBSCRIPTION_EXPIRE_AFTER - timedelta(days=1),
        )
        util.mock_blender_id_badger_badger_response(
            'revoke', 'cloud_subscriber', customer.user.oauth_info.oauth_user_id
        )
        # This account requested deletion
        user.__class__.objects.filter(pk=customer.user.pk).update(date_deletion_requested=now)

        Clock().tick()

        # Check that subscription is expired now
        self.subscription.refresh_from_db()
        self.assertEqual('expired', self.subscription.status)
        self.assertTrue(self.subscription.is_cancelled)
        self.assertAlmostEqual(now, self.subscription.cancelled_at, delta=timedelta(seconds=1))

        # No emails was sent because deletion was requested for account
        self._assert_no_emails_sent()
