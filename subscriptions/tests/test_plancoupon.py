from django.test import TestCase
from django.urls import reverse

from looper.models import Plan, PlanVariation, Product
from subscriptions.models import PlanCoupon


class TestPlanSelection(TestCase):
    url = reverse('subscriptions:join')

    def setUp(self):
        super().setUp()
        self.product = Product.objects.create()
        self.plan1 = Plan.objects.create(name='discounted plan', product=self.product)
        PlanVariation.objects.create(plan=self.plan1, price=100)
        self.plan2 = Plan.objects.create(product=self.product)
        PlanVariation.objects.create(plan=self.plan2, price=100)
        PlanCoupon.objects.create(coupon_code='ABC', plan=self.plan1)

    def test_bad_coupon(self):
        response = self.client.get(self.url + '?coupon_code=DEF')
        self.assertEqual(response.status_code, 404)

    def test_good_coupon(self):
        response = self.client.get(self.url + '?coupon_code=ABC')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'discounted plan')

    def test_no_coupon(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, 'discounted plan')
