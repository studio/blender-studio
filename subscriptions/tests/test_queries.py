from django.test.testcases import TestCase

from looper.tests.factories import SubscriptionFactory

from common.tests.factories.subscriptions import TeamFactory
from common.tests.factories.users import UserFactory

import looper.models

from subscriptions.queries import (
    has_active_subscription,
    has_subscription,
    has_non_legacy_subscription,
    has_not_yet_cancelled_subscription,
)


class TestHasActiveSubscription(TestCase):
    fixtures = ['gateways', 'plans']

    def test_false_when_no_subscription(self):
        user = UserFactory()

        self.assertFalse(has_active_subscription(user))

    def test_true_when_subscription_active(self):
        subscription = SubscriptionFactory(
            customer=UserFactory().customer,
            plan_id=1,
            status=list(looper.models.Subscription._ACTIVE_STATUSES)[0],
        )

        self.assertTrue(has_active_subscription(subscription.customer.user))

    def test_false_when_subscription_inactive(self):
        subscription = SubscriptionFactory(customer=UserFactory().customer, plan_id=1)

        self.assertFalse(has_active_subscription(subscription.customer.user))

    def test_false_when_team_subscription_inactive(self):
        team = TeamFactory(subscription__plan_id=1)
        team.team_users.create(user=UserFactory())

        self.assertFalse(has_active_subscription(team.team_users.first().user))

    def test_true_when_team_subscription_active(self):
        team = TeamFactory(
            subscription__customer=UserFactory().customer,
            subscription__plan_id=1,
            subscription__status=list(looper.models.Subscription._ACTIVE_STATUSES)[0],
        )
        team.team_users.create(user=UserFactory())

        self.assertTrue(has_active_subscription(team.team_users.first().user))


class TestHasNotYetCancelledSubscription(TestCase):
    fixtures = ['gateways', 'plans']

    def test_false_when_no_subscription(self):
        user = UserFactory()

        self.assertFalse(has_not_yet_cancelled_subscription(user))

    def test_true_when_subscription_active(self):
        subscription = SubscriptionFactory(
            customer=UserFactory().customer,
            plan_id=1,
            status=list(looper.models.Subscription._ACTIVE_STATUSES)[0],
        )

        self.assertTrue(has_not_yet_cancelled_subscription(subscription.customer.user))

    def test_false_when_subscription_cancelled(self):
        subscription = SubscriptionFactory(
            customer=UserFactory().customer, plan_id=1, status='cancelled'
        )

        self.assertFalse(has_not_yet_cancelled_subscription(subscription.customer.user))

    def test_true_when_subscription_inactive(self):
        subscription = SubscriptionFactory(customer=UserFactory().customer, plan_id=1)

        self.assertTrue(has_not_yet_cancelled_subscription(subscription.customer.user))

    def test_false_when_team_subscription_inactive(self):
        team = TeamFactory(subscription__plan_id=1)
        team.team_users.create(user=UserFactory())

        self.assertFalse(has_not_yet_cancelled_subscription(team.team_users.first().user))

    def test_false_when_team_subscription_active(self):
        team = TeamFactory(
            subscription__customer=UserFactory().customer,
            subscription__plan_id=1,
            subscription__status=list(looper.models.Subscription._ACTIVE_STATUSES)[0],
        )
        team.team_users.create(user=UserFactory())

        self.assertFalse(has_not_yet_cancelled_subscription(team.team_users.first().user))

    def test_false_when_team_subscription_cancelled(self):
        team = TeamFactory(
            subscription__plan_id=1,
            subscription__status='cancelled',
        )
        team.team_users.create(user=UserFactory())

        self.assertFalse(has_not_yet_cancelled_subscription(team.team_users.first().user))

    def test_true_when_team_subscription_cancelled_personal_active(self):
        team = TeamFactory(
            subscription__plan_id=1,
            subscription__status='cancelled',
        )
        team.team_users.create(user=UserFactory())
        SubscriptionFactory(
            customer=team.team_users.first().user.customer,
            plan_id=1,
            status=list(looper.models.Subscription._ACTIVE_STATUSES)[0],
        )

        self.assertTrue(has_not_yet_cancelled_subscription(team.team_users.first().user))

    def test_false_when_team_subscription_active_personal_cancelled(self):
        team = TeamFactory(
            subscription__customer=UserFactory().customer,
            subscription__plan_id=1,
            subscription__status=list(looper.models.Subscription._ACTIVE_STATUSES)[0],
        )
        team.team_users.create(user=UserFactory())
        SubscriptionFactory(
            customer=team.team_users.first().user.customer,
            plan_id=1,
            status='cancelled',
        )

        self.assertFalse(has_not_yet_cancelled_subscription(team.team_users.first().user))


class TestHasSubscription(TestCase):
    fixtures = ['gateways', 'plans']

    def test_false_when_no_subscription(self):
        user = UserFactory()

        self.assertFalse(has_subscription(user))

    def test_true_when_subscription_active(self):
        subscription = SubscriptionFactory(
            customer=UserFactory().customer,
            plan_id=1,
            status=list(looper.models.Subscription._ACTIVE_STATUSES)[0],
        )

        self.assertTrue(has_subscription(subscription.customer.user))

    def test_true_when_subscription_inactive(self):
        subscription = SubscriptionFactory(customer=UserFactory().customer, plan_id=1)

        self.assertTrue(has_subscription(subscription.customer.user))

    def test_true_when_team_subscription_inactive(self):
        team = TeamFactory(subscription__plan_id=1)
        team.team_users.create(user=UserFactory())

        self.assertTrue(has_subscription(team.team_users.first().user))

    def test_true_when_team_subscription_active(self):
        team = TeamFactory(
            subscription__customer=UserFactory().customer,
            subscription__plan_id=1,
            subscription__status=list(looper.models.Subscription._ACTIVE_STATUSES)[0],
        )
        team.team_users.create(user=UserFactory())

        self.assertTrue(has_subscription(team.team_users.first().user))

    def test_true_when_subscription_active_is_legacy(self):
        subscription = SubscriptionFactory(
            customer=UserFactory().customer,
            is_legacy=True,
            plan_id=1,
            status=list(looper.models.Subscription._ACTIVE_STATUSES)[0],
        )

        self.assertTrue(has_subscription(subscription.customer.user))

    def test_true_when_subscription_inactive_and_is_legacy(self):
        subscription = SubscriptionFactory(
            customer=UserFactory().customer, plan_id=1, is_legacy=True
        )

        self.assertTrue(has_subscription(subscription.customer.user))

    def test_true_when_team_subscription_inactive_and_is_legacy(self):
        team = TeamFactory(subscription__plan_id=1, subscription__is_legacy=True)
        team.team_users.create(user=UserFactory())

        self.assertTrue(has_subscription(team.team_users.first().user))


class TestHasNonLegacySubscription(TestCase):
    fixtures = ['gateways', 'plans']

    def test_false_when_no_subscription(self):
        user = UserFactory()

        self.assertFalse(has_non_legacy_subscription(user))

    def test_true_when_subscription_active_and_not_is_legacy(self):
        subscription = SubscriptionFactory(
            customer=UserFactory().customer,
            plan_id=1,
            status=list(looper.models.Subscription._ACTIVE_STATUSES)[0],
        )

        self.assertTrue(has_non_legacy_subscription(subscription.customer.user))

    def test_true_when_subscription_inactive_and_not_is_legacy(self):
        subscription = SubscriptionFactory(customer=UserFactory().customer, plan_id=1)

        self.assertTrue(has_non_legacy_subscription(subscription.customer.user))

    def test_true_when_team_subscription_inactive_and_not_is_legacy(self):
        team = TeamFactory(subscription__customer=UserFactory().customer, subscription__plan_id=1)
        team.team_users.create(user=UserFactory())

        self.assertTrue(has_non_legacy_subscription(team.team_users.first().user))

    def test_false_when_subscription_inactive_and_is_legacy(self):
        subscription = SubscriptionFactory(
            customer=UserFactory().customer, plan_id=1, is_legacy=True
        )

        self.assertFalse(has_non_legacy_subscription(subscription.customer.user))

    def test_false_when_subscription_active_and_is_legacy(self):
        subscription = SubscriptionFactory(
            customer=UserFactory().customer,
            is_legacy=True,
            plan_id=1,
            status=list(looper.models.Subscription._ACTIVE_STATUSES)[0],
        )

        self.assertFalse(has_non_legacy_subscription(subscription.customer.user))

    def test_false_when_team_subscription_inactive_and_is_legacy(self):
        team = TeamFactory(subscription__plan_id=1, subscription__is_legacy=True)
        team.team_users.create(user=UserFactory())

        self.assertFalse(has_non_legacy_subscription(team.team_users.first().user))

    def test_false_when_team_subscription_active_and_is_legacy(self):
        team = TeamFactory(
            subscription__customer=UserFactory().customer,
            subscription__is_legacy=True,
            subscription__plan_id=1,
            subscription__status=list(looper.models.Subscription._ACTIVE_STATUSES)[0],
        )
        team.team_users.create(user=UserFactory())

        self.assertFalse(has_non_legacy_subscription(team.team_users.first().user))
