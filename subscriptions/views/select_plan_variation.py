"""Views handling plan selection widget."""
from typing import Optional
import logging

from django.core.exceptions import ImproperlyConfigured
from django.http import Http404
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import FormView

from looper.views.checkout_stripe import CheckoutStripeView
import looper.gateways
import looper.middleware
import looper.models
import looper.money
import looper.taxes

from subscriptions.forms import SelectPlanVariationForm
from subscriptions.queries import should_redirect_to_billing

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class _PlanSelectorMixin:
    get_currency = CheckoutStripeView.get_currency
    select_team_plans = False
    plan_variation = None
    plan = None

    def _get_plans(self, coupon_code):
        qs = looper.models.Plan.objects.filter(
            is_active=True,
            team_properties__isnull=not self.select_team_plans,
        )
        if coupon_code:
            return qs.filter(plancoupon__coupon_code=coupon_code)
        else:
            return qs.filter(plancoupon__isnull=True)

    def _get_default_plan_variation(self, coupon_code):
        plan = self._get_plans(coupon_code).first()
        if not plan:
            if coupon_code:
                raise Http404('invalid coupon code')
            else:
                raise ImproperlyConfigured('no plans found')
        return plan.variation_for_currency(self.get_currency())

    def _get_plan_variation(self, plan_variation_id, coupon_code) -> Optional[
            looper.models.PlanVariation
    ]:
        if not plan_variation_id:
            return None
        try:
            qs = looper.models.PlanVariation.objects.active().filter(
                pk=plan_variation_id,
                currency=self.get_currency(),
                plan__team_properties__isnull=not self.select_team_plans,
            )
            if coupon_code:
                qs = qs.filter(plan__plancoupon__coupon_code=coupon_code)
            else:
                qs = qs.filter(plan__plancoupon__isnull=True)
            return qs.get()
        except looper.models.PlanVariation.DoesNotExist:
            return None


class SelectPlanVariationView(_PlanSelectorMixin, FormView):
    """Display available subscription plans."""

    template_name = 'subscriptions/join/select_plan_variation.html'
    form_class = SelectPlanVariationForm
    select_team_plans = False

    def get(self, request, *args, **kwargs):
        """Get optional plan_variation_id from the URL and set it on the view.

        If no plan_variation_id is available fetch either the first 'individual' or
        'team' plan, and get the default plan from there.
        """
        if should_redirect_to_billing(request.user):
            return redirect('user-settings-billing')

        plan_variation_id = kwargs.get('plan_variation_id')
        coupon_code = request.GET.get('coupon_code')
        plan_variation: looper.models.PlanVariation = self._get_plan_variation(
            plan_variation_id, coupon_code,
        )
        if not plan_variation:
            plan_variation = self._get_default_plan_variation(coupon_code)
        self.plan_variation = plan_variation
        self.plan = plan_variation.plan
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs) -> dict:
        """Add an extra form and gateway's client token."""
        coupon_code = self.request.GET.get('coupon_code')
        ctx = {
            **super().get_context_data(**kwargs),
            'plans': self._get_plans(coupon_code),
            'current_plan_variation': self.plan_variation,
            'select_team_plans': self.select_team_plans,
        }
        return ctx

    def get_initial(self) -> dict:
        """Add currency to the form initial parameters: it's used in form validation."""
        return {
            **super().get_initial(),
            'currency': self.get_currency(),
        }

    def form_valid(self, form):
        """Save the billing details and pass the data to the payment form."""
        plan_variation_id = form.cleaned_data['plan_variation_id']
        url = reverse(
            'subscriptions:join-billing-details',
            kwargs={'plan_variation_id': plan_variation_id},
        )
        if coupon_code := self.request.GET.get('coupon_code'):
            url += f'?coupon_code={coupon_code}'
        return redirect(url)


class SelectTeamPlanVariationView(SelectPlanVariationView):
    """Display available team subscription plans."""

    select_team_plans = True
