from unittest.mock import patch, MagicMock
import os
import unittest

from django.conf import settings
from django.test import TestCase
from django.urls import reverse
from freezegun import freeze_time
import responses

# from responses import _recorder

from looper.tests.test_preferred_currency import EURO_IPV4, USA_IPV4, SINGAPORE_IPV4
from looper.money import Money
import looper.models

from looper.tests.factories import create_customer_with_billing_address
from common.tests.factories.users import UserFactory, OAuthUserInfoFactory
from subscriptions.tests.base import BaseSubscriptionTestCase  # , responses_from_file
import subscriptions.tasks
import users.tasks
import users.tests.util as util

responses_dir = 'subscriptions/tests/_responses/'
required_address_data = {
    'country': 'NL',
    'email': 'my.billing.email@example.com',
    'full_name': 'New Full Name',
    'postal_code': '1000 AA',
}
full_billing_address_data = {
    **required_address_data,
    'street_address': 'MAIN ST 1',
    'locality': 'Amsterdam',
    'extended_address': 'Floor 2',
    'company': 'Test LLC',
    'vat_number': 'NL818152011B01',
}
# **N.B.**: test cases below require settings.GEOIP2_DB to point to an existing GeoLite2 database.


@freeze_time('2023-05-19 11:41:11')
class TestGETJoinView(BaseSubscriptionTestCase):
    url_usd = reverse('subscriptions:join-billing-details', kwargs={'plan_variation_id': 1})
    url = reverse('subscriptions:join-billing-details', kwargs={'plan_variation_id': 2})

    def test_pay_via_bank_transfer_button_is_shown_for_manual_plan_variation(self):
        customer = create_customer_with_billing_address(country='NL', full_name='Jane Doe')
        user = customer.user
        self.client.force_login(user)

        url, selected_variation = self._get_url_for(
            currency='EUR',
            interval_length=1,
            interval_unit='month',
            plan__name='Manual renewal',
        )
        response = self.client.get(url, REMOTE_ADDR=EURO_IPV4)

        self.assertEqual(response.status_code, 200)
        self._assert_pay_via_bank_displayed(response)

    def test_get_prefills_full_name_and_billing_email_from_user(self):
        user = UserFactory(full_name="Jane До", email='jane.doe@example.com')
        self.client.force_login(user)

        response = self.client.get(self.url, REMOTE_ADDR=EURO_IPV4)

        self.assertEqual(response.status_code, 200)
        self._assert_total_default_variation_selected_tax_21_eur(response)
        self._assert_pay_via_bank_not_displayed(response)
        self.assertContains(
            response,
            '<input type="text" name="full_name" value="Jane До" maxlength="255" placeholder="Your Full Name" class="form-control" required id="id_full_name">',
            html=True,
        )
        self.assertContains(
            response,
            '<input type="email" name="email" value="jane.doe@example.com" placeholder="mail@example.com" class="form-control" required id="id_email" maxlength="320">',
            html=True,
        )

    def test_get_displays_total_and_billing_details_to_logged_in_nl(self):
        customer = create_customer_with_billing_address(vat_number='', country='NL')
        user = customer.user
        self.client.force_login(user)

        response = self.client.get(self.url, REMOTE_ADDR=EURO_IPV4)

        self.assertEqual(response.status_code, 200)
        self._assert_billing_details_form_displayed(response)

        self._assert_total_default_variation_selected_tax_21_eur(response)

    def test_get_displays_total_and_billing_details_to_logged_in_de(self):
        customer = create_customer_with_billing_address(vat_number='', country='DE')
        user = customer.user
        self.client.force_login(user)

        response = self.client.get(self.url, REMOTE_ADDR=EURO_IPV4)

        self.assertEqual(response.status_code, 200)
        self._assert_billing_details_form_displayed(response)
        self._assert_total_default_variation_selected_tax_19_eur(response)

    def test_get_displays_total_and_billing_details_to_logged_in_us(self):
        customer = create_customer_with_billing_address(
            vat_number='', country='US', region='NY', postal_code='12001'
        )
        user = customer.user
        self.client.force_login(user)

        response = self.client.get(self.url_usd)

        self.assertEqual(response.status_code, 200)
        self._assert_billing_details_form_displayed(response)
        self._assert_form_us_address_is_displayed(response)
        self._assert_total_default_variation_selected_usd(response)

    @unittest.skipUnless(os.path.exists(settings.GEOIP2_DB), 'GeoIP database file is required')
    def test_get_detects_country_us_sets_preferred_currency_usd_and_redirects(self):
        customer = create_customer_with_billing_address()
        user = customer.user
        self.client.force_login(user)
        self.assertTrue(looper.middleware.PREFERRED_CURRENCY_SESSION_KEY not in self.client.session)

        response = self.client.get(self.url, REMOTE_ADDR=USA_IPV4)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], self.url_usd)
        self.assertEqual(
            self.client.session[looper.middleware.PREFERRED_CURRENCY_SESSION_KEY], 'USD'
        )

    @unittest.skipUnless(os.path.exists(settings.GEOIP2_DB), 'GeoIP database file is required')
    def test_get_detects_country_us_sets_preferred_currency_usd(self):
        customer = create_customer_with_billing_address()
        user = customer.user
        self.client.force_login(user)

        response = self.client.get(self.url_usd, REMOTE_ADDR=USA_IPV4)

        self.assertEqual(response.status_code, 200)
        # Check that country is preselected
        self.assertContains(
            response,
            '<option value="US" selected>United States of America</option>',
            html=True,
        )
        # Check that prices are in USD and there is no tax
        self._assert_total_default_variation_selected_usd(response)

    @unittest.skipUnless(os.path.exists(settings.GEOIP2_DB), 'GeoIP database file is required')
    def test_get_detects_country_sg_sets_preferred_currency_eur(self):
        customer = create_customer_with_billing_address()
        user = customer.user
        self.client.force_login(user)

        response = self.client.get(self.url, REMOTE_ADDR=SINGAPORE_IPV4)

        self.assertEqual(response.status_code, 200)
        # Check that country is preselected
        self.assertContains(
            response,
            '<option value="SG" selected>Singapore</option>',
            html=True,
        )
        # Check that prices are in EUR and there is no tax
        self._assert_total_default_variation_selected_no_tax_eur(response)

    @unittest.skipUnless(os.path.exists(settings.GEOIP2_DB), 'GeoIP database file is required')
    def test_get_detects_country_nl_sets_preferred_currency_eur_displays_correct_vat(self):
        customer = create_customer_with_billing_address()
        user = customer.user
        self.client.force_login(user)

        response = self.client.get(self.url, REMOTE_ADDR=EURO_IPV4)

        self.assertEqual(response.status_code, 200)
        # Check that country is preselected
        self.assertContains(
            response,
            '<option value="NL" selected>Netherlands</option>',
            html=True,
        )
        self._assert_total_default_variation_selected_tax_21_eur(response)

    def test_plan_variation_matches_detected_currency_eur_non_eea_ip(self):
        customer = create_customer_with_billing_address()
        self.client.force_login(customer.user)

        response = self.client.get(self.url, REMOTE_ADDR=SINGAPORE_IPV4)

        self.assertEqual(response.status_code, 200)
        # Check that prices are in EUR and there is no tax
        self._assert_total_default_variation_selected_no_tax_eur(response)

    def test_billing_address_country_takes_precedence_over_geo_ip(self):
        customer = create_customer_with_billing_address(country='NL')
        self.client.force_login(customer.user)

        response = self.client.get(self.url, REMOTE_ADDR=SINGAPORE_IPV4)

        self.assertEqual(response.status_code, 200)
        self._assert_total_default_variation_selected_tax_21_eur(response)


@freeze_time('2023-05-19 11:41:11')
class TestPOSTJoinView(BaseSubscriptionTestCase):
    url_usd = reverse('subscriptions:join-billing-details', kwargs={'plan_variation_id': 1})
    url = reverse('subscriptions:join-billing-details', kwargs={'plan_variation_id': 2})

    def setUp(self):
        super().setUp()
        responses._add_from_file(f'{responses_dir}vies_wsdl.yaml')

    def test_post_updates_billing_address_and_customer_renders_next_form_de(self):
        customer = create_customer_with_billing_address(vat_number='', country='DE')
        user = customer.user
        self.client.force_login(user)

        selected_variation = (
            looper.models.PlanVariation.objects.active()
            .filter(
                currency='EUR',
                plan__name='Manual renewal',
            )
            .first()
        )
        data = {**full_billing_address_data, 'gateway': 'stripe'}
        url = reverse(
            'subscriptions:join-billing-details',
            kwargs={'plan_variation_id': selected_variation.pk},
        )
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, 200)

        self._assert_required_billing_details_updated(user)
        # Check that a warning message is displayed
        self._assert_pricing_has_been_updated(response)

        self._assert_continue_to_payment_displayed(response)
        # Check that the manual plan variation totals are displayed
        self.assertContains(response, 'Total')
        self.assertContains(
            response, '<span class="x-price-tax">Inc. 21% VAT (€&nbsp;22.04)</span>', html=True
        )
        self.assertContains(response, '<span class="x-price">€&nbsp;127.00</span>', html=True)
        self.assertContains(response, 'Manual ')
        self.assertContains(response, '/ <span class="x-price-period">1 year</span>', html=True)

    def test_post_redirects_to_stripe_hosted_checkout(self):
        self.client.force_login(self.user)

        data = {**required_address_data, 'gateway': 'stripe'}
        response = self.client.post(self.url, data, REMOTE_ADDR=EURO_IPV4)

        self.assertEqual(response.status_code, 302)
        self.assertTrue(
            response['Location'].startswith('https://checkout.stripe.com/c/pay/cs_test_')
        )

    @responses.activate
    def test_post_updates_billing_address_and_customer_applies_reverse_charged_tax(self):
        responses._add_from_file(f'{responses_dir}vies_valid.yaml')
        self.client.force_login(self.user)

        data = {
            **required_address_data,
            'gateway': 'stripe',
            'country': 'DE',
            'postal_code': '11111',
            'vat_number': 'DE 260543043',
        }
        response = self.client.post(self.url, data, REMOTE_ADDR=EURO_IPV4)

        self.assertEqual(response.status_code, 200)

        self.user.refresh_from_db()
        address = self.user.customer.billing_address
        address.refresh_from_db()
        self.assertEqual(address.vat_number, 'DE260543043')
        self.assertEqual(address.full_name, 'New Full Name')
        self.assertEqual(address.postal_code, '11111')
        self.assertEqual(address.country, 'DE')

        # Check that a warning message is displayed
        self._assert_pricing_has_been_updated(response)

        # Check that default plan variation with subtracted VAT is displayed, and no tax is displayed
        self._assert_total_default_variation_selected_tax_19_eur_reverse_charged(response)

    def test_post_changing_address_from_with_region_to_without_region_clears_region(self):
        customer = create_customer_with_billing_address(
            vat_number='', country='US', region='NY', postal_code='12001'
        )
        user = customer.user
        self.client.force_login(user)

        response = self.client.get(self.url_usd)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(user.customer.billing_address.region, 'NY')
        self._assert_billing_details_form_displayed(response)
        self._assert_form_us_address_is_displayed(response)
        self._assert_total_default_variation_selected_usd(response)

        # Post an new address that doesn't require a region
        data = {
            **required_address_data,
            'gateway': 'stripe',
            'country': 'DE',
            'postal_code': '11111',
        }
        response = self.client.post(self.url_usd, data)

        self.assertEqual(response.status_code, 302)
        # Redirected to the plan variation with matching currency
        self.assertEqual(response['Location'], self.url)
        # Follow redirect to be able to assert contents of it
        response = self.client.get(response['Location'])

        self._assert_pricing_has_been_updated(response)

        self.assertEqual(user.customer.billing_address.full_name, 'New Full Name')
        # Check that billing details has been updated correctly
        self.assertEqual(user.customer.billing_address.region, '')
        self.assertEqual(user.customer.billing_address.country, 'DE')
        self.assertEqual(user.customer.billing_address.postal_code, '11111')

    def test_plan_variation_does_not_match_detected_currency_usd_euro_ip(self):
        url, _ = self._get_url_for(currency='USD', price=12700)
        customer = create_customer_with_billing_address(country='NL')
        user = customer.user
        self.client.force_login(user)

        data = {**required_address_data, 'gateway': 'stripe'}
        response = self.client.post(url, data, REMOTE_ADDR=EURO_IPV4)

        self.assertEqual(response.status_code, 302)
        expected_url, _ = self._get_url_for(currency='EUR', price=12700)
        self.assertEqual(response['Location'], expected_url)

    def test_billing_address_country_takes_precedence_over_geo_ip(self):
        customer = create_customer_with_billing_address(country='GE')
        self.client.force_login(customer.user)

        data = {**required_address_data, 'gateway': 'stripe'}
        response = self.client.post(self.url, data, REMOTE_ADDR=SINGAPORE_IPV4)

        self.assertEqual(response.status_code, 200)
        self._assert_total_default_variation_selected_tax_21_eur(response)

    def test_invalid_missing_required_fields(self):
        customer = create_customer_with_billing_address(country='NL')
        self.client.force_login(customer.user)

        response = self.client.post(self.url, {}, REMOTE_ADDR=EURO_IPV4)

        self.assertEqual(response.status_code, 200)
        self._assert_total_default_variation_selected_tax_21_eur(response)
        self.assertEqual(
            response.context['form'].errors,
            {
                'country': ['This field is required.'],
                'email': ['This field is required.'],
                'full_name': ['This field is required.'],
                'gateway': ['This field is required.'],
            },
        )

    def test_invalid_bank_transfer_cannot_be_selected_for_automatic_payments(self):
        url, selected_variation = self._get_url_for(currency='EUR', price=1150)
        customer = create_customer_with_billing_address(country='NL')
        user = customer.user
        self.client.force_login(user)

        data = {
            **required_address_data,
            'gateway': 'bank',
            'payment_method_nonce': 'unused',
            'price': '9.90',
        }
        response = self.client.post(url, data, REMOTE_ADDR=EURO_IPV4)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context['form'].errors,
            {
                'gateway': [
                    'Select a valid choice. That choice is not one of the available choices.'
                ]
            },
        )

    @patch(
        # Make sure background task is executed as a normal function
        'subscriptions.signals.tasks.send_mail_bank_transfer_required',
        new=subscriptions.tasks.send_mail_bank_transfer_required.task_function,
    )
    @patch(
        'users.signals.tasks.grant_blender_id_role',
        new=users.tasks.grant_blender_id_role.task_function,
    )
    @responses.activate
    def test_pay_with_bank_transfer_creates_order_subscription_on_hold(self):
        customer = create_customer_with_billing_address(country='NL')
        user = customer.user
        OAuthUserInfoFactory(user=user, oauth_user_id=554433)
        self.client.force_login(user)
        util.mock_blender_id_badger_badger_response(
            'grant', 'cloud_has_subscription', user.oauth_info.oauth_user_id
        )

        url, selected_variation = self._get_url_for(
            currency='EUR',
            interval_length=1,
            interval_unit='month',
            plan__name='Manual renewal',
        )
        data = {**required_address_data, 'full_name': 'Jane Doe', 'gateway': 'bank'}
        response = self.client.post(url, data, REMOTE_ADDR=EURO_IPV4)

        self._assert_transactionless_done_page_displayed(response)

        subscription = user.customer.subscription_set.first()
        self.assertEqual(subscription.status, 'on-hold')
        self.assertEqual(subscription.price, Money('EUR', 1700))
        self.assertEqual(subscription.tax, Money('EUR', 295))
        self.assertEqual(subscription.tax_country, 'NL')
        self.assertEqual(subscription.tax_type, 'VATC')
        self.assertEqual(subscription.collection_method, selected_variation.collection_method)
        self.assertEqual(subscription.collection_method, 'manual')
        self.assertEqual(subscription.plan, selected_variation.plan)

        order = subscription.latest_order()
        self.assertEqual(order.status, 'created')
        self.assertEqual(order.price, Money('EUR', 1700))
        self.assertEqual(order.tax, Money('EUR', 295))
        self.assertEqual(order.tax_country, 'NL')
        self.assertEqual(order.tax_type, 'VATC')
        self.assertIsNotNone(order.pk)
        self.assertIsNotNone(order.number)
        self.assertIsNotNone(order.display_number)
        self.assertNotEqual(order.display_number, str(order.pk))

        self._assert_bank_transfer_email_is_sent(subscription)
        self._assert_bank_transfer_email_is_sent_tax_21(subscription)

        # Check that the reverse-charged price is displayed in billing as well
        response = self.client.get(
            reverse('subscriptions:manage', kwargs={'subscription_id': subscription.pk})
        )
        self.assertContains(response, f'Subscription #{subscription.pk}', html=True)
        self.assertContains(response, 'Subscription On Hold', html=True)
        self.assertContains(response, 'Bank Transfer', html=True)
        self.assertContains(response, '€\xa017\u00A0/\u00A0month', html=True)
        # Check that bank details are displayed
        self.assertContains(
            response,
            'Blender Studio B.V.\n'
            'Bank: ING Bank\n'
            'IBAN: NL07 INGB 0008 4489 82\n'
            'BIC/Swift: INGB NL2A\n',
            html=True,
        )
        self.assertContains(response, f'Blender Studio order-{order.number}', html=True)

    @patch(
        'subscriptions.signals.tasks.send_mail_bank_transfer_required',
        new=subscriptions.tasks.send_mail_bank_transfer_required.task_function,
    )
    @patch(
        'users.signals.tasks.grant_blender_id_role',
        new=users.tasks.grant_blender_id_role.task_function,
    )
    @responses.activate
    def test_pay_with_bank_transfer_creates_order_subscription_on_hold_shows_reverse_charged_price(
        self,
    ):
        responses._add_from_file(f'{responses_dir}vies_valid.yaml')
        address = {
            **required_address_data,
            'country': 'ES',
            'full_name': 'Jane Doe',
            'postal_code': '11111',
            'vat_number': 'ES A78374725',
        }
        customer = create_customer_with_billing_address(**address)
        user = customer.user
        OAuthUserInfoFactory(user=user, oauth_user_id=554433)
        self.client.force_login(user)
        util.mock_blender_id_badger_badger_response(
            'grant', 'cloud_has_subscription', user.oauth_info.oauth_user_id
        )

        url, selected_variation = self._get_url_for(
            currency='EUR',
            plan__name='Manual renewal',
            interval_length=3,
            interval_unit='month',
        )
        data = {'gateway': 'bank', **address}
        response = self.client.post(url, data, REMOTE_ADDR=EURO_IPV4)

        self._assert_transactionless_done_page_displayed(response)

        subscription = user.customer.subscription_set.first()
        self.assertEqual(subscription.status, 'on-hold')
        self.assertEqual(subscription.price, Money('EUR', 3700))
        self.assertEqual(subscription.tax, Money('EUR', 0))
        self.assertEqual(subscription.tax_country, 'ES')
        self.assertEqual(subscription.tax_type, 'VATRC')
        self.assertEqual(subscription.collection_method, selected_variation.collection_method)
        self.assertEqual(subscription.collection_method, 'manual')
        self.assertEqual(subscription.plan, selected_variation.plan)
        self.assertEqual(str(subscription.payment_method), 'Bank Transfer')
        self.assertIsNone(subscription.payment_method.token)

        order = subscription.latest_order()
        self.assertEqual(order.status, 'created')
        self.assertEqual(order.price, Money('EUR', 3058))
        self.assertEqual(order.tax, Money('EUR', 0))
        self.assertEqual(order.tax_country, 'ES')
        self.assertEqual(order.tax_type, 'VATRC')
        self.assertIsNotNone(order.pk)
        self.assertIsNotNone(order.number)
        self.assertIsNotNone(order.display_number)
        self.assertIsNotNone(order.vat_number)
        self.assertNotEqual(order.display_number, str(order.pk))
        self.assertEqual(str(order.payment_method), 'Bank Transfer')
        self.assertIsNone(order.payment_method.token)

        self._assert_bank_transfer_email_is_sent(subscription)
        self._assert_bank_transfer_email_is_sent_tax_21_eur_reverse_charged(subscription)

        # Check that the reverse-charged price is displayed in billing as well
        response = self.client.get(
            reverse('subscriptions:manage', kwargs={'subscription_id': subscription.pk})
        )

        self.assertNotIn('32.00', response)
        self.assertNotIn('21%', response)
        self.assertNotIn('Inc.', response)
        self.assertNotIn('VAT', response)
        self.assertContains(response, f'Subscription #{subscription.pk}', html=True)
        self.assertContains(response, 'Subscription On Hold', html=True)
        self.assertContains(response, 'Bank Transfer', html=True)
        self.assertContains(response, '€\xa030.58\u00A0/\u00A0quarter', html=True)
        # Check that bank details are displayed
        self.assertContains(
            response,
            'Blender Studio B.V.\n'
            'Bank: ING Bank\n'
            'IBAN: NL07 INGB 0008 4489 82\n'
            'BIC/Swift: INGB NL2A\n',
            html=True,
        )
        self.assertContains(response, f'Blender Studio order-{order.number}', html=True)

    @patch('stripe.checkout.Session.create')
    @patch('stripe.Customer.create', MagicMock(return_value=MagicMock(id='cus_mock_id')))
    def test_continue_to_payment_eur(self, mock_checkout_session):
        mock_checkout_session.return_value = MagicMock()
        mock_checkout_session.return_value.url = 'https://example.com/checkout-session-url/'
        url, selected_variation = self._get_url_for(currency='EUR', price=1150)
        customer = create_customer_with_billing_address(country='NL', full_name='Jane Doe')
        user = customer.user
        OAuthUserInfoFactory(user=user, oauth_user_id=554433)
        self.client.force_login(user)

        data = {**required_address_data, 'gateway': 'stripe'}
        response = self.client.post(url, data, REMOTE_ADDR=EURO_IPV4)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], 'https://example.com/checkout-session-url/')

        mock_checkout_session.assert_called_once_with(
            cancel_url='http://testserver' + url,
            customer='cus_mock_id',
            line_items=[
                {
                    'price_data': {
                        'currency': 'eur',
                        'product_data': {'name': 'Blender Studio Subscription / Automatic renewal'},
                        'unit_amount': 1150,
                    },
                    'quantity': 1,
                }
            ],
            mode='payment',
            payment_intent_data={
                'metadata': {'plan_variation_id': selected_variation.pk},
                'setup_future_usage': 'off_session',
            },
            payment_method_types=['card', 'link', 'paypal'],
            submit_type='pay',
            success_url=f'http://testserver/join/plan-variation/{selected_variation.pk}/done/{{CHECKOUT_SESSION_ID}}/',
            ui_mode='hosted',
        )

    @patch('stripe.checkout.Session.create')
    @patch('stripe.Customer.create', MagicMock(return_value=MagicMock(id='cus_mock_id')))
    def test_continue_to_payment_eur_business_de(self, mock_checkout_session):
        mock_checkout_session.return_value = MagicMock()
        mock_checkout_session.return_value.url = 'https://example.com/checkout-session-url/'
        url, selected_variation = self._get_url_for(currency='EUR', price=1150)
        customer = create_customer_with_billing_address(country='DE', vat_number='DE260543043')
        user = customer.user
        self.client.force_login(user)

        data = {
            **required_address_data,
            'gateway': 'stripe',
            'country': 'DE',
            'postal_code': '11111',
            'vat_number': 'DE 260543043',
        }
        response = self.client.post(url, data, REMOTE_ADDR=EURO_IPV4)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], 'https://example.com/checkout-session-url/')

        mock_checkout_session.assert_called_once_with(
            cancel_url='http://testserver' + url,
            customer='cus_mock_id',
            line_items=[
                {
                    'price_data': {
                        'currency': 'eur',
                        'product_data': {'name': 'Blender Studio Subscription / Automatic renewal'},
                        # Check that checkout got the correct lowered total
                        'unit_amount': 966,
                    },
                    'quantity': 1,
                }
            ],
            mode='payment',
            payment_intent_data={
                'metadata': {'plan_variation_id': 2},
                'setup_future_usage': 'off_session',
            },
            payment_method_types=['card', 'link', 'paypal'],
            submit_type='pay',
            success_url='http://testserver/join/plan-variation/2/done/{CHECKOUT_SESSION_ID}/',
            ui_mode='hosted',
        )

        # Check that customer details are saved correctly
        customer.refresh_from_db()
        address = customer.billing_address
        self.assertEqual(address.vat_number, 'DE260543043')
        self.assertEqual(address.country, 'DE')
        self.assertEqual(address.postal_code, '11111')

    @patch('stripe.checkout.Session.create')
    @patch('stripe.Customer.create', MagicMock(return_value=MagicMock(id='cus_mock_id')))
    def test_continue_to_payment_usd(self, mock_checkout_session):
        mock_checkout_session.return_value = MagicMock()
        mock_checkout_session.return_value.url = 'https://example.com/checkout-session-url/'
        url, selected_variation = self._get_url_for(
            currency='USD',
            interval_length=1,
            interval_unit='month',
            plan__name='Automatic renewal',
        )
        customer = create_customer_with_billing_address(country='US', full_name='Jane Doe')
        user = customer.user
        self.client.force_login(user)

        data = {**required_address_data, 'postal_code': '', 'country': 'US', 'gateway': 'stripe'}
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], 'https://example.com/checkout-session-url/')

        mock_checkout_session.assert_called_once_with(
            cancel_url='http://testserver' + url,
            customer='cus_mock_id',
            line_items=[
                {
                    'price_data': {
                        'currency': 'usd',
                        'product_data': {'name': 'Blender Studio Subscription / Automatic renewal'},
                        'unit_amount': 1150,
                    },
                    'quantity': 1,
                }
            ],
            mode='payment',
            payment_intent_data={
                'metadata': {'plan_variation_id': 1},
                'setup_future_usage': 'off_session',
            },
            payment_method_types=['card', 'link', 'paypal'],
            submit_type='pay',
            success_url='http://testserver/join/plan-variation/1/done/{CHECKOUT_SESSION_ID}/',
            ui_mode='hosted',
        )

    @patch('stripe.checkout.Session.create')
    @patch('stripe.Customer.create', MagicMock(return_value=MagicMock(id='cus_mock_id')))
    def test_continue_to_payment_team(self, mock_checkout_session):
        mock_checkout_session.return_value = MagicMock()
        mock_checkout_session.return_value.url = 'https://example.com/checkout-session-url/'
        url, selected_variation = self._get_url_for(currency='EUR', price=1200000)
        customer = create_customer_with_billing_address(country='NL', full_name='Jane Doe')
        user = customer.user
        self.client.force_login(user)

        data = {**required_address_data, 'gateway': 'stripe'}
        response = self.client.post(url, data, REMOTE_ADDR=EURO_IPV4)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], 'https://example.com/checkout-session-url/')

        mock_checkout_session.assert_called_once_with(
            cancel_url='http://testserver' + url,
            customer='cus_mock_id',
            line_items=[
                {
                    'price_data': {
                        'currency': 'eur',
                        'product_data': {'name': 'Blender Studio Corporate Subscription / Silver'},
                        'unit_amount': 1200000,
                    },
                    'quantity': 1,
                }
            ],
            mode='payment',
            payment_intent_data={
                'metadata': {'plan_variation_id': selected_variation.pk},
                'setup_future_usage': 'off_session',
            },
            payment_method_types=['card', 'link', 'paypal'],
            submit_type='pay',
            success_url=f'http://testserver/join/plan-variation/{selected_variation.pk}/done/{{CHECKOUT_SESSION_ID}}/',
            ui_mode='hosted',
        )


class TestGETJoinDoneView(
    BaseSubscriptionTestCase
):  # TODO: records and mock Stripe responses for this test case
    """Pretend that checkout session was completed and we've returned to the success page with its ID."""

    @patch(
        # Make sure background task is executed as a normal function
        'subscriptions.signals.tasks.send_mail_subscription_status_changed',
        new=subscriptions.tasks.send_mail_subscription_status_changed.task_function,
    )
    @patch(
        'users.signals.tasks.grant_blender_id_role',
        new=users.tasks.grant_blender_id_role.task_function,
    )
    def test_pay_with_credit_card_creates_order_subscription_active_eur(self):
        gwc_id = 'cus_QJhFyMa6fxZlsw'
        cs_id = 'cs_test_a1TgF6wE6hpU8UoLAtgFYbDup1RT797IWebOtTRf1zMrrNzInOCPwwFgR2'

        _, selected_variation = self._get_url_for(currency='EUR', price=1150)
        customer = create_customer_with_billing_address(
            country='NL', full_name='Jane Doe', gateway_customer_id=gwc_id
        )
        user = customer.user
        OAuthUserInfoFactory(user=user, oauth_user_id=554433)
        # No subscription yet
        self.assertIsNone(customer.subscription_set.first())

        url = reverse(
            'subscriptions:join-done',
            kwargs={'plan_variation_id': selected_variation.pk, 'stripe_session_id': cs_id},
        )
        self.client.force_login(user)
        with responses.RequestsMock() as rsps:
            rsps.add_passthru('https://api.stripe.com/v1')  # TODO: record and mock Stripe responses
            util.mock_blender_id_badger_badger_response(
                'grant', 'cloud_has_subscription', user.oauth_info.oauth_user_id, rsps
            )
            util.mock_blender_id_badger_badger_response(
                'grant', 'cloud_subscriber', user.oauth_info.oauth_user_id, rsps
            )
            response = self.client.get(url)

        self._assert_done_page_displayed(response)

        subscription = customer.subscription_set.first()
        self.assertEqual(subscription.status, 'active')
        self.assertEqual(subscription.price, Money('EUR', 1150))
        self.assertEqual(subscription.collection_method, selected_variation.collection_method)
        self.assertEqual(subscription.collection_method, 'automatic')
        self.assertEqual(subscription.plan, selected_variation.plan)

        order = subscription.latest_order()
        self.assertEqual(order.status, 'paid')
        self.assertIsNotNone(order.number)
        self.assertEqual(order.price, Money('EUR', 1150))

        self._assert_subscription_activated_email_is_sent(subscription)

    @patch(
        # Make sure background task is executed as a normal function
        'subscriptions.signals.tasks.send_mail_subscription_status_changed',
        new=subscriptions.tasks.send_mail_subscription_status_changed.task_function,
    )
    def test_pay_with_credit_card_creates_order_subscription_active_usd(self):
        gwc_id = 'cus_QJd93kBKke76x9'
        cs_id = 'cs_test_a1V47pczwN57cZyLrHmbyR3vHX2CU0i8bvFuFpV0v4T4DKse24bgzCk803'

        _, selected_variation = self._get_url_for(currency='USD', price=1150)
        customer = create_customer_with_billing_address(
            country='NL', full_name='Jane Doe', gateway_customer_id=gwc_id
        )
        user = customer.user
        OAuthUserInfoFactory(user=user, oauth_user_id=554433)
        # No subscription yet
        self.assertIsNone(customer.subscription_set.first())

        url = reverse(
            'subscriptions:join-done',
            kwargs={'plan_variation_id': selected_variation.pk, 'stripe_session_id': cs_id},
        )
        self.client.force_login(user)
        with responses.RequestsMock() as rsps:
            rsps.add_passthru('https://api.stripe.com/v1')  # TODO: record and mock Stripe responses
            response = self.client.get(url)

        self._assert_done_page_displayed(response)

        subscription = customer.subscription_set.first()
        self.assertEqual(subscription.status, 'active')
        self.assertEqual(subscription.price, Money('USD', 1150))
        self.assertEqual(subscription.collection_method, selected_variation.collection_method)
        self.assertEqual(subscription.collection_method, 'automatic')
        self.assertEqual(subscription.plan, selected_variation.plan)

        order = subscription.latest_order()
        self.assertEqual(order.status, 'paid')
        self.assertIsNotNone(order.number)
        self.assertEqual(order.price, Money('USD', 1150))

        self._assert_subscription_activated_email_is_sent(subscription)

    @patch(
        # Make sure background task is executed as a normal function
        'subscriptions.signals.tasks.send_mail_subscription_status_changed',
        new=subscriptions.tasks.send_mail_subscription_status_changed.task_function,
    )
    @patch(
        'users.signals.tasks.grant_blender_id_role',
        new=users.tasks.grant_blender_id_role.task_function,
    )
    def test_pay_with_credit_card_creates_order_subscription_active_team(self):
        gwc_id = 'cus_QJdieDlyr6G010'
        cs_id = 'cs_test_a1qAGDogXLVjkIOTEZhKUKqODgcgqqULOdZasZIj5hM6DKciPsmKGLC6VD'

        _, selected_variation = self._get_url_for(currency='EUR', price=100000)
        customer = create_customer_with_billing_address(
            country='NL', full_name='Jane Doe', gateway_customer_id=gwc_id
        )
        user = customer.user
        OAuthUserInfoFactory(user=user, oauth_user_id=554433)
        # No subscription yet
        self.assertIsNone(customer.subscription_set.first())

        # **N.B**: this flow happens in 2 different views separated by a Stripe payment page.
        # Pretend that checkout session was completed and we've returned to the success page with its ID:
        url = reverse(
            'subscriptions:join-done',
            kwargs={'plan_variation_id': selected_variation.pk, 'stripe_session_id': cs_id},
        )
        self.client.force_login(user)
        with responses.RequestsMock() as rsps:
            rsps.add_passthru('https://api.stripe.com/v1')  # TODO: record and mock Stripe responses
            util.mock_blender_id_badger_badger_response(
                'grant', 'cloud_has_subscription', user.oauth_info.oauth_user_id, rsps=rsps
            )
            util.mock_blender_id_badger_badger_response(
                'grant', 'cloud_subscriber', user.oauth_info.oauth_user_id, rsps=rsps
            )
            response = self.client.get(url)

        self._assert_done_page_displayed(response)

        subscription = customer.subscription_set.first()
        order = subscription.latest_order()
        self.assertEqual(subscription.status, 'active')
        self.assertEqual(subscription.price, Money('EUR', 100000))
        self.assertEqual(subscription.collection_method, selected_variation.collection_method)
        self.assertEqual(subscription.collection_method, 'automatic')
        self.assertEqual(subscription.plan, selected_variation.plan)
        self.assertEqual(order.status, 'paid')
        self.assertIsNotNone(order.number)
        self.assertEqual(order.price, Money('EUR', 100000))
        self.assertIsNone(order.subscription.team.seats)  # unlimited seats

        self._assert_team_subscription_activated_email_is_sent(subscription)

    def test_pay_with_credit_card_creates_order_subscription_active_business_de(self):
        gwc_id = 'cus_QJhFyMa6fxZlsw'
        cs_id = 'cs_test_a1mP1WLM96XWNJL4CnGOO9Z251F1R4nWTS57dhz0UNhDB1vXRa2bTpC19f'

        customer = create_customer_with_billing_address(
            country='DE', vat_number='DE260543043', gateway_customer_id=gwc_id
        )
        user = customer.user
        # No subscription yet
        self.assertIsNone(customer.subscription_set.first())

        _, selected_variation = self._get_url_for(
            currency='EUR', price=1700, collection_method='manual'
        )
        # **N.B**: this flow happens in 2 different views separated by a Stripe payment page.
        # Pretend that checkout session was completed and we've returned to the success page with its ID:
        url = reverse(
            'subscriptions:join-done',
            kwargs={'plan_variation_id': selected_variation.pk, 'stripe_session_id': cs_id},
        )
        self.client.force_login(user)
        with responses.RequestsMock() as rsps:
            rsps.add_passthru('https://api.stripe.com/v1')  # TODO: record and mock Stripe responses
            response = self.client.get(url)

        self._assert_done_page_displayed(response)

        subscription = customer.subscription_set.first()
        self.assertEqual(subscription.status, 'active')
        self.assertEqual(subscription.price, Money('EUR', 1700))
        self.assertEqual(subscription.tax, Money('EUR', 0))
        self.assertEqual(subscription.tax_country, 'DE')
        self.assertEqual(subscription.tax_rate, 19)
        self.assertEqual(subscription.collection_method, selected_variation.collection_method)
        self.assertEqual(subscription.collection_method, 'manual')
        self.assertEqual(subscription.plan, selected_variation.plan)
        self.assertEqual(subscription.currency, 'EUR')
        self.assertEqual(subscription.interval_unit, 'month')
        self.assertEqual(subscription.interval_length, 1)

        order = subscription.latest_order()
        self.assertEqual(order.status, 'paid')
        self.assertEqual(order.price, Money('EUR', 1429))
        self.assertEqual(order.tax, Money('EUR', 0))
        self.assertEqual(order.vat_number, 'DE260543043')
        self.assertEqual(order.tax_country, 'DE')
        self.assertEqual(order.tax_rate, 19)
        self.assertIsNotNone(order.number)


class TestJoinViewLoggedInUserOnly(TestCase):
    url = reverse('subscriptions:join-billing-details', kwargs={'plan_variation_id': 2})

    def test_get_anonymous_redirects_to_login_with_next(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], '/oauth/login?next=/join/plan-variation/2/billing/')

    def test_post_anonymous_redirects_to_login_with_next(self):
        response = self.client.post(self.url)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], '/oauth/login?next=/join/plan-variation/2/billing/')
