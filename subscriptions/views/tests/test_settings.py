from unittest.mock import patch

from django.urls import reverse

from looper.models import PaymentMethod, PaymentMethodAuthentication, Gateway
from looper.money import Money
from looper.tests.factories import SubscriptionFactory
import responses

# from responses import _recorder

from common.tests.factories.users import UserFactory
from subscriptions.tests.base import BaseSubscriptionTestCase, responses_from_file
import subscriptions.tasks

responses_dir = 'subscriptions/tests/_responses/'
required_address_data = {
    'country': 'NL',
    'email': 'my.billing.email@example.com',
    'full_name': 'New Full Name',
    'locality': 'Amsterdam',
    'street_address': 'MAIN ST 1',
}
full_billing_address_data = {
    **required_address_data,
    'postal_code': '1000 AA',
    'extended_address': 'Floor 2',
    'company': 'Test LLC',
    'vat_number': 'NL818152011B01',
}


class TestSubscriptionSettingsBillingAddress(BaseSubscriptionTestCase):
    url = reverse('subscriptions:billing-address')

    def test_saves_full_billing_address(self):
        user = UserFactory()
        self.client.force_login(user)

        response = self.client.post(self.url, full_billing_address_data)

        # Check that the redirect on success happened
        self.assertEqual(response.status_code, 302, response.content)
        self.assertEqual(response['Location'], self.url)

        # Check that all address fields were updated
        customer = user.customer
        address = customer.billing_address
        self.assertEqual(address.full_name, 'New Full Name')
        self.assertEqual(address.street_address, 'MAIN ST 1')
        self.assertEqual(address.extended_address, 'Floor 2')
        self.assertEqual(address.locality, 'Amsterdam')
        self.assertEqual(address.postal_code, '1000 AA')
        # self.assertEqual(address.region, 'North Holland')
        self.assertEqual(address.country, 'NL')
        self.assertEqual(address.company, 'Test LLC')

        # Check that customer fields were updated as well
        self.assertEqual(customer.billing_address.vat_number, 'NL818152011B01')
        self.assertEqual(customer.billing_address.email, 'my.billing.email@example.com')

    def test_invalid_missing_required_fields(self):
        user = UserFactory()
        self.client.force_login(user)

        response = self.client.post(self.url, {})

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'errorlist')
        self.assertContains(response, 'is required')

    def test_invalid_missing_required_full_name(self):
        user = UserFactory()
        self.client.force_login(user)

        data = {
            'email': 'new@example.com',
        }
        response = self.client.post(self.url, data)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'errorlist')
        self.assertContains(response, 'is required')

    def test_invalid_missing_required_email(self):
        user = UserFactory()
        self.client.force_login(user)

        data = {
            'full_name': 'New Full Name',
        }
        response = self.client.post(self.url, data)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'errorlist')
        self.assertContains(response, 'is required')


class TestSubscriptionSettingsChangePaymentMethod(BaseSubscriptionTestCase):
    shared_payment_form_data = {
        **required_address_data,
        'next_url_after_done': reverse('user-settings-billing'),
    }
    url_name = 'subscriptions:payment-method-change'
    success_url_name = 'user-settings-billing'

    # @_recorder.record(file_path=f'{responses_dir}stripe_new_cs_setup.yaml')
    # @_recorder.record(file_path=f'{responses_dir}stripe_get_cs_setup.yaml')
    def test_can_change_payment_method_from_bank_to_credit_card_with_sca(self):
        bank = Gateway.objects.get(name='bank')
        subscription = SubscriptionFactory(
            customer=self.user.customer,
            payment_method__customer_id=self.user.customer.pk,
            payment_method__gateway=bank,
        )
        self.assertEqual(PaymentMethod.objects.count(), 1)
        self.assertIsNone(PaymentMethodAuthentication.objects.first())
        payment_method = subscription.payment_method
        self.client.force_login(self.user)

        url = reverse(self.url_name, kwargs={'subscription_id': subscription.pk})
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(f'{responses_dir}stripe_new_cs_setup.yaml')
            response = self.client.post(url)

        self.assertEqual(response.status_code, 302)
        expected_redirect_url = 'https://checkout.stripe.com/c/pay/cs_test_c1QeSt36UcbmnmrXJnEYZpWakr377WPMfbWLeR9d3ZBYJPWXRUJ3TQ0UG9#fidkdWxOYHwnPyd1blpxYHZxWjA0VUpnNzNAMU5EUEcwYW92dWIwclxRMzQ8ZkxsUDRET2dPbTVidnBCNEJTdlBJQTRJYFF2c09BMEFBdlxVT19USGpWSXRSXFJwdm5UQXRpdVw2Rmp%2FZ11NNTU3fHdHUl1JTCcpJ2N3amhWYHdzYHcnP3F3cGApJ2lkfGpwcVF8dWAnPyd2bGtiaWBaZmppcGhrJyknYGtkZ2lgVWlkZmBtamlhYHd2Jz9xd3BgeCUl'
        self.assertEqual(response['Location'], expected_redirect_url, response['Location'])

        # **N.B**: this flow happens in 2 different views separated by a Stripe payment page.
        # Pretend that checkout session was completed and we've returned to the success page with its ID:
        checkout_session_id = 'cs_test_c1QeSt36UcbmnmrXJnEYZpWakr377WPMfbWLeR9d3ZBYJPWXRUJ3TQ0UG9'
        success_url = url + f'{checkout_session_id}/'
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(f'{responses_dir}stripe_get_cs_setup.yaml')
            response = self.client.get(success_url)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], f'/subscription/{subscription.pk}/manage/')

        # New payment method was created
        self.assertEqual(PaymentMethod.objects.count(), 2)

        subscription.refresh_from_db()
        subscription.payment_method.refresh_from_db()
        # Subscription's payment method was changed to a credit card
        self.assertNotEqual(subscription.payment_method_id, payment_method.pk)
        self.assertEqual(
            str(subscription.payment_method),
            'visa credit card ending in 4242',
        )
        # SCA isn't stored for Stripe payment methods
        self.assertIsNone(PaymentMethodAuthentication.objects.first())


class TestSubscriptionCancel(BaseSubscriptionTestCase):
    url_name = 'subscriptions:cancel'

    def test_can_cancel_when_on_hold(self):
        subscription = SubscriptionFactory(
            customer=self.user.customer,
            payment_method__customer_id=self.user.customer.pk,
            payment_method__gateway=Gateway.objects.get(name='bank'),
            status='on-hold',
        )
        self.client.force_login(self.user)

        url = reverse(self.url_name, kwargs={'subscription_id': subscription.pk})
        data = {'confirm': True}
        with patch(
            # Make sure background task is executed as a normal function
            'subscriptions.signals.tasks.send_mail_subscription_status_changed',
            new=subscriptions.tasks.send_mail_subscription_status_changed.task_function,
        ):
            response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], reverse('user-settings-billing'))

        subscription.refresh_from_db()
        self.assertEqual(subscription.status, 'cancelled')
        # No email is sent when: pending-cancellation still means subscription is active

    def test_can_cancel_when_active(self):
        subscription = SubscriptionFactory(
            customer=self.user.customer,
            payment_method__customer_id=self.user.customer.pk,
            payment_method__gateway=Gateway.objects.get(name='bank'),
            status='active',
        )
        self.client.force_login(self.user)

        url = reverse(self.url_name, kwargs={'subscription_id': subscription.pk})
        data = {'confirm': True}
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], reverse('user-settings-billing'))

        subscription.refresh_from_db()
        self.assertEqual(subscription.status, 'pending-cancellation')
        # No email is sent when: pending-cancellation still means subscription is active

    def test_email_sent_when_pending_cancellation_changes_to_cancelled(self):
        subscription = SubscriptionFactory(
            customer=self.user.customer,
            payment_method__customer_id=self.user.customer.pk,
            payment_method__gateway=Gateway.objects.get(name='bank'),
            status='pending-cancellation',
        )

        with patch(
            # Make sure background task is executed as a normal function
            'subscriptions.signals.tasks.send_mail_subscription_status_changed',
            new=subscriptions.tasks.send_mail_subscription_status_changed.task_function,
        ):
            subscription.status = 'cancelled'
            subscription.save()

        subscription.refresh_from_db()
        self.assertEqual(subscription.status, 'cancelled')

        self._assert_subscription_deactivated_email_is_sent(subscription)


class TestPayExistingOrder(BaseSubscriptionTestCase):
    url_name = 'subscriptions:pay-existing-order'

    def test_redirect_to_login_when_anonymous(self):
        subscription = SubscriptionFactory(
            customer=self.user.customer,
            payment_method__customer_id=self.user.customer.pk,
            payment_method__gateway=Gateway.objects.get(name='bank'),
            status='on-hold',
        )
        order = subscription.generate_order()

        self.client.logout()
        url = reverse(self.url_name, kwargs={'order_id': order.pk})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], f'/oauth/login?next={url}')

    def test_cannot_pay_someone_elses_order(self):
        subscription = SubscriptionFactory(
            customer=self.user.customer,
            payment_method__customer_id=self.user.customer.pk,
            payment_method__gateway=Gateway.objects.get(name='bank'),
            status='on-hold',
        )
        order = subscription.generate_order()
        user = UserFactory()

        self.client.force_login(user)
        url = reverse(self.url_name, kwargs={'order_id': order.pk})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 404)

    # @_recorder.record(file_path=f'{responses_dir}stripe_new_cs_usd.yaml')
    # @_recorder.record(file_path=f'{responses_dir}stripe_get_cs_usd.yaml')
    @patch(
        # Make sure background task is executed as a normal function
        'subscriptions.signals.tasks.send_mail_subscription_status_changed',
        new=subscriptions.tasks.send_mail_subscription_status_changed.task_function,
    )
    def test_can_pay_for_manual_subscription_with_an_order(self):
        subscription = SubscriptionFactory(
            plan__name='Automatic renewal subscription',
            customer=self.user.customer,
            payment_method__customer_id=self.user.customer.pk,
            payment_method__gateway=Gateway.objects.get(name='bank'),
            currency='USD',
            price=Money('USD', 1110),
            status='on-hold',
        )
        self.assertEqual(subscription.collection_method, 'automatic')
        order = subscription.generate_order()
        self.client.force_login(self.user)

        url = reverse(self.url_name, kwargs={'order_id': order.pk})
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(f'{responses_dir}stripe_new_cs_usd.yaml')
            response = self.client.get(url)

        self.assertEqual(response.status_code, 302)
        expected_redirect_url = 'https://checkout.stripe.com/c/pay/cs_test_a1XUS0akCexOKoMTKKnt9SjK1UjPI9UTrF7LiLzXWYInOcANZzOFLBkA5w#fidkdWxOYHwnPyd1blpxYHZxWjA0VUpnNzNAMU5EUEcwYW92dWIwclxRMzQ8ZkxsUDRET2dPbTVidnBCNEJTdlBJQTRJYFF2c09BMEFBdlxVT19USGpWSXRSXFJwdm5UQXRpdVw2Rmp%2FZ11NNTU3fHdHUl1JTCcpJ2N3amhWYHdzYHcnP3F3cGApJ2lkfGpwcVF8dWAnPyd2bGtiaWBabHFgaCcpJ2BrZGdpYFVpZGZgbWppYWB3dic%2FcXdwYHgl'
        self.assertEqual(response['Location'], expected_redirect_url)

        # **N.B**: this flow happens in 2 different views separated by a Stripe payment page.
        # Pretend that checkout session was completed and we've returned to the success page with its ID:
        checkout_session_id = 'cs_test_a1XUS0akCexOKoMTKKnt9SjK1UjPI9UTrF7LiLzXWYInOcANZzOFLBkA5w'
        url = reverse(
            'looper:stripe_success',
            kwargs={'pk': order.pk, 'stripe_session_id': 'CHECKOUT_SESSION_ID'},
        )
        url = url.replace('CHECKOUT_SESSION_ID', checkout_session_id)
        with responses.RequestsMock() as rsps:
            responses_from_file('stripe_get_cs_usd.yaml', order_id=order.pk, rsps=rsps)
            response = self.client.get(url)

        self.assertEqual(order.transaction_set.count(), 1)
        transaction = order.latest_transaction()
        self.assertEqual(
            response['Location'],
            reverse('looper:checkout_done', kwargs={'transaction_id': transaction.pk}),
        )
        # New payment method was created
        self.assertEqual(PaymentMethod.objects.count(), 2)

        subscription.refresh_from_db()
        subscription.payment_method.refresh_from_db()
        # Subscription's payment method wasn't changed
        self.assertNotEqual(subscription.payment_method, 'bank')
        self.assertEqual(
            str(subscription.payment_method),
            'PayPal account billing@example.com',
        )

        self.assertEqual(subscription.status, 'active')
        self._assert_subscription_activated_email_is_sent(subscription)
