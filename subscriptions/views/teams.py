"""Views specific to team subscriptions."""
from collections import defaultdict
from django.views.generic.base import TemplateView

from looper.views.checkout_stripe import CheckoutStripeView
import looper.models

import subscriptions.models


class TeamsLanding(TemplateView):
    """Display a selection of team plans and existing sponsors."""

    get_currency = CheckoutStripeView.get_currency
    template_name = 'subscriptions/teams_landing.html'

    @staticmethod
    def _get_team_plans():
        """Return specific team Plans.

        This method will return Plans only if they fulfill certain requirements.
        - they must have TeamPlanProperties defined
        - they only have PlanVariations with automatic collection method

        This is done in order to present a selection of preferred plans to the
        users, without showing all the possible variations at first.
        """
        return (
            looper.models.Plan.objects.filter(
                is_active=True,
                team_properties__isnull=False,
            )
            .exclude(
                variations__collection_method='manual',
            )
            .order_by('team_properties__position')
        )

    def _get_default_plan_variation(self, plan: looper.models.Plan):
        return plan.variation_for_currency(self.get_currency())

    def _get_teams(self):
        query = (
            subscriptions.models.Team.objects.filter(is_visible_as_sponsor=True)
            .select_related(
                'subscription', 'subscription__plan', 'subscription__plan__team_properties'
            )
            .order_by('-subscription__plan__team_properties__position')
        )
        teams = defaultdict(list)
        for team in query:
            display_level = team.subscription.plan.team_properties.level or ''
            teams[display_level.lower()].append(team)

        return [(level, teams[level]) for level in teams]

    def get_context_data(self, **kwargs):
        """Add team plans and existing teams visible as sponsors to the context."""
        context = super().get_context_data(**kwargs)
        context['team_plans'] = []
        for p in self._get_team_plans():
            context['team_plans'].append(
                {'plan': p, 'default_variation': self._get_default_plan_variation(p)}
            )

        context['teams'] = self._get_teams()
        return context
