"""Views handling subscription management."""
import logging

from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy, reverse
from django.views.generic import UpdateView, FormView

import looper.models
import looper.views.settings

from subscriptions.forms import (
    BillingAddressForm,
    CancelSubscriptionForm,
    TeamForm,
)
from subscriptions.views.mixins import SingleSubscriptionMixin, BootstrapErrorListMixin
import subscriptions.models

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class BillingAddressView(looper.views.settings.BillingAddressView):
    """Override form class and success URL of looper's view."""

    template_name = 'settings/billing_address.html'
    form_class = BillingAddressForm
    success_url = reverse_lazy('subscriptions:billing-address')


class CancelSubscriptionView(SingleSubscriptionMixin, FormView):
    """Confirm and cancel a subscription."""

    _log = logger

    template_name = 'subscriptions/cancel.html'
    form_class = CancelSubscriptionForm
    initial = {'confirm': False}
    success_url = reverse_lazy('user-settings-billing')

    def form_valid(self, form):
        """Cancel the subscription."""
        subscription = self.get_subscription()
        self._log.info(
            'Cancelling subscription pk=%d on behalf of user pk=%d',
            subscription.pk,
            self.request.user.pk,
        )
        subscription.cancel_subscription()
        return super().form_valid(form)


class PaymentMethodChangeView(looper.views.settings.PaymentMethodChangeView):
    """Override cancel and success URLs."""

    success_url = 'subscriptions:payment-method-change-done'

    def get_cancel_url(self):
        """Return to this subscription's manage page."""
        return reverse(
            'subscriptions:manage',
            kwargs={'subscription_id': self.kwargs['subscription_id']},
        )


class PaymentMethodChangeDoneView(looper.views.settings.PaymentMethodChangeDoneView):
    """Change payment method in response to a successful payment setup."""

    @property
    def success_url(self):
        """Return to this subscription's manage page."""
        return reverse(
            'subscriptions:manage',
            kwargs={'subscription_id': self.kwargs['subscription_id']},
        )


class PayExistingOrderView(looper.views.checkout_stripe.CheckoutExistingOrderView):
    """Override looper's view with our forms."""

    # Redirect to LOGIN_URL instead of raising an exception
    raise_exception = False

    def get_cancel_url(self):
        """Return to this subscription's manage page."""
        order = self.get_object()
        return reverse('subscriptions:manage', kwargs={'subscription_id': order.subscription_id})


class ManageSubscriptionView(
    SuccessMessageMixin, SingleSubscriptionMixin, BootstrapErrorListMixin, UpdateView
):
    """View and manage a subscription."""

    template_name = 'subscriptions/manage.html'
    form_class = TeamForm
    model = subscriptions.models.Team
    pk_url_kwarg = 'subscription_id'
    success_message = 'Team subscription updated successfully'

    def get_object(self, queryset=None):
        """Get team if this is a team subscription."""
        subscription = self.get_subscription()
        return subscription.team if hasattr(subscription, 'team') else None

    def get_success_url(self):
        """Display the same manage subscription page when done editing the team."""
        return reverse(
            'subscriptions:manage', kwargs={'subscription_id': self.object.subscription_id}
        )
