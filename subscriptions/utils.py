# noqa: D100
import logging

import subscriptions.models

logger = logging.getLogger(__name__)


def configure_team_subscription(subscription: 'subscriptions.models.Subscription'):
    """Configure the team if this is a team plan."""
    if not hasattr(subscription.plan, 'team_properties'):
        return
    team_properties = subscription.plan.team_properties
    team, team_is_new = subscriptions.models.Team.objects.get_or_create(
        subscription=subscription,
        seats=team_properties.seats,
    )
    logger.info(
        '%s a team for subscription pk=%r, seats: %s',
        team_is_new and 'Created' or 'Updated',
        subscription.pk,
        team.seats and team.seats or 'unlimited',
    )
