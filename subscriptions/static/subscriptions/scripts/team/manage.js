document.addEventListener('DOMContentLoaded', () => {
  const elEmails = document.getElementById('id_emails');
  if (!elEmails) {
    return;
  }

  tagify = new Tagify(elEmails, {
    editTags: {
      // better to auto-remove invalid tags which are in edit-mode (on blur)
      keepInvalid: false,
    },
    whitelist: [],
    // email address validation (https://stackoverflow.com/a/46181/104380)
    pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  });
});
