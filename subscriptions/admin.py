from django.contrib import admin
from django.contrib.auth import get_user_model

import looper.admin
import looper.admin.mixins

from subscriptions import models

User = get_user_model()
manager_link = looper.admin.create_admin_fk_link(
    'manager', 'manager', looper.admin._get_admin_url_name(User)
)

# Blender Studio subscriptions override Plan and Subscription, adding team properties
admin.site.unregister(looper.models.Plan)
admin.site.unregister(looper.models.Subscription)


class TeamPlanPropertiesInlineAdmin(admin.TabularInline):
    model = models.TeamPlanProperties
    extra = 0


class TeamInlineAdmin(admin.StackedInline):
    show_change_link = True
    model = models.Team
    extra = 0


class PlanCouponInlineAdmin(admin.TabularInline):
    model = models.PlanCoupon
    extra = 0


@admin.register(looper.models.Plan)
class PlanAdmin(looper.admin.PlanAdmin):
    list_display = looper.admin.PlanAdmin.list_display + [
        '_team_plan_level',
        '_team_plan_seats',
        '_team_plan_position',
    ]
    inlines = looper.admin.PlanAdmin.inlines + [
        TeamPlanPropertiesInlineAdmin,
        PlanCouponInlineAdmin,
    ]

    def _get_team_plan_property(self, obj, attr: str):
        if not hasattr(obj, 'team_properties'):
            return ''
        return getattr(obj.team_properties, attr)

    def _team_plan_seats(self, obj):
        return self._get_team_plan_property(obj, 'seats')

    def _team_plan_position(self, obj):
        return self._get_team_plan_property(obj, 'position')

    def _team_plan_level(self, obj):
        return self._get_team_plan_property(obj, 'level')

    _team_plan_seats.short_description = 'seats'
    _team_plan_position.short_description = 'position'
    _team_plan_level.short_description = 'level'


@admin.register(looper.models.Subscription)
class SubscriptionAdmin(looper.admin.SubscriptionAdmin):
    inlines = [TeamInlineAdmin] + looper.admin.SubscriptionAdmin.inlines


class TeamUserInlineAdmin(
    looper.admin.mixins.NoAddDeleteMixin, looper.admin.mixins.NoChangeMixin, admin.TabularInline
):
    model = models.TeamUsers
    raw_id_fields = ['user', 'team']
    verbose_name = 'team member'
    verbose_name_plural = 'team members'
    extra = 0


@admin.register(models.Team)
class TeamAdmin(admin.ModelAdmin):
    raw_id_fields = ['subscription']
    list_display = [
        '__str__',
        'date_created',
        'email_domain',
        '_get_plan',
        '_get_plan_is_active',
        '_get_user_count',
        'is_visible_as_sponsor',
    ]
    inlines = [TeamUserInlineAdmin]

    @admin.display(description='Is Active', boolean=True)
    def _get_plan_is_active(self, obj):
        if not obj.subscription:
            return False
        return obj.subscription.plan.is_active

    @admin.display(description='Plan')
    def _get_plan(self, obj):
        if not obj.subscription:
            return '-'
        return obj.subscription.plan

    @admin.display(description='User Count')
    def _get_user_count(self, obj):
        return models.TeamUsers.objects.filter(team=obj).count()


@admin.register(models.PlanCoupon)
class PlanCouponAdmin(admin.ModelAdmin):
    pass
