from datetime import timedelta
from unittest.mock import patch, call, Mock
import json

from freezegun import freeze_time

from background_task.models import Task, CompletedTask
from background_task.tasks import tasks
from django.conf import settings
from django.db.models.signals import post_save, pre_delete
from django.test import override_settings
from django.test.testcases import TestCase
from django.utils import timezone

from blog.models import Post
from common.tests.factories.helpers import generate_file_path, catch_signal
from common.tests.factories.projects import ProjectFactory, AssetFactory
from common.tests.factories.training import TrainingFactory
from common.tests.factories.users import UserFactory
from projects.models import Project, ProjectStatus
import search.signals


@override_settings(THUMBNAIL_STORAGE='django.core.files.storage.InMemoryStorage')
@freeze_time('2025-01-30T17:49:29.086Z')
@patch('search.indexer.check_meilisearch')
@patch('django.conf.settings.SEARCH_CLIENT')
class TestBlogPostIndexing(TestCase):
    maxDiff = None

    @classmethod
    def setUpTestData(cls) -> None:
        cls.user = UserFactory()
        cls.post_data = {
            'author': cls.user,
            'title': 'Strawberry Fields Forever',
            'content': '# Hot news',
            'thumbnail': generate_file_path(),
        }

    def test_unpublished_posts_trigger_signal_but_are_not_indexed(
        self, mock_search_client, mock_check_meilisearch
    ):
        with catch_signal(post_save, sender=Post) as handler:
            Post.objects.create(**self.post_data, is_published=False)
            handler.assert_called()
            mock_check_meilisearch.assert_called()
            mock_search_client.assert_not_called()

    def test_new_published_update_indexed(self, mock_search_client, mock_check_meilisearch):
        p = Post.objects.create(**self.post_data, is_published=True)

        mock_check_meilisearch.assert_called()
        expected_data = {
            'id': 1,
            'date_created': '2025-01-30T17:49:29.086Z',
            'date_updated': '2025-01-30T17:49:29.086Z',
            'project_id': None,
            'training_id': None,
            'author_id': p.author_id,
            'slug': 'strawberry-fields-forever',
            'date_published': '2025-01-30T17:49:29.086Z',
            'legacy_id': '',
            'is_published': True,
            'is_subscribers_only': False,
            'title': 'Strawberry Fields Forever',
            'category': 'update',
            'excerpt': '',
            'content': '# Hot news',
            'content_html': '<h1 id="hot-news" class="is-anchor"><a href="#hot-news">Hot news</a></h1>\n',
            'thumbnail': self.post_data['thumbnail'],
            'header': '',
            'model': 'post',
            'search_id': 'post_1',
            'author_name': '',
            'project_title': None,
            'name': 'Strawberry Fields Forever',
            'url': '/blog/strawberry-fields-forever/',
            'timestamp': 1738259369.086,
            'thumbnail_url': p.thumbnail_s_url,
            'tags': [],
            'description': '',
        }
        # Check that post was added into 3 studio* indices
        mock_add_documents = mock_search_client.get_index.return_value.add_documents
        self.assertEqual(mock_add_documents.call_count, 3)
        self.assertEqual(mock_add_documents.mock_calls[0].args[0][0], expected_data)
        self.assertEqual(mock_add_documents.mock_calls[-1].args[0][0], expected_data)
        expected_update_calls = [
            call().add_documents([expected_data]),
            call().update_searchable_attributes(settings.MAIN_SEARCH['SEARCHABLE_ATTRIBUTES']),
            call().update_sortable_attributes(settings.MAIN_SEARCH['SORTABLE_ATTRIBUTES']),
            call().update_filterable_attributes(settings.MAIN_SEARCH['FILTERABLE_ATTRIBUTES']),
        ]
        self.assertEqual(
            mock_search_client.get_index.mock_calls,
            [
                call('studio'),
                *expected_update_calls,
                call('studio_date_desc'),
                *expected_update_calls,
                call('studio_date_asc'),
                *expected_update_calls,
                # This object is not added to other indices
                call('training'),
                call().get_document('post_1'),
                call('training'),
                call().delete_document('post_1'),
                call('training_date_desc'),
                call().get_document('post_1'),
                call('training_date_desc'),
                call().delete_document('post_1'),
                call('training_date_asc'),
                call().get_document('post_1'),
                call('training_date_asc'),
                call().delete_document('post_1'),
            ],
            mock_search_client.get_index.mock_calls,
        )

    def test_new_published_training_article_indexed_twice(
        self, mock_search_client, mock_check_meilisearch
    ):
        p = Post.objects.create(**self.post_data, category='article')
        p.tags.add('training')
        mock_add_documents = mock_search_client.get_index.return_value.add_documents
        mock_add_documents.assert_not_called()

        mock_search_client.reset_mock()
        p.is_published = True
        p.save()

        mock_check_meilisearch.assert_called()
        expected_data = {
            'id': 1,
            'date_created': '2025-01-30T17:49:29.086Z',
            'date_updated': '2025-01-30T17:49:29.086Z',
            'project_id': None,
            'training_id': None,
            'author_id': p.author_id,
            'slug': 'strawberry-fields-forever',
            'date_published': '2025-01-30T17:49:29.086Z',
            'legacy_id': '',
            'is_published': True,
            'is_subscribers_only': False,
            'title': 'Strawberry Fields Forever',
            'category': 'article',
            'excerpt': '',
            'content': '# Hot news',
            'content_html': '<h1 id="hot-news" class="is-anchor"><a href="#hot-news">Hot news</a></h1>\n',
            'thumbnail': self.post_data['thumbnail'],
            'header': '',
            'model': 'post',
            'search_id': 'post_1',
            'author_name': '',
            'project_title': None,
            'name': 'Strawberry Fields Forever',
            'url': '/blog/strawberry-fields-forever/',
            'timestamp': 1738259369.086,
            'thumbnail_url': p.thumbnail_s_url,
            'tags': ['training'],
            'description': '',
        }
        expected_data_t = {
            **expected_data,
            'type': 'Article',
        }
        # Check that post was added into all indices
        mock_add_documents = mock_search_client.get_index.return_value.add_documents
        self.assertEqual(mock_add_documents.call_count, 6)
        self.assertEqual(mock_add_documents.mock_calls[0].args[0][0], expected_data)
        self.assertDictEqual(mock_add_documents.mock_calls[-1].args[0][0], expected_data_t)
        expected_update_calls = [
            call().add_documents([expected_data]),
            call().update_searchable_attributes(settings.MAIN_SEARCH['SEARCHABLE_ATTRIBUTES']),
            call().update_sortable_attributes(settings.MAIN_SEARCH['SORTABLE_ATTRIBUTES']),
            call().update_filterable_attributes(settings.MAIN_SEARCH['FILTERABLE_ATTRIBUTES']),
        ]
        expected_update_calls_t = [
            call().add_documents([expected_data_t]),
            call().update_searchable_attributes(settings.TRAINING_SEARCH['SEARCHABLE_ATTRIBUTES']),
            call().update_sortable_attributes(settings.MAIN_SEARCH['SORTABLE_ATTRIBUTES']),
            call().update_filterable_attributes(settings.TRAINING_SEARCH['FILTERABLE_ATTRIBUTES']),
        ]
        self.assertEqual(
            mock_search_client.get_index.mock_calls,
            [
                call('studio'),
                *expected_update_calls,
                call('studio_date_desc'),
                *expected_update_calls,
                call('studio_date_asc'),
                *expected_update_calls,
                call('training'),
                *expected_update_calls_t,
                call('training_date_desc'),
                *expected_update_calls_t,
                call('training_date_asc'),
                *expected_update_calls_t,
            ],
            mock_search_client.get_index.mock_calls,
        )

    def test_new_published_standalone_indexed(self, mock_search_client, mock_check_meilisearch):
        p = Post.objects.create(**self.post_data, category='standalone', is_published=True)

        mock_check_meilisearch.assert_called()
        expected_data = {
            'id': 1,
            'date_created': '2025-01-30T17:49:29.086Z',
            'date_updated': '2025-01-30T17:49:29.086Z',
            'project_id': None,
            'training_id': None,
            'author_id': p.author_id,
            'slug': 'strawberry-fields-forever',
            'date_published': '2025-01-30T17:49:29.086Z',
            'legacy_id': '',
            'is_published': True,
            'is_subscribers_only': False,
            'title': 'Strawberry Fields Forever',
            'category': 'standalone',
            'excerpt': '',
            'content': '# Hot news',
            'content_html': '<h1 id="hot-news" class="is-anchor"><a href="#hot-news">Hot news</a></h1>\n',
            'thumbnail': self.post_data['thumbnail'],
            'header': '',
            'model': 'post',
            'search_id': 'post_1',
            'author_name': '',
            'project_title': None,
            'name': 'Strawberry Fields Forever',
            'url': '/blog/strawberry-fields-forever/',
            'timestamp': 1738259369.086,
            'thumbnail_url': p.thumbnail_s_url,
            'tags': [],
            'description': '',
        }
        # Check that post was added into 3 studio* indices
        mock_add_documents = mock_search_client.get_index.return_value.add_documents
        self.assertEqual(mock_add_documents.call_count, 3)
        self.assertEqual(mock_add_documents.mock_calls[0].args[0][0], expected_data)
        self.assertEqual(mock_add_documents.mock_calls[-1].args[0][0], expected_data)
        expected_update_calls = [
            call().add_documents([expected_data]),
            call().update_searchable_attributes(settings.MAIN_SEARCH['SEARCHABLE_ATTRIBUTES']),
            call().update_sortable_attributes(settings.MAIN_SEARCH['SORTABLE_ATTRIBUTES']),
            call().update_filterable_attributes(settings.MAIN_SEARCH['FILTERABLE_ATTRIBUTES']),
        ]
        self.assertEqual(
            mock_search_client.get_index.mock_calls,
            [
                call('studio'),
                *expected_update_calls,
                call('studio_date_desc'),
                *expected_update_calls,
                call('studio_date_asc'),
                *expected_update_calls,
                # This object is not added to other indices
                call('training'),
                call().get_document('post_1'),
                call('training'),
                call().delete_document('post_1'),
                call('training_date_desc'),
                call().get_document('post_1'),
                call('training_date_desc'),
                call().delete_document('post_1'),
                call('training_date_asc'),
                call().get_document('post_1'),
                call('training_date_asc'),
                call().delete_document('post_1'),
            ],
            mock_search_client.get_index.mock_calls,
        )

    def test_future_published_training_article_indexed_later(self, mock_search_client, _):
        p = Post.objects.create(**self.post_data, category='article')
        p.tags.add('training')
        mock_add_documents = mock_search_client.get_index.return_value.add_documents
        mock_add_documents.assert_not_called()
        self.assertFalse(Task.objects.exists())

        p.is_published = True
        p.date_published = timezone.now() + timedelta(days=10)
        p.save()

        # Check that a task for indexing the post later was created
        mock_add_documents = mock_search_client.get_index.return_value.add_documents
        mock_add_documents.assert_not_called()
        self.assertEqual(Task.objects.count(), 2)
        task_1, task_2 = list(Task.objects.order_by('pk'))
        self.assertEqual(task_1.task_name, 'search.indexer.index_instance_async')
        self.assertEqual(task_2.task_name, 'search.indexer.index_instance_async')
        self.assertEqual(
            json.loads(task_1.task_params),
            [
                [],
                {
                    'index_uids': ['studio', 'studio_date_desc', 'studio_date_asc'],
                    'indexer_type': 'MainPostSaveSearchIndexer',
                    'model_name': 'post',
                    'pk': 1,
                },
            ],
        )
        self.assertEqual(
            json.loads(task_2.task_params),
            [
                [],
                {
                    'index_uids': ['training', 'training_date_desc', 'training_date_asc'],
                    'indexer_type': 'TrainingPostSaveSearchIndexer',
                    'model_name': 'post',
                    'pk': 1,
                },
            ],
        )

        # The indexing will run more than 10 days later, at 1 minute over publish date
        self.assertGreater(task_1.run_at - p.date_created, timedelta(days=10))
        self.assertEqual(task_1.run_at, p.date_published + timedelta(seconds=60))
        self.assertEqual(task_1.run_at.isoformat(), '2025-02-09T17:50:29.086000+00:00')

        self.assertGreater(task_2.run_at - p.date_created, timedelta(days=10))
        self.assertEqual(task_2.run_at, p.date_published + timedelta(seconds=60))
        self.assertEqual(task_2.run_at.isoformat(), '2025-02-09T17:50:29.086000+00:00')

        # Changing the publish date shouldn't create any new tasks
        p.date_published = timezone.now() + timedelta(days=5)
        p.save()

        mock_add_documents.assert_not_called()
        self.assertEqual(Task.objects.count(), 2)
        # Now the indexing will run more than 5 days later, at 1 minute over publish date
        task_1.refresh_from_db()
        task_2.refresh_from_db()
        self.assertGreater(task_1.run_at - p.date_created, timedelta(days=5))
        self.assertEqual(task_1.run_at, p.date_published + timedelta(seconds=60))
        self.assertEqual(task_1.run_at.isoformat(), '2025-02-04T17:50:29.086000+00:00')

        self.assertGreater(task_2.run_at - p.date_created, timedelta(days=5))
        self.assertEqual(task_2.run_at, p.date_published + timedelta(seconds=60))
        self.assertEqual(task_2.run_at.isoformat(), '2025-02-04T17:50:29.086000+00:00')

        # Calling "Reindex" shouldn't create any new tasks
        search.signals.reindex(None, None, Post.objects.filter(pk=p.pk))

        # Nothing changed
        mock_add_documents.assert_not_called()
        self.assertEqual(Task.objects.count(), 2)
        task_1.refresh_from_db()
        task_2.refresh_from_db()
        self.assertGreater(task_1.run_at - p.date_created, timedelta(days=5))
        self.assertEqual(task_1.run_at, p.date_published + timedelta(seconds=60))
        self.assertEqual(task_1.run_at.isoformat(), '2025-02-04T17:50:29.086000+00:00')

        self.assertGreater(task_2.run_at - p.date_created, timedelta(days=5))
        self.assertEqual(task_2.run_at, p.date_published + timedelta(seconds=60))
        self.assertEqual(task_2.run_at.isoformat(), '2025-02-04T17:50:29.086000+00:00')

        # Check that tasks can actually run
        with override_settings(BACKGROUND_TASK_RUN_ASYNC=False):
            with freeze_time('2025-03-04T17:50:29.086000+00:00'):
                tasks.run_task(task_1)
                tasks.run_task(task_2)
        self.assertEqual(Task.objects.count(), 0)
        self.assertEqual(CompletedTask.objects.count(), 2)
        mock_add_documents.assert_called()
        self.assertEqual(len(mock_add_documents.mock_calls), 6)


class TestPostDeleteSignal(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.user = UserFactory()
        cls.project_data = {
            'title': 'Strawberry Fields Forever',
            'slug': 'strawberry-fields-forever',
            'description': 'Living is easy with eyes closed',
            'summary': 'Misunderstanding all you see',
            'status': ProjectStatus.released.value,
            'is_published': True,
            'logo': generate_file_path(),
            'poster': generate_file_path(),
            'picture_header': generate_file_path(),
            'thumbnail': generate_file_path(),
        }

    def setUp(self) -> None:
        self.project_not_indexed = ProjectFactory()

    def test_deleting_not_indexed_project_triggers_signal(self):
        with catch_signal(pre_delete, sender=Project) as handler:
            self.project_not_indexed.delete()
            handler.assert_called()

    def test_deleting_indexed_project_triggers_signal(self):
        project = Project.objects.create(**self.project_data)

        with catch_signal(pre_delete, sender=Project) as handler:
            project.delete()
            handler.assert_called()


@override_settings(THUMBNAIL_STORAGE='django.core.files.storage.InMemoryStorage')
@freeze_time('2025-01-30T17:49:29.086Z')
@patch('search.indexer.check_meilisearch')
@patch('django.conf.settings.SEARCH_CLIENT')
class TestTrainingIndexing(TestCase):
    maxDiff = None

    def test_not_published(self, mock_search_client, mock_check_meilisearch):
        t = TrainingFactory(is_published=False)
        # Nothing indexed yet
        mock_add_documents = mock_search_client.get_index.return_value.add_documents
        mock_add_documents.assert_not_called()

        t.name = 'Name changed'
        t.save()

        # Nothing indexed still
        mock_add_documents.assert_not_called()

    def test_published(self, mock_search_client, mock_check_meilisearch):
        slug, name, summary, description = (
            'wonder-bag',
            'Wonder bag',
            'Strategy section wonder',
            'Talk such race idea different help international.',
        )
        t = TrainingFactory(
            is_published=False,
            slug=slug,
            name=name,
            summary=summary,
            description=description,
        )
        # Nothing indexed yet
        mock_add_documents = mock_search_client.get_index.return_value.add_documents
        mock_add_documents.assert_not_called()

        t.is_published = True
        t.save()

        expected_data = {
            'id': 1,
            'date_created': '2025-01-30T17:49:29.086Z',
            'date_updated': '2025-01-30T17:49:29.086Z',
            'name': name,
            'slug': slug,
            'description': description,
            'summary': summary,
            'is_published': True,
            'is_featured': False,
            'type': t.type,
            'difficulty': t.difficulty,
            'picture_header': t.picture_header.name,
            'thumbnail': t.thumbnail.name,
            'show_blog_posts': False,
            'model': 'training',
            'search_id': 'training_1',
            'url': '/training/wonder-bag/',
            'timestamp': 1738259369.086,
            'thumbnail_url': t.thumbnail_s_url,
            'tags': [],
            'secondary_tags': [],
        }
        # Indexed data for training* index has extra fields
        expected_data_t = {
            **expected_data,
            'favorite_url': '/training/api/trainings/1/favorite/',
            'is_free': True,
        }

        # Check that training was added into all indices
        mock_add_documents = mock_search_client.get_index.return_value.add_documents
        self.assertEqual(mock_add_documents.call_count, 6)
        self.assertEqual(mock_add_documents.mock_calls[0].args[0][0], expected_data)
        self.assertEqual(mock_add_documents.mock_calls[-1].args[0][0], expected_data_t)
        expected_update_calls = [
            call().add_documents([expected_data]),
            call().update_searchable_attributes(settings.MAIN_SEARCH['SEARCHABLE_ATTRIBUTES']),
            call().update_sortable_attributes(settings.MAIN_SEARCH['SORTABLE_ATTRIBUTES']),
            call().update_filterable_attributes(settings.MAIN_SEARCH['FILTERABLE_ATTRIBUTES']),
        ]
        expected_update_calls_t = [
            call().add_documents([expected_data_t]),
            call().update_searchable_attributes(settings.TRAINING_SEARCH['SEARCHABLE_ATTRIBUTES']),
            call().update_sortable_attributes(settings.MAIN_SEARCH['SORTABLE_ATTRIBUTES']),
            call().update_filterable_attributes(settings.TRAINING_SEARCH['FILTERABLE_ATTRIBUTES']),
        ]
        self.assertEqual(
            mock_search_client.get_index.mock_calls,
            [
                call('studio'),
                *expected_update_calls,
                call('studio_date_desc'),
                *expected_update_calls,
                call('studio_date_asc'),
                *expected_update_calls,
                call('training'),
                *expected_update_calls_t,
                call('training_date_desc'),
                *expected_update_calls_t,
                call('training_date_asc'),
                *expected_update_calls_t,
            ],
            mock_search_client.get_index.mock_calls,
        )


@override_settings(THUMBNAIL_STORAGE='django.core.files.storage.InMemoryStorage')
@freeze_time('2025-01-30T17:49:29.086Z')
@patch('search.indexer.check_meilisearch', Mock(return_value=True))
@patch('django.conf.settings.SEARCH_CLIENT')
class TestAssetIndexing(TestCase):
    maxDiff = None

    def test_not_published(self, mock_search_client):
        a = AssetFactory(is_published=False)
        a.name = 'Name changed'
        a.save()

        # Nothing indexed
        mock_add_documents = mock_search_client.get_index.return_value.add_documents
        mock_add_documents.assert_not_called()

    def test_collection_artwork_published(self, mock_search_client):
        name, description, slug, title, project_slug, collection_slug, collection_name = (
            'Awesome Asset',
            'Asset Description Words Here',
            'concept-asset',
            'Super Duper Demo',
            'super-duper-demo',
            'light-and-shadow',
            'Light and Shadow',
        )
        a = AssetFactory(
            slug=slug,
            name=name,
            category='artwork',
            description=description,
            is_published=False,
            is_featured=False,
            project__is_published=True,
            project__slug=project_slug,
            project__title=title,
            static_asset__license__name='CC-0',
            collection__slug=collection_slug,
            collection__name=collection_name,
        )
        mock_add_documents = mock_search_client.get_index.return_value.add_documents
        # Nothing indexed yet
        mock_add_documents.assert_not_called()

        a.is_published = True
        a.name = 'New name'
        a.save()

        expected_data = {
            'id': 1,
            'date_created': '2025-01-30T17:49:29.086Z',
            'date_updated': '2025-01-30T17:49:29.086Z',
            'date_published': '2025-01-30T17:49:29.086Z',
            'project_id': 1,
            'static_asset_id': 1,
            'collection_id': 1,
            'order': None,
            'name': 'New name',
            'slug': 'concept-asset',
            'description': description,
            'category': 'artwork',
            'view_count': 0,
            'is_published': True,
            'is_featured': False,
            'is_free': False,
            'is_spoiler': False,
            'contains_blend_file': False,
            'model': 'asset',
            'search_id': 'asset_1',
            'author_name': '',
            'project_title': 'Super Duper Demo',
            'url': '/projects/super-duper-demo/light-and-shadow/?asset=1',
            'timestamp': 1738259369.086,
            'thumbnail_url': a.static_asset.thumbnail_s_url,
            'tags': [],
            'collection_name': 'Light and Shadow',
            'free': 'false',  # FIXME: why is duplicate `is_free` like this?
            'license': 'CC-0',
            'media_type': 'video',
        }

        # Check that asset was added into studio* index
        self.assertEqual(mock_add_documents.call_count, 3)
        self.assertEqual(
            mock_add_documents.mock_calls[0].args[0][0],
            expected_data,
            mock_add_documents.mock_calls[0].args[0][0],
        )
        self.assertEqual(mock_add_documents.mock_calls[-1].args[0][0], expected_data)
        expected_update_calls = [
            call().add_documents([expected_data]),
            call().update_searchable_attributes(settings.MAIN_SEARCH['SEARCHABLE_ATTRIBUTES']),
            call().update_sortable_attributes(settings.MAIN_SEARCH['SORTABLE_ATTRIBUTES']),
            call().update_filterable_attributes(settings.MAIN_SEARCH['FILTERABLE_ATTRIBUTES']),
        ]
        self.assertEqual(
            mock_search_client.get_index.mock_calls,
            [
                call('studio'),
                *expected_update_calls,
                call('studio_date_desc'),
                *expected_update_calls,
                call('studio_date_asc'),
                *expected_update_calls,
                # This object is not added to other indices
                call('training'),
                call().get_document('asset_1'),
                call('training'),
                call().delete_document('asset_1'),
                call('training_date_desc'),
                call().get_document('asset_1'),
                call('training_date_desc'),
                call().delete_document('asset_1'),
                call('training_date_asc'),
                call().get_document('asset_1'),
                call('training_date_asc'),
                call().delete_document('asset_1'),
            ],
            mock_search_client.get_index.mock_calls,
        )

    def test_featured_artwork_published(self, mock_search_client):
        name, description, slug, title, project_slug = (
            'Awesome Asset',
            'Asset Description Words Here',
            'concept-asset',
            'Super Duper Demo',
            'super-duper-demo',
        )
        a = AssetFactory(
            slug=slug,
            name=name,
            category='artwork',
            description=description,
            is_published=False,
            is_featured=True,
            project__is_published=True,
            project__slug=project_slug,
            project__title=title,
            static_asset__license__name='CC-0',
            collection=None,
        )
        mock_add_documents = mock_search_client.get_index.return_value.add_documents
        # Nothing indexed yet
        mock_add_documents.assert_not_called()

        a.is_published = True
        a.name = 'New name'
        a.save()

        expected_data = {
            'author_name': '',
            'category': 'artwork',
            'collection_id': None,
            'collection_name': None,
            'contains_blend_file': False,
            'date_created': '2025-01-30T17:49:29.086Z',
            'date_published': '2025-01-30T17:49:29.086Z',
            'date_updated': '2025-01-30T17:49:29.086Z',
            'description': description,
            'free': 'false',  # FIXME: why is duplicate `is_free` like this?
            'id': 1,
            'is_featured': True,
            'is_free': False,
            'is_published': True,
            'is_spoiler': False,
            'license': 'CC-0',
            'media_type': 'video',
            'model': 'asset',
            'name': 'New name',
            'order': None,
            'project_id': 1,
            'project_title': 'Super Duper Demo',
            'search_id': 'asset_1',
            'slug': 'concept-asset',
            'static_asset_id': 1,
            'tags': [],
            'thumbnail_url': a.static_asset.thumbnail_s_url,
            'timestamp': 1738259369.086,
            'url': '/projects/super-duper-demo/gallery/?asset=1',
            'view_count': 0,
        }

        # Check that asset was added into studio* index
        self.assertEqual(mock_add_documents.call_count, 3)
        self.assertEqual(
            mock_add_documents.mock_calls[0].args[0][0],
            expected_data,
            mock_add_documents.mock_calls[0].args[0][0],
        )
        self.assertEqual(mock_add_documents.mock_calls[-1].args[0][0], expected_data)
        expected_update_calls = [
            call().add_documents([expected_data]),
            call().update_searchable_attributes(settings.MAIN_SEARCH['SEARCHABLE_ATTRIBUTES']),
            call().update_sortable_attributes(settings.MAIN_SEARCH['SORTABLE_ATTRIBUTES']),
            call().update_filterable_attributes(settings.MAIN_SEARCH['FILTERABLE_ATTRIBUTES']),
        ]
        self.assertEqual(
            mock_search_client.get_index.mock_calls,
            [
                call('studio'),
                *expected_update_calls,
                call('studio_date_desc'),
                *expected_update_calls,
                call('studio_date_asc'),
                *expected_update_calls,
                # This object is not added to other indices
                call('training'),
                call().get_document('asset_1'),
                call('training'),
                call().delete_document('asset_1'),
                call('training_date_desc'),
                call().get_document('asset_1'),
                call('training_date_desc'),
                call().delete_document('asset_1'),
                call('training_date_asc'),
                call().get_document('asset_1'),
                call('training_date_asc'),
                call().delete_document('asset_1'),
            ],
            mock_search_client.get_index.mock_calls,
        )

    def test_neither_in_collection_nor_featured_not_published(self, mock_search_client):
        name, description, slug, title, project_slug = (
            'Awesome Asset',
            'Asset Description Words Here',
            'concept-asset',
            'Super Duper Demo',
            'super-duper-demo',
        )
        a = AssetFactory(
            slug=slug,
            name=name,
            category='artwork',
            description=description,
            is_published=False,
            project__is_published=True,
            project__slug=project_slug,
            project__title=title,
            static_asset__license__name='CC-0',
            # This kind of asset has no public URL, indexing it makes no sense
            collection=None,
            is_featured=False,
        )
        mock_add_documents = mock_search_client.get_index.return_value.add_documents
        # Nothing indexed yet
        mock_add_documents.assert_not_called()

        a.is_published = True
        a.name = 'New name'
        a.save()

        # Check that nothing was added to the index
        mock_add_documents.assert_not_called()

    def test_production_lesson_published(self, mock_search_client):
        name, description, slug, title, project_slug, collection_slug, collection_name = (
            'Awesome Asset',
            'Asset Description Words Here',
            'concept-asset',
            'Super Duper Demo',
            'super-duper-demo',
            'light-and-shadow',
            'Light and Shadow',
        )
        a = AssetFactory(
            slug=slug,
            name=name,
            description=description,
            category='production_lesson',
            is_published=False,
            project__is_published=True,
            project__slug=project_slug,
            project__title=title,
            static_asset__license__name='CC-0',
            collection__slug=collection_slug,
            collection__name=collection_name,
        )
        mock_add_documents = mock_search_client.get_index.return_value.add_documents
        # Nothing indexed yet
        mock_add_documents.assert_not_called()

        a.is_published = True
        a.name = 'New name'
        a.save()

        data_shared = {
            'id': 1,
            'date_created': '2025-01-30T17:49:29.086Z',
            'date_updated': '2025-01-30T17:49:29.086Z',
            'date_published': '2025-01-30T17:49:29.086Z',
            'project_id': 1,
            'static_asset_id': 1,
            'collection_id': 1,
            'order': None,
            'name': 'New name',
            'slug': 'concept-asset',
            'description': description,
            'category': 'production_lesson',
            'view_count': 0,
            'is_published': True,
            'is_featured': False,
            'is_free': False,
            'is_spoiler': False,
            'contains_blend_file': False,
            'model': 'asset',
            'search_id': 'asset_1',
            'author_name': '',
            'project_title': 'Super Duper Demo',
            'url': '/projects/super-duper-demo/light-and-shadow/?asset=1',
            'timestamp': 1738259369.086,
            'thumbnail_url': a.static_asset.thumbnail_s_url,
            'tags': [],
        }
        # Indexed data for studio* and training* differ
        expected_data = {
            **data_shared,
            'collection_name': 'Light and Shadow',
            'free': 'false',  # FIXME: why is duplicate `is_free` like this?
            'license': 'CC-0',
            'media_type': 'video',
        }
        expected_data_t = {
            **data_shared,
            'type': 'production lesson',
        }

        # Check that training was added into all indices
        mock_add_documents = mock_search_client.get_index.return_value.add_documents
        self.assertEqual(mock_add_documents.call_count, 6)
        self.assertEqual(
            mock_add_documents.mock_calls[0].args[0][0],
            expected_data,
            mock_add_documents.mock_calls[0].args[0][0],
        )
        self.assertEqual(mock_add_documents.mock_calls[-1].args[0][0], expected_data_t)
        expected_update_calls = [
            call().add_documents([expected_data]),
            call().update_searchable_attributes(settings.MAIN_SEARCH['SEARCHABLE_ATTRIBUTES']),
            call().update_sortable_attributes(settings.MAIN_SEARCH['SORTABLE_ATTRIBUTES']),
            call().update_filterable_attributes(settings.MAIN_SEARCH['FILTERABLE_ATTRIBUTES']),
        ]
        expected_update_calls_t = [
            call().add_documents([expected_data_t]),
            call().update_searchable_attributes(settings.TRAINING_SEARCH['SEARCHABLE_ATTRIBUTES']),
            call().update_sortable_attributes(settings.MAIN_SEARCH['SORTABLE_ATTRIBUTES']),
            call().update_filterable_attributes(settings.TRAINING_SEARCH['FILTERABLE_ATTRIBUTES']),
        ]
        self.assertEqual(
            mock_search_client.get_index.mock_calls,
            [
                call('studio'),
                *expected_update_calls,
                call('studio_date_desc'),
                *expected_update_calls,
                call('studio_date_asc'),
                *expected_update_calls,
                call('training'),
                *expected_update_calls_t,
                call('training_date_desc'),
                *expected_update_calls_t,
                call('training_date_asc'),
                *expected_update_calls_t,
            ],
            mock_search_client.get_index.mock_calls,
        )
