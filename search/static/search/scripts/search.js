/* eslint-disable no-undef, no-unused-vars, no-nested-ternary */
const searchClientConfig = JSON.parse(document.getElementById('search-client-config').textContent);

const indexName = 'studio_date_desc';
const search = instantsearch({
  indexName,
  searchClient: instantMeiliSearch(searchClientConfig.hostUrl, searchClientConfig.apiKey),
  routing: {
    stateMapping: {
      stateToRoute(uiState) {
        const indexUiState = uiState[indexName];
        return {
          query: indexUiState.query,
          type: indexUiState.menu && indexUiState.menu.model,
          media_type: indexUiState.menu && indexUiState.menu.media_type,
          // license: indexUiState.menu && indexUiState.menu.license,
          tags: indexUiState.refinementList && indexUiState.refinementList.tags,
          project_title: indexUiState.menu && indexUiState.menu.project_title,
          sortBy: indexUiState && indexUiState.sortBy,
        };
      },
      routeToState(routeState) {
        return {
          [indexName]: {
            query: routeState.query,
            sortBy: routeState.sortBy,
            refinementList: {
              tags: routeState.tags,
            },
            menu: {
              model: routeState.type,
              categories: routeState.categories,
              media_type: routeState.media_type,
              // license: routeState.license,
              project_title: routeState.project_title,
            },
          },
        };
      },
    },
  },
});

// -------- INPUT -------- //

// Create a render function
const renderSearchBox = (renderOptions, isFirstRender) => {
  const { query, refine, clear } = renderOptions;

  if (isFirstRender) {
    const input = document.getElementById('searchInput');
    input.value = query;
    const clearButton = document.getElementById('clearSearchBtn');
    let typingTimer;
    input.addEventListener('input', (event) => {
      clearTimeout(typingTimer); // Clear the previous timer
      typingTimer = setTimeout(() => {
        refine(input.value); // Call refine after delay
      }, 200); // 200ms delay
    });
    clearButton.addEventListener('click', () => {
      clear();
      search.helper.clearRefinements('tags').search();
      input.value = '';
    });
  }
};

// create custom widget
const customSearchBox = instantsearch.connectors.connectSearchBox(renderSearchBox);

// -------- SORTING -------- //

// Create the render function
const renderSortBy = (renderOptions, isFirstRender) => {
  const { options, currentRefinement, hasNoResults, refine, widgetParams } = renderOptions;

  if (isFirstRender) {
    const select = document.createElement('select');
    select.setAttribute('class', 'form-select');

    select.addEventListener('change', (event) => {
      refine(event.target.value);
    });

    widgetParams.container.insertAdjacentElement('beforeend', select);
  }

  const select = widgetParams.container.querySelector('select');

  select.disabled = hasNoResults;

  select.innerHTML = `
    ${options
      .map(
        (option) => `
          <option
            value="${option.value}"
            ${option.value === currentRefinement ? 'selected' : ''}
          >
            ${option.label}
          </option>
        `
      )
      .join('')}
  `;
};

// Create the custom widget
const customSortBy = instantsearch.connectors.connectSortBy(renderSortBy);

// -------- HITS -------- //

let lastRenderArgs;

// Create the render function
const renderHits = (renderOptions, isFirstRender) => {
  const { hits, showMore, widgetParams } = renderOptions;
  const modelLabelsToHide = ['section', 'project', 'asset'];
  const categoryLabelsToHide = ['other', 'standalone'];

  /* Debug. */
  // hits.map((item) => console.log(item));

  widgetParams.container.innerHTML = `
      ${hits
        .map(
          (item) =>
            `
<div class="card-grid-item cards-item ${item.is_spoiler ? 'is-spoiler' : ''}">
  <a aria-label="${item.name}" class="cards-item-content" href="${item.url}">
    ${item.is_featured ? '<div class="cards-item-featured" title="Featured"><i class="i-star"></i></div>' : ''}
    <div class="cards-item-thumbnail">
        ${
        item.thumbnail_url
          ? `<img alt="${item.name}" aria-label="${item.name}" src="${item.thumbnail_url}">`
          : `<div class="cards-item-thumbnail-icon"><i class="i-mediatype-${item.media_type}"></i></div>`
        }
        ${
        item.contains_blend_file
          ? `
            <div class="cards-item-thumbnail-icon-overlay"><i class="i-blender"></i></div>
          `
          : ''
        }
      ${item.is_spoiler ? '<div class="cards-item-spoiler" title="Spoiler"><i class="i-alert-triangle"></i> Spoiler</div>' : ''}
    </div>
    <div class="cards-item-headline">
      ${item.type ? '<span class="text-capitalize">' + item.type + '</span>' : ''}

      ${item.model === 'section' ? item.training_name : ''}
      ${item.project_title ? item.project_title : ''}
      ${item.collection_name ? ' <span class="opacity-50"><i class="i-chevron-right"></i></span> ' + item.collection_name : ''}
    </div>
    <h3 class="cards-item-title fs-base">${instantsearch.highlight({ attribute: 'name', hit: item })}</h3>
    <div class="cards-item-extra">
      <ul class="list-inline w-100">
        ${!modelLabelsToHide.includes(item.model) ? '<li>' + item.model + '</li>' : ''}

        ${item.category && !categoryLabelsToHide.includes(item.category) ? '<li>' + item.category.replace(/_/g, ' ') + '</li>' : ''}
        ${item.category == 'other' ? '<li>Project</li>' : '' }
        ${item.category == 'standalone' ? '<li>Standalone Project</li>' : '' }

        ${item.media_type ? '<li>' + item.media_type + '</li>' : ''}
        ${
        item.is_free
          ? `
            <li><span class="badge">Free</span></li>
          `
          : ''
        }
        ${item.author ? '<li>' + item.author + '</li>' : '' }

        <li class="ms-auto">
          ${epochToFormattedDate(item.timestamp)}
        </li>
      </ul>
    </div>
  </a>
</div>
            `
        )
        .join('')}
  `;

  lastRenderArgs = renderOptions;

  if (isFirstRender) {
    const sentinel = document.createElement('div');
    widgetParams.container.insertAdjacentElement('afterend', sentinel);

    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting && !lastRenderArgs.isLastPage) {
          showMore();
        }
      });
    });

    observer.observe(sentinel);

    // When the entirety of the first page and the sentinel fit into the viewport, observer doesn't fire.
    // This checks if the sentinel is **still** within the viewport and calls showMore,
    // otherwise the next page is never shown unless the browser window is resized.
    // This way showMore will be called at the end of one or more renders, for example
    // if someone is viewing the search page on a large vertical monitor.
    search.on('render', function() {
      const shouldShowMoreAlready = isElementInViewport(sentinel);
      const infiniteHits = search.renderState[search.indexName].infiniteHits;
      if (shouldShowMoreAlready && !infiniteHits.isLastPage) {
        infiniteHits.showMore();
      };
    });
  }
};

const customHits = instantsearch.connectors.connectInfiniteHits(renderHits);

function valueNotEmpty(el) {
  if (el.value !== false || el.value !== null || el.value !== 0 || el.value !== '') {
    return el.value;
  }
  return null;
}

function filterEmpty(arr) {
  return arr.filter(valueNotEmpty);
}

// -------- FILTERS -------- //

// 1. Create a render function
const renderMenuSelect = (renderOptions, isFirstRender) => {
  const { items, canRefine, refine, widgetParams } = renderOptions;

  if (isFirstRender) {
    const select = document.createElement('select');

    select.setAttribute('class', 'form-select');
    select.addEventListener('change', (event) => {
      refine(event.target.value);
    });

    widgetParams.container.insertAdjacentElement('beforeend', select);
    // widgetParams.container.appendChild(select);
  }

  const select = widgetParams.container.querySelector('select');

  select.disabled = !canRefine;

  select.innerHTML = `
    <option value="">${widgetParams.placeholder}</option>
    ${filterEmpty(items)
      .map(
        (item) =>
          `<option
            value="${item.value}"
            ${item.isRefined ? 'selected' : ''}
          >
            ${item.label}
          </option>`
      )
      .join('')}
  `;
};

// 2. Create the custom widget
const customMenuSelect = instantsearch.connectors.connectMenu(renderMenuSelect);

// -------- CONFIGURE -------- //

const renderConfigure = (renderOptions, isFirstRender) => {};

const customConfigure = instantsearch.connectors.connectConfigure(renderConfigure, () => {});

// -------- RENDER -------- //
search.addWidgets([
  customSearchBox({
    container: document.querySelector('#search-container'),
  }),
  customHits({
    container: document.querySelector('#hits'),
  }),
  instantsearch.widgets.refinementList({
    container: '#tags-refinement',
    attribute: 'tags',
    operator: 'and', // Use 'or' for broader filtering
    sortBy: ['count:desc'],
    limit: 20, // Number of tags to display initially
    showMore: true, // Enable "Show more" option
    showMoreLimit: 40, // Total number of tags to show after clicking "Show more"
  }),
  // customMenu({
  //   container: document.querySelector('#filters'),
  //   attribute: 'categories',
  //   showMoreLimit: 20,
  // }),
  customMenuSelect({
    container: document.querySelector('#searchType'),
    attribute: 'model',
    placeholder: 'All Types',
  }),
  // customMenuSelect({
  //   container: document.querySelector('#searchLicense'),
  //   attribute: 'license',
  // }),
  customMenuSelect({
    container: document.querySelector('#searchMedia'),
    attribute: 'media_type',
    placeholder: 'All Media',
  }),
  customMenuSelect({
    container: document.querySelector('#searchFilm'),
    attribute: 'project_title',
    placeholder: 'All Projects',
    // There's no way to have no limit for menu items and showMore makes no sense for a dropdown
    // See https://www.algolia.com/doc/api-reference/widgets/menu/js/#widget-param-limit
    limit: 999,
  }),
  // customMenuSelect({
  //   container: document.querySelector('#searchFree'),
  //   attribute: 'free',
  // }),
  customSortBy({
    container: document.querySelector('#sorting'),
    items: [
      { label: 'Relevance', value: 'studio' },
      { label: 'Date (new first)', value: 'studio_date_desc' },
      { label: 'Date (old first)', value: 'studio_date_asc' },
    ],
  }),
  customSortBy({
    container: document.querySelector('#sorting-mobile'),
    items: [
      { label: 'Relevance', value: 'studio' },
      { label: 'Date (new first)', value: 'studio_date_asc' },
      { label: 'Date (old first)', value: 'studio_date_desc' },
    ],
  }),
  customConfigure({
    container: document.querySelector('#hits'),
    searchParameters: {
      hitsPerPage: 9,
    },
  }),
]);

search.start();

document.addEventListener('DOMContentLoaded', () => {
  document.querySelector('#searchInput').focus();
});
