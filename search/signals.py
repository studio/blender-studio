import logging
from typing import Type, Any

from django.conf import settings
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from blog.models import Post
from projects.models import Project, Asset
from search import ALL_INDEX_UIDS
from search.indexer import (
    MainPostSaveSearchIndexer,
    TrainingPostSaveSearchIndexer,
    instance_search_id,
    is_search_service_available,
)
from search.serializers.base import SearchableModel
from training.models import Training, Section

log = logging.getLogger(__name__)


@receiver(post_save, sender=Project)
@receiver(post_save, sender=Asset)
@receiver(post_save, sender=Training)
@receiver(post_save, sender=Section)
@receiver(post_save, sender=Post)
def update_main_search_indexes(
    sender: Type[SearchableModel], instance: SearchableModel, raw, **kwargs: Any
) -> None:
    """Adds new objects to the main search indexes and updates the updated ones."""
    if raw:
        return

    indexer = MainPostSaveSearchIndexer()
    indexer.handle(sender=sender, instance=instance)


@receiver(post_save, sender=Training)
@receiver(post_save, sender=Asset)
@receiver(post_save, sender=Post)
def update_training_search_index(
    sender: Type[SearchableModel], instance: SearchableModel, raw, **kwargs: Any
) -> None:
    """Adds new objects to the training search index and updates the updated ones.

    For the training search, Trainings, Sections, production lessons (an Asset
    category) and blog posts articles linked to a training are indexed.
    """
    if raw:
        return

    indexer = TrainingPostSaveSearchIndexer()
    indexer.handle(sender=sender, instance=instance)


@receiver(post_save, sender=Section)
def update_training_search_index_on_section_update(
    sender: Type[Section], instance: Section, raw, **kwargs: Any
) -> None:
    """Add or update Training when its Section is updated.

    Some properties of Training depend on its Sections, hence this separate signal.
    """
    if raw:
        return

    training = instance.chapter.training

    indexer = TrainingPostSaveSearchIndexer()
    indexer.handle(sender=Training, instance=training)
    indexer = MainPostSaveSearchIndexer()
    indexer.handle(sender=Training, instance=training)


@receiver(pre_delete, sender=Project)
@receiver(pre_delete, sender=Asset)
@receiver(pre_delete, sender=Training)
@receiver(pre_delete, sender=Section)
@receiver(pre_delete, sender=Post)
def delete_from_index(
    sender: Type[SearchableModel], instance: SearchableModel, **kwargs: Any
) -> None:
    """On object deletion, send a request to remove the related document from all indexes.

    If there is no related document in an index, nothing happens.
    """
    if not is_search_service_available():
        log.error('Did not delete from search indexes: search service unavailable')
        return

    search_id = instance_search_id(sender, instance)

    for index_uid in ALL_INDEX_UIDS:
        settings.SEARCH_CLIENT.get_index(index_uid).delete_document(search_id)


def reindex(self, request, queryset):
    """Reindex objects from the given queryset by calling Meilisearch API for each."""
    for instance in queryset:
        sender = instance.__class__
        update_main_search_indexes(sender, instance, raw=False)
        if isinstance(instance, (Training, Asset, Post)):
            update_training_search_index(sender, instance, raw=False)
        if isinstance(instance, Section):
            update_training_search_index_on_section_update(sender, instance, raw=False)


def remove_from_index(self, request, queryset):
    """Remove objects from the given queryset from all search indices."""
    for instance in queryset:
        sender = instance.__class__
        delete_from_index(sender, instance)
