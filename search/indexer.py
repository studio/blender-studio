from abc import ABC
from datetime import timedelta
from typing import Type, Any, List
import logging

from background_task import background
from background_task.tasks import TaskSchedule
from django.conf import settings
from django.utils import timezone
import meilisearch.errors

from blog.models import Post
from common.types import assert_cast
from projects.models import Asset
from search.health_check import check_meilisearch, MeiliSearchServiceError
from search.serializers.base import SearchableModel, BaseSearchSerializer
from search.serializers.main_search import MainSearchSerializer
from search.serializers.training_search import TrainingSearchSerializer

from search import MAIN_INDEX_UIDS, TRAINING_INDEX_UIDS

log = logging.getLogger(__name__)
INDEXING_DELAY_SECONDS = 60


def _is_indexed(index_uid: str, search_id: str) -> bool:
    try:
        settings.SEARCH_CLIENT.get_index(index_uid).get_document(search_id)
        return True
    except meilisearch.errors.MeiliSearchApiError as err:
        if 'document_not_found' != err.code:
            log.error(f'Unable to check if {search_id} is indexed by {index_uid}: {err}')
    except MeiliSearchServiceError as err:
        log.error(f'Unable to check if {search_id} is indexed by {index_uid}: {err}')
    return False


def is_search_service_available() -> bool:
    try:
        check_meilisearch(check_indexes=True)
        return True
    except MeiliSearchServiceError as err:
        log.error(f'Search service unavailable: {err}')
    return False


def instance_search_id(sender: Type[SearchableModel], instance: SearchableModel) -> str:
    return f'{sender._meta.model_name}_{instance.id}'


class BasePostSaveSearchIndexer(ABC):
    """A base class for post_save signal handlers.

    Attributes:
        index_uids: A list of index uids to which a document should be added.
        serializer: A BaseSearchSerializer instance, used to prepare the object to be
            added to the indexes from the index_uids list.
    """

    index_uids: List[str]
    searchable_attributes: List[str]
    sortable_attributes: List[str]
    filterable_attributes: List[str]
    serializer: BaseSearchSerializer

    def _add_document_to_index(self, data_to_load: List[Any]) -> None:
        """Adds document to the appropriate search index and its replicas."""
        for index_uid in self.index_uids:
            index = settings.SEARCH_CLIENT.get_index(index_uid)
            index.add_documents(data_to_load)

            # There seems to be no way in MeiliSearch v0.13.0 to disable adding new document
            # fields automatically to searchable attrs, so we update the settings to set them:
            index.update_searchable_attributes(self.searchable_attributes)
            index.update_sortable_attributes(self.sortable_attributes)
            index.update_filterable_attributes(self.filterable_attributes)

    def _remove_document(self, sender: Type[SearchableModel], instance: SearchableModel) -> None:
        search_id = instance_search_id(sender, instance)
        for index_uid in self.index_uids:
            is_indexed = _is_indexed(index_uid, search_id)
            if not is_indexed:
                continue
            settings.SEARCH_CLIENT.get_index(index_uid).delete_document(search_id)
            log.warning(f'Removed {instance} from the {index_uid} index.')

    def handle(self, sender: Type[SearchableModel], instance: SearchableModel) -> None:
        """Indexes the objects that should be available in search."""
        if not is_search_service_available():
            log.warning('Skipping index updates on post_save: search service unavailable')
            return

        instance_qs = self.serializer.get_searchable_queryset(sender, id=instance.id)
        if instance_qs:
            (data_to_load, not_ready) = self.serializer.prepare_data_for_indexing(instance_qs)
            if len(not_ready):
                now = timezone.now()
                index_at = instance.date_published > now and instance.date_published or now
                index_at = index_at + timedelta(seconds=INDEXING_DELAY_SECONDS)
                # queue for later processing
                index_instance_async(
                    model_name=instance._meta.model_name,
                    pk=instance.pk,
                    index_uids=self.index_uids,
                    indexer_type=self.__class__.__name__,
                    schedule={'action': TaskSchedule.RESCHEDULE_EXISTING, 'run_at': index_at},
                )
                return
            self._add_document_to_index(data_to_load)
            log.info(f'Added {instance} to the {self.index_uids} search indexes.')
        else:
            # Make sure that, if this instance shouldn't be in the index, it's not there
            self._remove_document(sender, instance)


class MainPostSaveSearchIndexer(BasePostSaveSearchIndexer):
    """Post_save signal handler, adds documents to the main index and its replicas."""

    index_uids = MAIN_INDEX_UIDS
    searchable_attributes = assert_cast(list, settings.MAIN_SEARCH['SEARCHABLE_ATTRIBUTES'])
    sortable_attributes = assert_cast(list, settings.MAIN_SEARCH['SORTABLE_ATTRIBUTES'])
    filterable_attributes = assert_cast(list, settings.MAIN_SEARCH['FILTERABLE_ATTRIBUTES'])
    serializer = MainSearchSerializer()


class TrainingPostSaveSearchIndexer(BasePostSaveSearchIndexer):
    """Post_save signal handler, adds documents to the training index and its replicas."""

    index_uids = TRAINING_INDEX_UIDS
    searchable_attributes = assert_cast(list, settings.TRAINING_SEARCH['SEARCHABLE_ATTRIBUTES'])
    sortable_attributes = assert_cast(list, settings.TRAINING_SEARCH['SORTABLE_ATTRIBUTES'])
    filterable_attributes = assert_cast(list, settings.TRAINING_SEARCH['FILTERABLE_ATTRIBUTES'])
    serializer = TrainingSearchSerializer()


@background(schedule=60)
def index_instance_async(model_name, pk, index_uids, indexer_type):
    """Background task that waits for an instance to be ready for indexing.

    Initial delay (60s) is meant as an optimistic estimate for a coconut job to finish,
    the task should be retried using a standard retry policy.

    This task can also be scheduled to index blog posts published in the future.
    """
    if indexer_type.endswith('MainPostSaveSearchIndexer'):
        indexer = MainPostSaveSearchIndexer()
    elif indexer_type.endswith('TrainingPostSaveSearchIndexer'):
        indexer = TrainingPostSaveSearchIndexer()
    else:
        log.error(f'Called with unexpected indexer_type={indexer_type}')
        return
    if model_name == 'asset':
        model = Asset
    elif model_name == 'post':
        model = Post
    else:
        log.error(f'Called with unexpected model_name={model_name}')
        return

    instance_qs = indexer.serializer.get_searchable_queryset(model, id=pk)
    (data_to_load, not_ready) = indexer.serializer.prepare_data_for_indexing(instance_qs)
    if len(not_ready):
        raise Exception(f'{model_name}.pk={pk} is not yet ready for indexing, will retry later')

    for index_uid in index_uids:
        index = settings.SEARCH_CLIENT.get_index(index_uid)
        index.add_documents(data_to_load)
        index.update_searchable_attributes(indexer.searchable_attributes)
        index.update_sortable_attributes(indexer.sortable_attributes)
