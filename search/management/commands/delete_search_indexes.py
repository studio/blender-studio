# noqa: D100
from typing import Optional, Any, Dict, List
import logging

import meilisearch
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from common.types import assert_cast
from search import _wait_for_task
from search.health_check import MeiliSearchServiceError, check_meilisearch

logger = logging.getLogger(__name__)


class Command(BaseCommand):  # noqa: D101
    help = f'Delete all search indexes "{settings.MAIN_SEARCH["RANKING_RULES"].keys()}"'

    def _delete_index(self, index_uid: str) -> meilisearch.index.Index:
        try:
            task = settings.SEARCH_CLIENT.delete_index(index_uid)
            logger.debug(task)
            task_status, error_code = _wait_for_task(task)
            if error_code == 'index_not_found':
                self.stdout.write(f'Index "{index_uid}" does not exist')
            elif task_status != 'succeeded':
                raise Exception(
                    'Unable to delete index %s: task failed with status "%s", error code: %s',
                    index_uid,
                    task_status,
                    error_code,
                )
        except Exception as err:
            raise CommandError(err)
        else:
            self.stdout.write(self.style.SUCCESS(f'Deleted the index "{index_uid}".'))

    def handle(self, *args: Any, **options: Any) -> Optional[str]:  # noqa: D102
        try:
            check_meilisearch()
        except MeiliSearchServiceError as err:
            raise CommandError(err)

        # Create or update the main index, the replica indexes, and the training index
        main_search_ranking_rules = assert_cast(dict, settings.MAIN_SEARCH['RANKING_RULES'])
        training_search_ranking_rules = assert_cast(dict, settings.TRAINING_SEARCH['RANKING_RULES'])
        indexes_and_ranking_rules: Dict[str, List[str]] = {
            **main_search_ranking_rules,
            **training_search_ranking_rules,
        }
        for index_uid, ranking_rules in indexes_and_ranking_rules.items():
            self._delete_index(index_uid)

            self.stdout.write(self.style.SUCCESS(f'"{index_uid}" has been deleted.'))
        return None
