from django.db.models.expressions import F, Value, Case, When
from django.db.models.fields import CharField
from taggit.models import Tag

from projects.models import Asset, AssetCategory
from search.serializers.base import BaseSearchSerializer
from training.models import Training
from blog.models import Post

clean_tag = BaseSearchSerializer.clean_tag


class TrainingSearchSerializer(BaseSearchSerializer):
    """Prepare database objects to be indexed in the training search index."""

    models_to_index = [Training, Asset, Post]
    filter_args = {}
    filter_kwargs = {
        Training: {'is_published': True},
        Asset: {
            'is_published': True,
            'project__is_published': True,
            'category': AssetCategory.production_lesson,
        },
        Post: {
            'is_published': True,
            'category': Post.PostCategory.ARTICLE,
            'tags__name__in': ['training'],
        },
    }
    annotations = {
        Training: {},
        Asset: {
            'author_name': Case(
                When(static_asset__author__isnull=False, then=F('static_asset__author__full_name')),
                default=F('static_asset__user__full_name'),
                output_field=CharField(),
            ),
            'project_title': F('project__title'),
            'type': Value(clean_tag(AssetCategory.production_lesson), output_field=CharField()),
        },
        Post: {
            'author_name': F('author__full_name'),
            'project_title': F('project__title'),
            'name': F('title'),
            'type': Case(
                When(
                    category=Post.PostCategory.ARTICLE, then=Value(Post.PostCategory.ARTICLE.label)
                ),
                default=Value('Unknown'),  # In case there's an unexpected category
                output_field=CharField(),
            ),
        },
    }
    additional_fields = {
        Training: {
            'tags': lambda instance: [clean_tag(tag.name) for tag in instance.tags.all()],
            'secondary_tags': lambda instance: [
                clean_tag(tag.name)
                for tag in Tag.objects.filter(section__chapter__training__pk=instance.pk).distinct()
            ],
            'favorite_url': lambda instance: instance.favorite_url,
            'is_free': lambda instance: instance.is_free,
        },
        Asset: {'tags': lambda instance: [clean_tag(tag.name) for tag in instance.tags.all()]},
        Post: {
            'tags': lambda instance: [tag.name for tag in instance.tags.all()],
            'description': lambda instance: '' if not instance.excerpt else instance.excerpt,
        },
    }
