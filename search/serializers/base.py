from abc import ABC
from html.parser import HTMLParser
from html import unescape
from io import StringIO
from typing import Optional, Any, Type, Dict, Union, Callable, List, TYPE_CHECKING
import json

from django.core.serializers.json import DjangoJSONEncoder
from django.db.models.expressions import Value
from django.db.models.fields import CharField
from django.db.models.functions.text import Concat
from django.db.models.query import QuerySet
from django.utils import timezone

if TYPE_CHECKING:
    # ValuesQuerySet has long been removed from Django, but it is required by mypy
    from django.db.models.query import ValuesQuerySet

from blog.models import Post
from projects.models import Project, Asset
from training.models import Training, Section

SearchableModel = Union[Project, Asset, Training, Section, Post]


class NotReadyForIndexing(Exception):
    """Currently used to communicate that a video Asset is missing a thumbnail.
    We expect it to be ready when a coconut job finishes and a corresponding static_asset record
    is updated.
    """
    pass


class HTMLText(HTMLParser):
    def __init__(self):
        """Initialise HTML parser."""
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs = True
        self.text = StringIO()

    def handle_data(self, d):
        self.text.write(d)

    def get_data(self):
        return self.text.getvalue()


class BaseSearchSerializer(ABC):
    """A base class for serializers of database data for indexing.

    Each concrete serializer class defines which objects will be added to a given index,
    and how they will be serialized.

    Attributes:
        models_to_index: A list of models to be added to the index.

        The following attributes are dicts whose keys are models, and values - dicts:
        filter_args: For each model, a list of positional arguments for filtering
            the objects that should be available in search (e.g. published).
            Allows for more complex queries, e.g. using "Q" in conjunction with "|"
        filter_kwargs: For each model, a dict of keyword arguments for filtering
            the objects that should be available in search (e.g. published).
        annotations: For each model, a dict with model-specific annotations to be added to
            the queryset.
        additional_fields: For each model, a dict with additional fields to be added
            to each instance; the values of the fields are lambda functions, taking the
            instance as their only argument and returning the additional field's value.
    """

    models_to_index: List[Type[SearchableModel]]
    filter_args: Dict[Type[SearchableModel], List[Any]]
    filter_kwargs: Dict[Type[SearchableModel], Dict[str, Any]]
    annotations: Dict[Type[SearchableModel], Dict[str, Any]]
    additional_fields: Dict[Type[SearchableModel], Dict[str, Callable[[Any], Any]]]

    def get_searchable_queryset(
        self, model: Type[SearchableModel], *filter_args: Any, **filter_kwargs: Any
    ) -> 'QuerySet[SearchableModel]':
        """Only returns the model's objects that should be available in search."""
        args = []
        args.extend(self.filter_args.get(model, []))
        args.extend(filter_args)

        kwargs = {}
        kwargs.update(self.filter_kwargs.get(model, {}))
        kwargs.update(filter_kwargs)
        return model.objects.filter(*args, **kwargs)

    def prepare_data_for_indexing(
        self, queryset: 'QuerySet[SearchableModel]'
    ) -> (List[Dict[str, Any]], List[SearchableModel]):
        """Serializes objects for search, adding all the necessary additional fields.

        Returns a tuple with two values:
            1. a list of serialized data,
            2. a list of instances that are not ready for indexing.
        """
        model = queryset.model

        queryset = self._add_common_annotations(queryset)
        queryset = queryset.annotate(**self.annotations.get(model, {}))
        qs_values = queryset.values()
        not_ready_instances = []
        for instance_dict, instance in zip(qs_values, queryset):
            try:
                instance_dict = self._set_common_additional_fields(instance_dict, instance)
            except NotReadyForIndexing:
                not_ready_instances.append(instance)
                continue
            for key, func in self.additional_fields.get(model, {}).items():
                instance_dict[key] = func(instance)

        return (self._serialize_data(qs_values), not_ready_instances)

    def _add_common_annotations(
        self, queryset: 'QuerySet[SearchableModel]'
    ) -> 'QuerySet[SearchableModel]':
        """Adds queryset annotations common to all indexed objects: model and search_id."""
        model = queryset.model._meta.model_name

        return queryset.annotate(
            model=Value(model, output_field=CharField()),
            search_id=Concat(Value(f'{model}_'), 'id', output_field=CharField()),
        )

    def _set_common_additional_fields(
        self, instance_dict: Dict[Any, Any], instance: SearchableModel
    ) -> Dict[Any, Any]:
        """Adds fields common to all searchable models to the values dict of the instance."""
        now = timezone.now()
        date_published = getattr(instance, 'date_published', None)
        if date_published and date_published > now:
            raise NotReadyForIndexing(
                f'{instance.__class__} should be published at {date_published}'
            )

        instance_dict['url'] = instance.url
        instance_dict['timestamp'] = (date_published or instance.date_created).timestamp()

        if hasattr(instance, 'description'):
            instance_dict['description'] = self.clean_html(instance.description)

        if isinstance(instance, Asset):
            if instance.static_asset:
                # A video may still be missing its thumbnail at this moment,
                # because processing takes time.
                thumbnail_url = instance.static_asset.thumbnail_s_url
                if not thumbnail_url:
                    raise NotReadyForIndexing()
                instance_dict['thumbnail_url'] = thumbnail_url
        elif isinstance(instance, Section):
            instance_dict['thumbnail_url'] = instance.chapter.training.thumbnail_s_url or ''
        else:
            instance_dict['thumbnail_url'] = instance.thumbnail_s_url or ''
        return instance_dict

    def _serialize_data(
        self, qs_values: 'ValuesQuerySet[SearchableModel, Any]'
    ) -> List[Dict[str, Any]]:
        """Turns values queryset into a list of objects that can be added to a search index.

        The Index.add_documents method expects a list of objects, not a single object.
        Datetime values have to be serialized with DjangoJSONEncoder.
        """
        serialized_data: List[Dict[str, Any]] = json.loads(
            json.dumps(list(qs_values), cls=DjangoJSONEncoder)
        )
        return serialized_data

    @classmethod
    def clean_html(cls, text: Optional[str]) -> Optional[str]:
        """Strip HTML tags from given text."""
        if not text:
            return text
        s = HTMLText()
        s.feed(unescape(text))
        return s.get_data()

    @classmethod
    def clean_tag(cls, tag: Optional[str]) -> Optional[str]:
        """Replace '_' and '-' characters with spaces in a given tag."""
        if not tag:
            return tag
        return tag.replace('-', ' ').replace('_', ' ')
