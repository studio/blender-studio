/* eslint-disable no-unused-expressions */
/* global ajax:false */

window.comments = (function comments() {
  class Comment {
    constructor(element) {
      this.id = element.dataset.commentId;
      this.likeUrl = element.dataset.commentLikeUrl;
      this.editUrl = element.dataset.editUrl;
      this.deleteUrl = element.dataset.deleteUrl;
      this.deleteTreeUrl = element.dataset.deleteTreeUrl;
      this.hardDeleteTreeUrl = element.dataset.hardDeleteTreeUrl;
      this.archiveUrl = element.dataset.archiveUrl;
      this.profileImageUrl = element.dataset.profileImageUrl;
      this.message_markdown = element.dataset.message;
      this.message_html = element.querySelector('.js-comment-text').innerHTML;
      this.element = element;
      this._setupEventListeners();
    }

    get replyLink() {
      return this.element.querySelector('.js-comment-reply');
    }

    get editLink() {
      return this.element.querySelector(':scope > .js-comment-content .js-comment-edit');
    }

    get deleteLink() {
      return this.element.querySelector('.js-comment-delete');
    }

    get deleteTreeLink() {
      return this.element.querySelector('.js-comment-delete-tree');
    }

    get hardDeleteTreeLink() {
      return this.element.querySelector('.js-comment-hard-delete-tree');
    }

    get archiveLink() {
      return this.element.querySelector('.js-comment-archive');
    }

    get likeButton() {
      return this.element.querySelector('.js-checkbox-like');
    }

    get commentLikesCountElement() {
      return this.element.querySelector('.js-likes-count');
    }

    get commentSection() {
      const commentSectionElement = this.element.closest(`.${Section.className}`);
      if (commentSectionElement == null) {
        return null;
      } else {
        return Section.getOrWrap(commentSectionElement);
      }
    }

    get textElement() {
      return this.element.querySelector('.js-comment-text');
    }

    get message() {
      return this.textElement.innerText;
    }

    set message(value) {
      this.textElement.innerText = value;
    }

    _setupEventListeners() {
      this.replyLink &&
        this.replyLink.addEventListener('click', (event) => {
          event.preventDefault();
          this._showReplyInput();
        });

      this.editLink &&
        this.editLink.addEventListener('click', (event) => {
          event.preventDefault();
          this._showEditInput();
        });

      this.deleteLink &&
        this.deleteLink.addEventListener('click', (event) => {
          event.preventDefault();
          this._postDeleteComment();
        });

      this.deleteTreeLink &&
        this.deleteTreeLink.addEventListener('click', (event) => {
          event.preventDefault();
          this._postDeleteTreeComment();
        });

      this.hardDeleteTreeLink &&
        this.hardDeleteTreeLink.addEventListener('click', (event) => {
          event.preventDefault();
          this._postHardDeleteTreeComment();
        });

      this.archiveLink &&
        this.archiveLink.addEventListener('click', (event) => {
          event.preventDefault();
          this._postArchiveComment();
        });

      this.likeButton && this.likeButton.addEventListener('click', this._postLike.bind(this));
    }

    get replyInputsElement() {
      const inputsElement = this.element.querySelector('.js-comment-reply-inputs');

      if (inputsElement == null) {
        return null;
      } else {
        return inputsElement;
      }
    }

    get replyInput() {
      const { replyInputsElement } = this;
      if (replyInputsElement == null) {
        return null;
      }

      const replyInputElement = replyInputsElement.querySelector(`.${ReplyInput.className}`);
      if (replyInputElement == null) {
        return null;
      }

      return ReplyInput.getOrWrap(replyInputElement);
    }

    _getOrCreateReplyInput() {
      const { replyInput, replyInputsElement } = this;
      let inputText = null;
      if (replyInput == null) {
        // eslint-disable-next-line no-undef
        const replyInput = ReplyInput.create(currentUser.image_url);
        inputText = `@${this.element.querySelector('.js-comment-name').innerText}&nbsp;`;
        replyInput.element.querySelector('.js-comment-input-div').innerHTML = inputText;
        replyInputsElement.append(replyInput.element);
        return replyInput;
      } else {
        inputText = `@${this.element.querySelector('.js-comment-name').innerText}&nbsp;`;
        replyInput.element.querySelector('.js-comment-input-div').innerHTML = inputText;
        return replyInput;
      }
    }

    _showReplyInput() {
      const replyInput = this._getOrCreateReplyInput();
      const replyInputElement = replyInput.element.querySelector('.js-comment-input-div');
      replyInput.show();
      replyInputElement.focus();
      // Set caret to the end.
      const range = document.createRange();
      const sel = window.getSelection();
      range.setStart(replyInputElement.firstChild, replyInputElement.innerText.length);
      range.collapse(true);
      sel.removeAllRanges();
      sel.addRange(range);
    }

    appendReply(comment) {
      const repliesElement = this.element
        .closest('.js-top-level-comment')
        .querySelector('.js-replies .js-comments');
      repliesElement.append(comment.element);
    }

    get editInputsElement() {
      const inputsElement = this.element.querySelector('.js-comment-edit-inputs');

      if (inputsElement == null) {
        return null;
      } else {
        return inputsElement;
      }
    }

    get editInput() {
      const { editInputsElement } = this;
      if (editInputsElement == null) {
        return null;
      }

      const editInputElement = editInputsElement.querySelector(`.${EditInput.className}`);
      if (editInputElement == null) {
        return null;
      }

      return EditInput.getOrWrap(editInputElement);
    }

    _getOrCreateEditInput() {
      const { editInput, editInputsElement } = this;
      if (editInput == null) {
        // eslint-disable-next-line no-undef
        const editInput = EditInput.create(currentUser.image_url);
        editInputsElement.append(editInput.element);
        return editInput;
      } else {
        return editInput;
      }
    }

    _showEditInput() {
      const editInput = this._getOrCreateEditInput();
      editInput.prepopulateMessage();
      this.hideContent();
      editInput.show();
      editInput.focus();
    }

    _postLike() {
      const { likeButton } = this;

      ajax
        .jsonRequest('POST', this.likeUrl, {
          like: !likeButton.dataset.checked,
        })
        .then((data) => {
          if (data.like) {
            likeButton.dataset.checked = 'checked';
          } else {
            delete likeButton.dataset.checked;
          }

          if (likeButton.querySelector('.js-likes-count')) {
            // eslint-disable-next-line no-param-reassign
            likeButton.querySelector('.js-likes-count').innerText = data.number_of_likes;
          } else {
            const likeCountHTML = `<span class="js-likes-count">${data.number_of_likes}</span>`;
            likeButton.insertAdjacentHTML('beforeend', likeCountHTML);
          }
        });
    }

    _postDeleteComment() {
      const { deleteUrl, element } = this;

      ajax.jsonRequest('POST', deleteUrl).then(() => {
        element.querySelector('.js-comment-text').textContent = '[deleted]';
        element.querySelector('.js-comment-name').textContent = '[deleted]';

        const commentToolbar = element.querySelector('.js-comment-toolbar');
        // eslint-disable-next-line no-unused-expressions
        commentToolbar && commentToolbar.remove();
      });
    }

    _postDeleteTreeComment() {
      const { deleteTreeUrl, element } = this;

      ajax.jsonRequest('POST', deleteTreeUrl).then(() => {
        if (element.classList.contains('.js-top-level-comment')) {
          // eslint-disable-next-line no-return-assign
          element.querySelectorAll('.js-comment-text').forEach((e) => (e.textContent = '[deleted]'));
          // eslint-disable-next-line no-return-assign
          element.querySelectorAll('.js-comment-name').forEach((e) => (e.textContent = '[deleted]'));
          // eslint-disable-next-line no-unused-expressions
          element.querySelector('.js-comment-toolbar') &&
            element.querySelectorAll('.js-comment-toolbar').forEach((e) => e.remove());
        } else {
          element
            .closest('.js-top-level-comment')
            .querySelectorAll('.js-comment-text')
            // eslint-disable-next-line no-return-assign
            .forEach((e) => (e.textContent = '[deleted]'));
          element
            .closest('.js-top-level-comment')
            .querySelectorAll('.js-comment-name')
            // eslint-disable-next-line no-return-assign
            .forEach((e) => (e.textContent = '[deleted]'));
        }
      });
    }

    _postHardDeleteTreeComment() {
      const { hardDeleteTreeUrl, element } = this;

      ajax.jsonRequest('POST', hardDeleteTreeUrl).then(() => {
        if (element.classList.contains('.js-top-level-comment')) {
          element.remove();
        } else {
          element.closest('.js-top-level-comment').remove();
        }
      });
    }

    _postArchiveComment() {
      const { archiveUrl, element } = this;
      const archivedBadge = '<p class="badge bg-secondary js-archived-badge">Archived</p>';
      // const commentTitleBar = element.querySelectorAll('.js-comment-name-date-wrapper');

      function unarchive(element) {
        element.classList.remove('js-archived', 'archived');
        element.querySelectorAll('.js-comment').forEach((e) => e.classList.remove('js-archived', 'archived'));
        element
          .querySelectorAll('.js-comment-archive .material-icons')
          // eslint-disable-next-line no-return-assign
          .forEach((e) => (e.textContent = 'archive'));
        element
          .querySelectorAll('.js-comment-archive-text')
          // eslint-disable-next-line no-return-assign
          .forEach((e) => (e.textContent = 'Archive comment'));
        element.querySelectorAll('.js-archived-badge').forEach((e) => e.remove());
        // eslint-disable-next-line no-unused-expressions
        element.querySelector('.js-comment-expand-archived') &&
          element.querySelectorAll('.js-comment-expand-archived').forEach((e) => e.remove());
      }

      function archive(element) {
        element.classList.add('js-archived', 'archived');
        element.querySelectorAll('.js-comment').forEach((e) => e.classList.add('js-archived', 'archived'));
        element
          .querySelectorAll('.js-comment-archive .material-icons')
          // eslint-disable-next-line no-return-assign
          .forEach((e) => (e.textContent = 'unarchive'));
        element
          .querySelectorAll('.js-comment-archive-text')
          // eslint-disable-next-line no-return-assign
          .forEach((e) => (e.textContent = 'Un-archive comment'));
        element
          .querySelectorAll('.js-comment-name-date-wrapper')
          // eslint-disable-next-line no-return-assign
          .forEach((e) => (e.innerHTML += archivedBadge));
      }

      ajax.jsonRequest('POST', archiveUrl).then(() => {
        if (element.classList.contains('js-archived')) {
          if (element.classList.contains('js-top-level-comment')) {
            unarchive(element);
          } else {
            unarchive(element.closest('.js-top-level-comment'));
          }
        } else if (element.classList.contains('js-top-level-comment')) {
          archive(element);
        } else {
          archive(element.closest('.js-top-level-comment'));
        }
      });
    }

    get contentElement() {
      return this.element.querySelector('.js-comment-content');
    }

    showContent() {
      this.contentElement.style.display = null;
    }

    hideContent() {
      this.contentElement.style.display = 'none';
    }
  }

  Comment.className = 'comment';
  Comment.instances = new WeakMap();

  Comment.create = function create(
    id,
    fullName,
    profileImageUrl,
    dateString,
    message,
    messageHtml,
    likeUrl,
    liked,
    likes,
    editUrl,
    deleteUrl,
    archiveUrl
  ) {
    const template = document.getElementById('comment-template');
    const element = template.content.cloneNode(true).querySelector(`.${Comment.className}`);
    element.dataset.commentId = id;
    element.dataset.profileImageUrl = profileImageUrl;
    element.dataset.commentLikeUrl = likeUrl;
    element.dataset.editUrl = editUrl;
    element.dataset.deleteUrl = deleteUrl;
    element.dataset.archiveUrl = archiveUrl;
    element.dataset.message = message;
    if (profileImageUrl) {
      // Breaks new comment display on submit
      // element.querySelector('.profile').style.backgroundImage = `url('${profileImageUrl}')`;
    }
    element.querySelector('.js-comment-name').innerText = fullName;
    element.querySelector('.js-comment-date').innerText = formatDate(dateString);
    element.querySelector('.js-comment-text').innerHTML = messageHtml;
    element.querySelector('.js-likes-count').innerText = likes;
    if (liked) {
      element.querySelector('.js-checkbox-like').dataset.checked = 'checked';
    } else {
      delete element.querySelector('.js-checkbox-like').dataset.checked;
    }
    return Comment.getOrWrap(element);
  };

  Comment.getOrWrap = function getOrWrap(element) {
    const c = Comment.instances.get(element);
    if (c == null) {
      return new Comment(element);
    } else {
      return c;
    }
  };

  class CommentInput {
    constructor(element) {
      this.element = element;
    }

    get sendButton() {
      return this.element.querySelector('.js-comment-send .btn-input');
    }

    get inputElement() {
      return this.element.querySelector('.js-comment-input-div');
    }

    get commentSection() {
      const commentSectionElement = this.element.closest(`.${Section.className}`);
      if (commentSectionElement == null) {
        return null;
      } else {
        return Section.getOrWrap(commentSectionElement);
      }
    }

    show() {
      this.element.style.display = null;
    }

    hide() {
      this.element.style.display = 'none';
    }

    focus() {
      this.inputElement.focus();
    }
  }

  class MainInput extends CommentInput {
    constructor(element) {
      super(element);
      MainInput.instances.set(element, this);
      this._setupEventListeners();
    }

    _setupEventListeners() {
      this.sendButton.addEventListener('click', (event) => {
        event.preventDefault();
        this._postComment();
      });

      this.inputElement.addEventListener('keydown', (event) => {
        if (event.key === 'Enter' && !event.shiftKey) {
          event.preventDefault();
          this.sendButton.click();
        } else if (event.key === 'Escape') {
          this.comment.showContent();
          this.hide();
        }
      });
    }

    _postComment() {
      const { commentSection, inputElement } = this;
      const message = inputElement.value;

      if (message === '') {
        return;
      }

      inputElement.value = '';
      ajax
        .jsonRequest('POST', commentSection.commentUrl, {
          reply_to: null,
          message,
        })
        .then((data) => {
          const comment = Comment.create(
            data.id,
            data.full_name,
            data.profile_image_url,
            data.date_string,
            data.message,
            data.message_html,
            data.like_url,
            data.liked,
            data.likes,
            data.edit_url,
            data.delete_url
          );
          comment.element.classList.add('js-top-level-comment', 'top-level-comment');
          commentSection.prependComment(comment);
        });

      bumpCommentsCounter();
    }
  }

  MainInput.className = 'comment-main-input';
  MainInput.instances = new WeakMap();

  MainInput.getOrWrap = function getOrWrap(element) {
    const c = MainInput.instances.get(element);
    if (c == null) {
      return new MainInput(element);
    } else {
      return c;
    }
  };

  class ReplyInput extends CommentInput {
    constructor(element) {
      super(element);
      ReplyInput.instances.set(element, this);
      this._setupEventListeners();
    }

    get replyTo() {
      return Comment.getOrWrap(this.element.closest(`.${Comment.className}`));
    }

    _setupEventListeners() {
      this.sendButton.addEventListener('click', (event) => {
        event.preventDefault();
        this._postReply();
        this.hide();
      });

      this.inputElement.addEventListener('keydown', (event) => {
        if (event.key === 'Enter' && !event.shiftKey) {
          event.preventDefault();
          this.sendButton.click();
        } else if (event.key === 'Escape') {
          // this.comment.showContent();
          this.hide();
        }
      });
    }

    _postReply() {
      const { commentSection, inputElement, replyTo } = this;

      const message = inputElement.value;

      inputElement.value = '';

      ajax
        .jsonRequest('POST', commentSection.commentUrl, {
          reply_to: replyTo.id,
          message,
        })
        .then((data) => {
          const comment = Comment.create(
            data.id,
            data.full_name,
            data.profile_image_url,
            data.date_string,
            data.message,
            data.message_html,
            data.like_url,
            data.liked,
            data.likes,
            data.edit_url,
            data.delete_url
          );
          replyTo.appendReply(comment);
        });
    }
  }

  ReplyInput.className = 'comment-reply-input';
  ReplyInput.instances = new WeakMap();

  ReplyInput.getOrWrap = function getOrWrap(element) {
    const c = ReplyInput.instances.get(element);
    if (c == null) {
      return new ReplyInput(element);
    } else {
      return c;
    }
  };

  ReplyInput.create = function create(profileImageUrl) {
    const template = document.getElementById('comment-reply-input-template');
    const element = template.content.cloneNode(true).querySelector(`.${ReplyInput.className}`);
    if (!profileImageUrl) {
      element.querySelector('.profile').style.backgroundImage = `url('${profileImageUrl}')`;
    }
    return ReplyInput.getOrWrap(element);
  };

  class EditInput extends CommentInput {
    constructor(element) {
      super(element);
      EditInput.instances.set(element, this);
      this._setupEventListeners();
    }

    get comment() {
      return Comment.getOrWrap(this.element.closest(`.${Comment.className}`));
    }

    prepopulateMessage() {
      this.inputElement.innerText = this.comment.message_markdown;
    }

    _setupEventListeners() {
      this.sendButton.addEventListener('click', (event) => {
        event.preventDefault();
        this.comment.showContent();
        this._postEdit();
        this.hide();
      });

      this.inputElement.addEventListener('keydown', (event) => {
        if (event.key === 'Enter' && !event.shiftKey) {
          event.preventDefault();
          this.sendButton.click();
        } else if (event.key === 'Escape') {
          this.comment.showContent();
          this.hide();
        }
      });
    }

    _postEdit() {
      const { comment, inputElement } = this;
      const message = inputElement.value;

      ajax
        .jsonRequest('POST', comment.editUrl, {
          message,
        })
        .then((data) => {
          this.comment.message = data.message;
          this.comment.element.dataset.message = this.comment.message;
          this.comment.message_html = data.message_html;
          this.comment.element.querySelector('.js-comment-text').innerHTML = data.message_html;
        });
    }
  }

  EditInput.className = 'js-comment-edit-input';
  EditInput.instances = new WeakMap();

  EditInput.getOrWrap = function getOrWrap(element) {
    const c = EditInput.instances.get(element);
    if (c == null) {
      return new EditInput(element);
    } else {
      return c;
    }
  };

  EditInput.create = function create(profileImageUrl) {
    const template = document.getElementById('comment-edit-input-template');
    const element = template.content.cloneNode(true).querySelector(`.${EditInput.className}`);
    if (!profileImageUrl) {
      element.querySelector('.profile').style.backgroundImage = `url('${profileImageUrl}')`;
    }
    return EditInput.getOrWrap(element);
  };

  class Section {
    constructor(element) {
      Section.instances.set(element, this);
      this.element = element;
      this.commentUrl = element.dataset.commentUrl;
      this.profileImageUrl = element.dataset.profileImageUrl;
    }

    prependComment(comment) {
      this.element.querySelector('.js-comments').prepend(comment.element);
    }
  }

  // Create function 'bumpCommentsCount' to bump comments count on new comment submission
  function bumpCommentsCounter() {
    const commentsCounter = document.querySelector(".js-comments-counter");
    let commentsCounterValue = parseInt(commentsCounter.textContent);

    commentsCounterValue += 1;

    commentsCounter.textContent = commentsCounterValue.toString();
  }

  // Create function 'formatDate' to match new comment dates' output with system
  function formatDate(string) {
    // Init variables date parts
    const dateParts = string.split(' - ')[0].split(' ');
    const day = dateParts[0];
    const month = dateParts[1];
    const year = dateParts[2];

    // Add ordinal suffix to day
    let suffix = 'th';

    if (day === '1' || day === '21' || day === '31') {
      suffix = 'st';
    } else if (day === '2' || day === '22') {
      suffix = 'nd';
    } else if (day === '3' || day === '23') {
      suffix = 'rd';
    }

    const formattedDate = `${month} ${day}${suffix}, ${year}`;

    return formattedDate;
  }

  Section.className = 'js-comment-section';
  Section.instances = new WeakMap();
  Section.getOrWrap = function getOrWrap(element) {
    const c = Section.instances.get(element);
    if (c == null) {
      return new Section(element);
    } else {
      return c;
    }
  };

  // Comment actions are activated on 'activateComments' event. This allows to activate
  // comments manually if needed. Normally the event is dispatched after DOM loads.
  document.addEventListener('DOMContentLoaded', () => {
    const event = new CustomEvent('activateComments', { bubbles: true });
    document.dispatchEvent(event);
  });

  document.addEventListener('activateComments', () => {
    document.getElementsByClassName(Section.className).forEach(Section.getOrWrap);
    document.getElementsByClassName(MainInput.className).forEach(MainInput.getOrWrap);
    document.getElementsByClassName(ReplyInput.className).forEach(ReplyInput.getOrWrap);
    document.getElementsByClassName(EditInput.className).forEach(EditInput.getOrWrap);
    document.getElementsByClassName(Comment.className).forEach(Comment.getOrWrap);
  });

  return { CommentSection: Section };
})();
